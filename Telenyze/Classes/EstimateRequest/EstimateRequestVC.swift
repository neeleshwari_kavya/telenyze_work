//
//  EstimateRequestVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 2 on 21/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import Foundation
import HarishFrameworkSwift4
import AssetsLibrary
import Photos

class EstimateRequestVC: UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var lblAddPhotos: UILabel!
    
    @IBOutlet var imgPicture1: UIImageView!
    @IBOutlet var imgPicture2: UIImageView!
    @IBOutlet var imgPicture3: UIImageView!
    @IBOutlet var imgPicture4: UIImageView!
    
    @IBOutlet var btnPicture1: UIButton!
    @IBOutlet var btnPicture2: UIButton!
    @IBOutlet var btnPicture3: UIButton!
    @IBOutlet var btnPicture4: UIButton!
    
    @IBOutlet var tfIssueDescription: UITextField!
    @IBOutlet var imgFile: UIImageView!
    @IBOutlet var btnUploadFile: UIButton!
    
    @IBOutlet var imgIsVehicleDrivable: UIImageView!
    @IBOutlet var btnIsVehicleDrivable: UIButton!
    @IBOutlet var imgNeedRide: UIImageView!
    @IBOutlet var btnNeedRide: UIButton!
    @IBOutlet var imgNeedTow: UIImageView!
    @IBOutlet var btnNeedTow: UIButton!
    @IBOutlet var imgHighPriority: UIImageView!
    @IBOutlet var btnHighPriority: UIButton!
    
    @IBOutlet var btnRequestQuote: UIButton!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblEstimatePrice: UILabel!
    @IBOutlet var lblStatusImg: Label!
    
    @IBOutlet var viewStatus: UIView!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var btnDecline: UIButton!
    @IBOutlet var viewAccept: UIView!
    
    //variables
    var status = ""
    var estimateId = ""
    var imgPicker = UIImagePickerController()
    var strImageSelected = ""
    var shop_id = ""
    var arrShopHolidays = NSMutableArray()
    var arrShopTimings = NSMutableArray()
    
    var imgPic1:UIImage? = nil
    var imgPic2:UIImage? = nil
    var imgPic3:UIImage? = nil
    var imgPic4:UIImage? = nil
    var imgPictureFile:UIImage? = nil
    
    var imgNamePic1 = ""
    var imgNamePic2 = ""
    var imgNamePic3 = ""
    var imgNamePic4 = ""
    var imgPictureFileName = ""
    var showStatusView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegatesAndDisplayView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hideKeyboard()
        self.crossNavigationButton()
        setNav(className: "Estimate request")
        self.registerForKeyboardNotifications()
        self.manageStatusView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Fuctions
    func manageStatusView()  {
        if status == "Quoted" {
            showStatusView = true
            self.wsGetEstimate()
        } else {
            showStatusView = false
        }
        
        if showStatusView {
            viewStatus.isHidden = false
            viewAccept.isHidden = false
            btnRequestQuote.isHidden = true
            btnPicture1.isEnabled = false
            btnPicture2.isEnabled = false
            btnPicture3.isEnabled = false
            btnPicture4.isEnabled = false
            btnUploadFile.isEnabled = false
            btnIsVehicleDrivable.isEnabled = false
            btnHighPriority.isEnabled = false
            btnNeedRide.isEnabled = false
            btnUploadFile.isEnabled = false
            btnNeedTow.isEnabled = false
            tfIssueDescription.isUserInteractionEnabled = false
            
        } else {
            viewStatus.isHidden = true
            viewAccept.isHidden = true
            btnRequestQuote.isHidden = false
            
            btnPicture1.isEnabled = true
            btnPicture2.isEnabled = true
            btnPicture3.isEnabled = true
            btnPicture4.isEnabled = true
            btnUploadFile.isEnabled = true
            btnIsVehicleDrivable.isEnabled = true
            btnHighPriority.isEnabled = true
            btnNeedRide.isEnabled = true
            btnUploadFile.isEnabled = true
            btnNeedTow.isEnabled = true
            tfIssueDescription.isUserInteractionEnabled = true
        }
    }
    
    func crossNavigationButton()  {
        let imgN = imageWithImage(image: UIImage(named: "cross.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
        
        let button1 = UIBarButtonItem(image: imgN, style: .plain, target: self, action: #selector(actionCrossButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionCrossButton()  {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    func delegatesAndDisplayView() {
        self.hideKeyboard()
        lblStatusImg.border1(appColor.yellowColor, lblStatusImg.frame.size.height/2, 0)
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.delegate = self
        self.subView.addGestureRecognizer(tap)
        
        viewStatus.isHidden = true
        btnRequestQuote.isHidden = false
        
        btnPicture1.tag = 101
        btnPicture2.tag = 102
        btnPicture3.tag = 103
        btnPicture4.tag = 104
        
        imgPicker.delegate = self
        tfIssueDescription.delegate = self
        tfIssueDescription.text = ""
        tfIssueDescription.placeholder = "Describe issues as best as possible..."
        tfIssueDescription.placeHolderColor = UIColor.lightGray
        tfIssueDescription.paddingView(8)
        btnUploadFile.setTitle("Upload additional files", for: .normal)
        btnUploadFile.setTitleColor(UIColor.black, for: .normal)
        
        imgIsVehicleDrivable.image = UIImage(named: "swich_off.png")
        imgNeedRide.image = UIImage(named: "swich_off.png")
        imgNeedTow.image = UIImage(named: "swich_off.png")
        imgHighPriority.image = UIImage(named: "swich_off.png")
        
        btnIsVehicleDrivable.isSelected = false
        btnNeedRide.isSelected = false
        btnNeedTow.isSelected = false
        btnHighPriority.isSelected = false
        
        btnPicture1.border(UIColor.lightGray, 0, 1)
        btnPicture2.border(UIColor.lightGray, 0, 1)
        btnPicture3.border(UIColor.lightGray, 0, 1)
        btnPicture4.border(UIColor.lightGray, 0, 1)
        tfIssueDescription.border(UIColor.clear, 3, 1)
        btnUploadFile.border(UIColor.clear, 3, 1)
        
        let imgN = imageWithImage(image: UIImage(named: "cross.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
        btnDecline.setImage(imgN, for: .normal)
    }
    
    func getGallery(){
        self.hideKeyboard()
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
            print("camera selected")
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
            print("gallery selected")
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    //MARK:- Button Actions
    
    @IBAction func btnActionAddPictures(_ sender: Any) {
        self.hideKeyboard()
        
        print("(sender as AnyObject).tag >>\((sender as AnyObject).tag )")
        switch (sender as AnyObject).tag {
            
        case 101:
            strImageSelected = "picture_1"
        case 102:
            strImageSelected = "picture_2"
        case 103:
            strImageSelected = "picture_3"
        case 104:
            strImageSelected = "picture_4"
        default:
            print("Default case")
        }
        
        self.getGallery()
    }
    
    @IBAction func btnUploadFile(_ sender: Any) {
        strImageSelected = "file"
        self.getGallery()
    }
    
    @IBAction func btnIsVehicleDrivable(_ sender: Any) {
        self.hideKeyboard()
        if btnIsVehicleDrivable.isSelected{
            imgIsVehicleDrivable.image = UIImage(named: "swich_off.png")
            btnIsVehicleDrivable.isSelected = false
        }else{
            imgIsVehicleDrivable.image =  UIImage(named: "swich_on.png")
            btnIsVehicleDrivable.isSelected = true
        }
        
        self.manageStatusView()
    }
    
    @IBAction func btnNeedRide(_ sender: Any) {
        self.hideKeyboard()
        if btnNeedRide.isSelected{
            imgNeedRide.image = UIImage(named: "swich_off.png")
            btnNeedRide.isSelected = false
        }else{
            imgNeedRide.image =  UIImage(named: "swich_on.png")
            btnNeedRide.isSelected = true
        }
    }
    
    @IBAction func btnNeedTow(_ sender: Any) {
        self.hideKeyboard()
        if btnNeedTow.isSelected{
            imgNeedTow.image =  UIImage(named: "swich_off.png")
            btnNeedTow.isSelected = false
        }else{
            imgNeedTow.image = UIImage(named: "swich_on.png")
            btnNeedTow.isSelected = true
        }
    }
    
    @IBAction func btnHighPriority(_ sender: Any) {
        self.hideKeyboard()
        if btnHighPriority.isSelected{
            imgHighPriority.image =  UIImage(named: "swich_off.png")
            btnHighPriority.isSelected = false
        }else{
            imgHighPriority.image = UIImage(named: "swich_on.png")
            btnHighPriority.isSelected = true
        }
    }
    
    @IBAction func btnRequestQuote(_ sender: Any) {
        self.hideKeyboard()
        
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            wsEstimateRequest()
        }
    }
    //Button Action For Status
    
    @IBAction func btnAccept(_ sender: Any) {
        let params = NSMutableDictionary()
        params["status"] = "Accepted"
        params["estimate_id"] = estimateId
        
        Http.instance().json(api_acceptDenyEstimate, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyEstimateVC") as! MyEstimateVC
                    self.navigationController?.pushViewController(vc, animated: false)
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
    }
    
    @IBAction func btnDecline(_ sender: Any) {
        let params = NSMutableDictionary()
        params["status"] = "Decline"
        params["estimate_id"] = estimateId
        
        Http.instance().json(api_acceptDenyEstimate, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyEstimateVC") as! MyEstimateVC
                    self.navigationController?.pushViewController(vc, animated: false)
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
    }
    
    //MARK:- CAMERA AND GALLARY FUNCTION
    func camera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    //save image
    func saveImage(imageName: String,imgImage:UIImage){
        //create an instance of the FileManager
        let fileManager = FileManager.default
        //get the image path
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        //get the image we took with camera
        let image = imgImage//imageView.image!
        //get the PNG data for this image
        let data = UIImagePNGRepresentation(image)
        //store it in the document directory
        fileManager.createFile(atPath: imagePath as String, contents: data, attributes: nil)
    }
    
    func getImage(imageName: String){
        let fileManager = FileManager.default
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: imagePath){
            imgPicture2.image = UIImage(contentsOfFile: imagePath)
            
        }else{
            print("Panic! No Image!")
        }
    }
    func savedImageAlert()
    {
        let alert:UIAlertView = UIAlertView()
        alert.title = "Saved!"
        alert.message = "Your picture was saved to Camera Roll"
        alert.delegate = self
        alert.addButton(withTitle: "Ok")
        alert.show()
    }
    
    //MARK : CAMERAVIEW DELEGATES
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let img:UIImage? = info[UIImagePickerControllerEditedImage] as? UIImage
        var img_name = ""
        
        if picker.sourceType == .photoLibrary{
            if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
                let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
                let asset = result.firstObject
                let img_name1 = asset?.value(forKey: "filename")! as! String
                img_name = "\(img_name1)"
                print("neeleshwari Estimate image name :---->>>>>\(img_name)")
            }
        }else if(picker.sourceType == UIImagePickerControllerSourceType.camera){
            switch strImageSelected {
                
            case "picture_1":
                img_name = "picture_1.png"
            case "picture_2":
                img_name = "picture_2.png"
            case "picture_3":
                img_name = "picture_3.png"
            case "picture_4":
                img_name = "picture_4.png"
            case "file":
                img_name = "picture_5.png"
            default:
                print("Default case")
            }
            saveImage(imageName: img_name,imgImage:img!)
            // Access the uncropped image from info dictionary
            let imageToSave1: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage //same but with different way
            UIImageWriteToSavedPhotosAlbum(imageToSave1, nil, nil, nil)
            self.savedImageAlert()
        }
        print("neeleshwari Estimate image name :---->>>>>\(img_name)")
        //===========================================
        if (img != nil) {
          //  print("strImageSelected>>\(strImageSelected)")
            switch strImageSelected {
                
            case "picture_1":
                imgPic1 = img?.resize(300.0)
                imgPicture1.image = img?.resize(300.0)
                imgNamePic1 = img_name
                
            case "picture_2":
                imgPic2 = img?.resize(300.0)
                imgPicture2.image = img?.resize(300.0)
                imgNamePic2 = img_name
                
            case "picture_3":
                imgPic3 = img?.resize(300.0)
                imgPicture3.image = img?.resize(300.0)
                imgNamePic3 = img_name
                
            case "picture_4":
                imgPic4 = img?.resize(300.0)
                imgPicture4.image = img?.resize(300.0)
                imgNamePic4 = img_name
                
            case "file":
                imgPictureFile = img?.resize(300.0)
                btnUploadFile.setTitle("Image Uploaded", for: .normal)
                imgPictureFileName = img_name
                
            default:
                print("Default case")
            }
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    func checkValidation() -> String? {
        if tfIssueDescription.text?.count == 0  {
            return AlertMSG.blankIssueDetails
        }
        
        return nil
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    //MARK:- TEXTFIELD_DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tfIssueDescription.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfIssueDescription.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var length = (textField.text?.count)! + string.count - range.length
        let characterSet = CharacterSet.init(charactersIn: kAppDelegate.ACCEPTABLE_CHARACTERSWITHNO).inverted
        length = 250
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        let filter = string.components(separatedBy: characterSet).joined(separator:"")
        return ((string == filter) && newString.length <= length)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    /////////__________***********************_________---------------
    //MARK:- KEYBOARD NOTIFICATION METHODS
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrlView.contentInset = UIEdgeInsets.zero
        } else {
            scrlView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        
        scrlView.scrollIndicatorInsets = scrlView.contentInset
    }
    
    //MARK:--------------- WS Estimate Request Create ----------------//
    func wsEstimateRequest() {
        let params = NSMutableDictionary()
        let arrImages = NSMutableArray()
        let arrImgData = NSMutableArray()
        
        print("imgNamePic1---...\(imgNamePic1)")
        print("imgNamePic2---...\(imgNamePic2)")
        print("imgNamePic3---...\(imgNamePic3)")
        print("imgNamePic4---...\(imgNamePic4)")
        
        if imgPic1 != nil {
            let imageData:NSData = UIImagePNGRepresentation(imgPicture1.image!)! as NSData
            let imageStr = "data:image/jpeg;base64," + imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            arrImages.add(NSMutableDictionary(dictionary: ["image":imageStr,"name":imgNamePic1,"order":"image_1"]))
        }
        
        if imgPic2 != nil {
            
            let imageData:NSData = UIImagePNGRepresentation(imgPicture2.image!)! as NSData
            let imageStr = "data:image/jpeg;base64," + imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            arrImages.add(NSMutableDictionary(dictionary: ["image":imageStr,"name":imgNamePic2,"order":"image_2"]))
        }
        
        if imgPic3 != nil {
            
            let imageData:NSData = UIImagePNGRepresentation(imgPicture3.image!)! as NSData
            let imageStr = "data:image/jpeg;base64," + imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            arrImages.add(NSMutableDictionary(dictionary: ["image":imageStr,"name":imgNamePic3,"order":"image_3"]))
        }
        
        if imgPic4 != nil {
            
            let imageData:NSData = UIImagePNGRepresentation(imgPicture4.image!)! as NSData
            let imageStr = "data:image/jpeg;base64," + imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            arrImages.add(NSMutableDictionary(dictionary: ["image":imageStr,"name":imgNamePic4,"order":"image_4"]))
        }
        
        if imgPictureFile != nil {
            
            let imageData:NSData = UIImagePNGRepresentation(imgPictureFile!)! as NSData
            let imageStr = "data:image/jpeg;base64," + imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            
            arrImgData.add(NSMutableDictionary(dictionary: ["file":imageStr,"name":imgPictureFileName,"order":"file_1"]))
        }
        
        var is_drivable = ""
        var need_ride = ""
        var is_tow_needed = ""
        var high_priority = ""
        
        if btnIsVehicleDrivable.isSelected {
            is_drivable = "yes"
        }else{
            is_drivable = "no"
        }
        
        if btnNeedRide.isSelected {
            need_ride = "yes"
        }else{
            need_ride = "no"
        }
        
        if btnNeedTow.isSelected {
            is_tow_needed = "yes"
        }else{
            is_tow_needed = "no"
        }
        
        if btnHighPriority.isSelected {
            high_priority = "yes"
        }else{
            high_priority = "no"
        }
        
        let defaults = UserDefaults.standard
        let vehicle_id = defaults.value(forKey: "savedId")
        
        params["vehicle_id"] = vehicle_id
        params["shop_id"] = shop_id
        params["is_drivable"] = is_drivable
        params["need_ride"] = need_ride
        params["is_tow_needed"] = is_tow_needed
        params["high_priority"] = high_priority
        params["description"] = tfIssueDescription.text!
        params["images"] = arrImages.string()
        params["files"] = arrImgData.string()
        
        print("params>>>\(params)")
        
        Http.instance().json(api_estimate_request, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    kAppDelegate.arrGLVehicleList = NSMutableArray()
                    Http.alert("", string(json! as! NSDictionary, "message"))
                    self.delegatesAndDisplayView()
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyEstimateVC") as! MyEstimateVC
                    self.navigationController?.pushViewController(vc, animated: false)
                    
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
    }
    
    //---------------------------get Estimate Detail ----------------------//
    func wsGetEstimate() {
        
        let params = NSMutableDictionary()
        params["estimate_id"] = estimateId
        
        Http.instance().json(api_get_estimate, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            print("json>>>>\(json!)")
            
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    
                    if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                        let dict = result.object(at: 0) as! NSDictionary
                        
                        self.tfIssueDescription.text! = string(dict, "description").capFirstLetter()
                        self.tfIssueDescription.isUserInteractionEnabled = false
                        self.lblStatus.text! = "Status: " + string(dict, "status").capFirstLetter()
                        self.lblEstimatePrice.text! = "$" + string(dict, "estimate_price")
                        
                        if string(dict, "is_drivable").lowercased() == "no" {
                            self.imgIsVehicleDrivable.image = UIImage(named: "swich_off.png")
                        } else {
                            self.imgIsVehicleDrivable.image = UIImage(named: "swich_on.png")
                        }
                        
                        if string(dict, "is_tow_needed").lowercased() == "no" {
                            self.imgNeedTow.image = UIImage(named: "swich_off.png")
                        } else {
                            self.imgNeedTow.image = UIImage(named: "swich_on.png")
                        }
                        
                        if string(dict, "high_priority").lowercased() == "no" {
                            self.imgHighPriority.image = UIImage(named: "swich_off.png")
                        } else {
                            self.imgHighPriority.image = UIImage(named: "swich_on.png")
                        }
                        
                        if string(dict, "need_ride").lowercased() == "no" {
                            self.imgNeedRide.image = UIImage(named: "swich_off.png")
                        } else {
                            self.imgNeedRide.image = UIImage(named: "swich_on.png")
                        }
                        
                        if let arrImg = dict.object(forKey: "images") as? NSArray {
                            self.imgPicture1.image = UIImage(named: "camera.png")
                            self.imgPicture2.image = UIImage(named: "camera.png")
                            self.imgPicture3.image = UIImage(named: "camera.png")
                            self.imgPicture4.image = UIImage(named: "camera.png")
                            
                            if arrImg.count == 1 {
                                let first = arrImg.object(at: 0) as! NSDictionary
                                let vehicle_image1 = string(first, "filename")
                                let Sachin1 = (kAppDelegate.server?.base_url)! + vehicle_image1
                                
                                if vehicle_image1 != "" {
                                    let url = NSURL(string: Sachin1)
                                    self.imgPicture1.af_setImage(withURL: url as URL!)
                                }
                                
                            } else if arrImg.count == 2 {
                                
                                let first = arrImg.object(at: 0) as! NSDictionary
                                let vehicle_image1 = string(first, "filename")
                                let Sachin1 = (kAppDelegate.server?.base_url)! + vehicle_image1
                                if vehicle_image1 != "" {
                                    let url = NSURL(string: Sachin1)
                                    self.imgPicture1.af_setImage(withURL: url as URL!)
                                }
                                
                                let second = arrImg.object(at: 1) as! NSDictionary
                                let vehicle_image2 = string(second, "filename")
                                let Sachin2 = (kAppDelegate.server?.base_url)! + vehicle_image2
                                
                                if vehicle_image2 != "" {
                                    let url = NSURL(string: Sachin2)
                                    self.imgPicture2.af_setImage(withURL: url as URL!)
                                }
                                
                            } else if arrImg.count == 3 {
                                
                                let first = arrImg.object(at: 0) as! NSDictionary
                                let vehicle_image1 = string(first, "filename")
                                let Sachin1 = (kAppDelegate.server?.base_url)! + vehicle_image1
                                if vehicle_image1 != "" {
                                    let url = NSURL(string: Sachin1)
                                    self.imgPicture1.af_setImage(withURL: url as URL!)
                                }
                                
                                let second = arrImg.object(at: 1) as! NSDictionary
                                let vehicle_image2 = string(second, "filename")
                                let Sachin2 = (kAppDelegate.server?.base_url)! + vehicle_image2
                                
                                if vehicle_image2 != "" {
                                    let url = NSURL(string: Sachin2)
                                    self.imgPicture2.af_setImage(withURL: url as URL!)
                                }
                                
                                let third = arrImg.object(at: 2) as! NSDictionary
                                let vehicle_image3 = string(third, "filename")
                                let Sachin3 = (kAppDelegate.server?.base_url)! + vehicle_image3
                                
                                if vehicle_image3 != "" {
                                    let url = NSURL(string: Sachin3)
                                    self.imgPicture3.af_setImage(withURL: url as URL!)
                                }
                                
                            } else if arrImg.count == 4 {
                                
                                let first = arrImg.object(at: 0) as! NSDictionary
                                let vehicle_image1 = string(first, "filename")
                                let Sachin1 = (kAppDelegate.server?.base_url)! + vehicle_image1
                                
                                if vehicle_image1 != "" {
                                    let url = NSURL(string: Sachin1)
                                    self.imgPicture1.af_setImage(withURL: url as URL!)
                                }
                                
                                let second = arrImg.object(at: 1) as! NSDictionary
                                let vehicle_image2 = string(second, "filename")
                                let Sachin2 = (kAppDelegate.server?.base_url)! + vehicle_image2
                                
                                if vehicle_image2 != "" {
                                    let url = NSURL(string: Sachin2)
                                    self.imgPicture2.af_setImage(withURL: url as URL!)
                                }
                                
                                let third = arrImg.object(at: 2) as! NSDictionary
                                let vehicle_image3 = string(third, "filename")
                                let Sachin3 = (kAppDelegate.server?.base_url)! + vehicle_image3
                                
                                if vehicle_image3 != "" {
                                    let url = NSURL(string: Sachin3)
                                    self.imgPicture3.af_setImage(withURL: url as URL!)
                                }
                                
                                let fourth = arrImg.object(at: 3) as! NSDictionary
                                let vehicle_image4 = string(fourth, "filename")
                                let Sachin4 = (kAppDelegate.server?.base_url)! + vehicle_image4
                                if vehicle_image4 != "" {
                                    let url = NSURL(string: Sachin4)
                                    self.imgPicture4.af_setImage(withURL: url as URL!)
                                }
                            }
                        }
                    }
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
    }
}//Sachin :]


//====================================================================


import Foundation
import Photos

class CustomPhotoAlbum: NSObject {
    static let albumName = "Telenyze Photos"
    static let sharedInstance = CustomPhotoAlbum()
    var assetCollection: PHAssetCollection!
    
    override init() {
        super.init()
        
        if let assetCollection = fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
            return
        }
        
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                ()
            })
        }
        
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            self.createAlbum()
        } else {
            PHPhotoLibrary.requestAuthorization(requestAuthorizationHandler)
        }
    }
    
    func requestAuthorizationHandler(status: PHAuthorizationStatus) {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            // ideally this ensures the creation of the photo album even if authorization wasn't prompted till after init was done
            print("trying again to create the album")
            self.createAlbum()
        } else {
            print("should really prompt the user to let them know it's failed")
        }
    }
    
    func createAlbum() {
        PHPhotoLibrary.shared().performChanges({
            PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: CustomPhotoAlbum.albumName)   // create an asset collection with the album name
        }) { success, error in
            if success {
                self.assetCollection = self.fetchAssetCollectionForAlbum()
            } else {
                print("error \(error)")
            }
        }
    }
    
    func fetchAssetCollectionForAlbum() -> PHAssetCollection? {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", CustomPhotoAlbum.albumName)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        
        if let _: AnyObject = collection.firstObject {
            return collection.firstObject
        }
        return nil
    }
    
    func save(image: UIImage) {
        if assetCollection == nil {
            return                          // if there was an error upstream, skip the save
        }
        
        PHPhotoLibrary.shared().performChanges({
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            let assetPlaceHolder = assetChangeRequest.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
            let enumeration: NSArray = [assetPlaceHolder!]
            albumChangeRequest!.addAssets(enumeration)
            
        }, completionHandler: nil)
    }
}

