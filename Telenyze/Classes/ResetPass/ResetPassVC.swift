//
//  ResetPassVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 02/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class ResetPassVC: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet weak var viewSub: UIView!
    @IBOutlet var tfNewPass: SkyFloatingLabelTextField!
    @IBOutlet var tfConfirmPass: SkyFloatingLabelTextField!
    @IBOutlet var tfOtpCode: SkyFloatingLabelTextField!
    @IBOutlet var btnSubmit: UIButton!
    
    //MARK:- VARIABLES
    var email = ""
    var username = ""
    let HexColor = UIColor(hex: "D43D21")
    var strEmailUserName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(strEmailUserName)
        let string1 = strEmailUserName
        
        if (string1 as NSString).contains("@") {
            username = ""
        } else {
            email = ""
        }
        
        addDoneButtonOnKeyboard()
        self.delegatesAndDisplayView()
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tfNewPass.placeHolderColor = UIColor.white
        tfConfirmPass.placeHolderColor = UIColor.white
        tfOtpCode.placeHolderColor = UIColor.white
        
        self.navigationController?.isNavigationBarHidden = false
        self.BackNavigationButton()
        setNavTransparent(className: "Reset Password")
        self.hideKeyboard()
        
    }
    
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionBackButton()  {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
        self.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: ACTIONS.
    @IBAction func actionSubmit(_ sender: Any) {
        self.hideKeyboard()
        
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            self.wsResetPassword()
        }
    }
    
    //MARK: FUNCTIONS.
    func hideKeyboard() {
        self.view.endEditing(true)
        scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scroll.contentInset = UIEdgeInsets.zero
    }
    
    func delegatesAndDisplayView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.delegate = self
        self.viewSub.addGestureRecognizer(tap)
        
        tfNewPass.delegate = self
        tfConfirmPass.delegate = self
        tfOtpCode.delegate = self
    }
    
    //MARK:- TEXTFIELD_DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func checkValidation() -> String? {
        if tfNewPass.text?.count == 0 {
            return AlertMSG.blankNewPassword
        } else if (tfNewPass.text?.count)! < 6  || (tfConfirmPass.text?.count)! < 6  {
            return AlertMSG.passwordLength
        } else if tfConfirmPass.text?.count == 0 {
            return AlertMSG.blankConfirmPassword
        } else if tfNewPass.text != tfConfirmPass.text {
            return AlertMSG.pwdMissMatch
        } else if tfOtpCode.text?.count == 0 {
            return AlertMSG.blankOTPField
        }
        
        return nil
    }
    
    //MARK: TextField Delegates.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if tfNewPass == textField {
            tfConfirmPass.becomeFirstResponder()
        } else if textField == tfConfirmPass {
            tfOtpCode.becomeFirstResponder()
        } else if textField == tfOtpCode {
            tfOtpCode.resignFirstResponder()
            scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        
        if textField == tfNewPass {
            return (length > 30) ? false : true
        } else if textField == tfConfirmPass {
            return (length > 30) ? false : true
        } else if textField == tfOtpCode {
            return (length > 20) ? false : true
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scroll.contentSize = CGSize(width: view.frame.size.width, height: btnSubmit.frame.origin.y + btnSubmit.frame.size.height + 10)
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = appColor.appPrimaryColor
        doneToolbar.tintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        //let next: UIBarButtonItem = UIBarButtonItem(title: "NEXT", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.nextOfDoneTool))
        let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.hideKeyboard))
        
        var items:[UIBarButtonItem] = []
        //items.append(next)
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.tfOtpCode.inputAccessoryView = doneToolbar
    }
    
    func done() {
        tfOtpCode.resignFirstResponder()
        scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    /////////__________***********************_________
    //MARK:- KEYBOARD NOTIFICATION METHODS
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scroll.contentInset = UIEdgeInsets.zero
        } else {
            scroll.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        
        scroll.scrollIndicatorInsets = scroll.contentInset
    }
    /////////__________***********************_________
    
    //MARK: ws ResetPassword
    func wsResetPassword(){
        
        let params = NSMutableDictionary()
        let string1 = strEmailUserName
        
        print("string1-----\(string1)")
        
        if (string1 as NSString).contains("@") {
            email = strEmailUserName
            username = ""
            print("email-----\(email)")
            
        }else {
            email = ""
            username = strEmailUserName
            print("username-----\(username)")
        }
        
        params["email"] = email
        params["username"] = username
        params["new_password"] = tfNewPass.text!
        params["verify_code"] = tfOtpCode.text!
        
        Http.instance().json(api_reset_password, params, "POST", ai: true, popup: true, prnt: true) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // Http.alert("", string(json! , "message"))
                    
                    let viewControllers = self.navigationController!.viewControllers as [UIViewController]
                    for aViewController:UIViewController in viewControllers {
                        if aViewController.isKind(of: SignInVC.self) {
                            _ = self.navigationController?.popToViewController(aViewController, animated: true)
                        }
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    /*
     override var prefersStatusBarHidden: Bool {
     return true
     }
     */
}//Sachin :]

