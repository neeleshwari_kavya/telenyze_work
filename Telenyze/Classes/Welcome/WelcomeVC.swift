//
//  WelcomeVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 01/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4
import AVFoundation
import AVKit
import youtube_ios_player_helper

class WelcomeVC: UIViewController ,YTPlayerViewDelegate {
    
    //Outlets
    @IBOutlet var viewYTPlayer: YTPlayerView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var btnGetStart: UIButton!
    @IBOutlet var subView: UIView!
    
    //Variables
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Fuctions
    func displayAndDelegates(){
        self.navigationController?.isNavigationBarHidden = true
        viewYTPlayer.delegate = self
        activityIndicator.isHidden = true
        viewYTPlayer.backgroundColor = UIColor.clear
    }
    
    //MARK:- Button Actions
    @IBAction func actionPlay(_ sender: Any) {
        // self.playVideoFile()
        self.playYouTubeVideo()
    }
    
    @IBAction func actionGetStart(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //Mark: play YouTube Video ----********
    func playYouTubeVideo()  {
        
        var IsYoutube : String?
        // IsYoutube = string(PredefinedConstants.appDelegate.dictBaseUrl, "video_url").youtubeID
        IsYoutube = "TJ1SDXbij8Y"
        print((String(IsYoutube!)!))
        activityIndicator.isHidden = false
        if IsYoutube != nil{
            
            viewYTPlayer.delegate = self
            //  let playerVars = ["controls": 1, "playsinline": 0, "autohide": 0, "showinfo": 0, "modestbranding": 0,"autoplay":1,"fs":0,"f":1,"allowfullscreen":1,"playlist":"\(String(IsYoutube!)!)",] as [String : Any]
            
            let playerVars = [
                "modestbranding" : "1",
                "controls" : "2",
                "playsinline" : "1",
                "autohide" : "1",
                "showinfo" : "0",
                "autoplay" : "1",
                "fs" : "1",
                "rel" : "0",
                "loop" : "0",
                "enablejsapi" : "1",
                "iv_load_policy": "3",
                "playlist":"\(String(IsYoutube!)!)",
                ] as [String : Any]
            
            viewYTPlayer.load(withVideoId: "\(String(IsYoutube!)!)", playerVars: playerVars as [AnyHashable : Any])
            
        }else{
            print("Invalid url")
            
            /*
             let str = string(PredefinedConstants.appDelegate.dictBaseUrl, "video_url")
             if str != "" {
             let url = URL(string: "http://app.checkinscan.com/client/auth/test2")
             print(url)
             let YOUTUBE =  URLRequest(url:url!)
             print(YOUTUBE)
             //  webView.loadRequest(YOUTUBE)
             } else {
             Http.alert(ValidationsMessages().VideoNot(), "")
             }
             */
        }
    }
    //MARK:- YouTube Delegatess
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        viewYTPlayer.playVideo()
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo quality: YTPlaybackQuality) {
    }
    
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    //Mark: Play video file ----********
    func playVideoFile() {
        guard let path = Bundle.main.path(forResource: "HelloVideo", ofType:"3gp") else {
            debugPrint("HelloVideo.3gp not found")
            return
        }
        
        let url = URL(fileURLWithPath: path)
        player = AVPlayer(url: url)
        avpController.player = player
        self.addChildViewController(avpController)
        // Add your view Frame
        avpController.view.frame = self.viewYTPlayer.bounds
        // Add sub view in your view
        self.viewYTPlayer.addSubview(avpController.view)
        self.player.play()
    }
    
    //    override var prefersStatusBarHidden: Bool {
    //        return true
    //    }
    
}//Sachin :]
/////////__________************************

//MARK:- HEX_COLOR_CODE
extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

//MARK:- SET_BACK_BUTTON_VIEW_CONTOLLER
extension UIViewController {
    
    func setBackButton() {
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "BackButton")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "BackButton")
    }
}

//MARK:- SET_MENU_BUTTON_VIEW_CONTOLLER
extension UIViewController {
    
    func setTransparentBg() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
    }
}

//MARK:- SET_MENU_BUTTON_VIEW_CONTOLLER
extension UIViewController {
    
    func setMenuButton() {
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "MenuButton")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "MenuButton")
    }
}

extension String {
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, options: [], range: range) else {
            return nil
        }
        return (self as NSString).substring(with: result.range)
    }
}

extension UIViewController {
    
    func setNavigationView(className:String) {
        
        self.navigationItem.title = className
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = appColor.appPrimaryColor
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.titleTextAttributes =  [NSForegroundColorAttributeName:UIColor.white]
        self.navigationItem.hidesBackButton = false
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
    }
}

extension UIViewController {
    
    func setNav(className:String)  {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = appColor.appPrimaryColor
        
        let titleViewNav = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 100, height: 40))
        let lbl1 = UILabel(frame: CGRect.init(x: 0 , y: 0, width: view.frame.size.width - 100, height: 40))
        lbl1.text = className
        lbl1.font = UIFont.boldSystemFont(ofSize: 16.0)
        lbl1.textColor = UIColor.white
        lbl1.numberOfLines = 1
        titleViewNav.addSubview(lbl1)
        self.navigationItem.titleView = titleViewNav
    }
}

extension UIViewController {
    
    func setNavTransparent(className:String)  {
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        let titleViewNav = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 100, height: 40))
        let lbl1 = UILabel(frame: CGRect.init(x: 0 , y: 0, width: view.frame.size.width - 100, height: 40))
        lbl1.text = className
        lbl1.font = UIFont.boldSystemFont(ofSize: 16.0)
        lbl1.textColor = UIColor.white
        lbl1.numberOfLines = 1
        titleViewNav.addSubview(lbl1)
        self.navigationItem.titleView = titleViewNav
    }
}

extension UITextField {
    
    public func paddingView (_ pad:Int) {
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: pad, height: Int(self.frame.size.height)))
        self.leftView = paddingView;
        self.leftViewMode = UITextFieldViewMode.always
    }
}

extension UIApplication {  // set status bar colour
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
