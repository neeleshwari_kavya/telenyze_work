//
//  AboutVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 1 on 15/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class AboutVC: UIViewController, UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
    @IBOutlet var ActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        webView.isOpaque = false
        webView.backgroundColor = UIColor.clear
        webView.scrollView.showsHorizontalScrollIndicator = false
        self.ws_AboutUs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.BackNavigationButton()
        setNav(className: "About Us")
    }
    
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionBackButton() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- webView delegates
    func webViewDidStartLoad(_ webView : UIWebView) {
        ActivityIndicator.startAnimating()
        ActivityIndicator.isHidden = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ActivityIndicator.stopAnimating()
        ActivityIndicator.isHidden = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        ActivityIndicator.isHidden = true
    }
  
    func ws_AboutUs() {
        
        let params = NSMutableDictionary()
        params["slug"] = "about-us"
        
        Http.instance().json(api_terms_aboutUs_privacyPolicy, params, "POST", ai: false, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            print(json!)
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    if let result = (json as AnyObject).object(forKey: "result") as? NSDictionary {
                        
                        if let description = result.object(forKey: "description") {
                            let myVariable = "<font face='regular' size='3' color= 'white'>%@"
                            let Sachin = String(format: myVariable, description as! CVarArg)
                            self.webView.loadHTMLString(Sachin, baseURL: nil)
                        }
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
}//Sachin :]
