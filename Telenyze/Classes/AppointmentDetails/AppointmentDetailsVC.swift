//
//  AppointmentDetailsVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 2 on 23/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import Foundation
import HarishFrameworkSwift4

class AppointmentDetailsVC: UIViewController {
    
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    
    //appointmentdetails
    @IBOutlet var viewStatus: View!
    @IBOutlet var lblAppointmentName: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblStatusImg: UILabel!
    
    // car details
    @IBOutlet var viewCarDetails: UIView!
    @IBOutlet var imgCamera: UIImageView!
    @IBOutlet var lblCarName: UILabel!
    @IBOutlet var lblVin: UILabel!
    @IBOutlet var lblColorText: UILabel!
    @IBOutlet var lblObd: UILabel!
    @IBOutlet var lblCarColor: UILabel!

    //estimate request details
    @IBOutlet var imgPicture1: UIImageView!
    @IBOutlet var imgPicture2: UIImageView!
    @IBOutlet var imgPicture3: UIImageView!
    @IBOutlet var imgPicture4: UIImageView!
    
    @IBOutlet var btnPicture1: UIButton!
    @IBOutlet var btnPicture2: UIButton!
    @IBOutlet var btnPicture3: UIButton!
    @IBOutlet var btnPicture4: UIButton!
    
    @IBOutlet var tfIssueDescription: UITextField!
    @IBOutlet var imgFile: UIImageView!
    @IBOutlet var btnUploadFile: UIButton!
    
    @IBOutlet var imgIsVehicleDrivable: UIImageView!
    @IBOutlet var btnIsVehicleDrivable: UIButton!
    @IBOutlet var imgNeedRide: UIImageView!
    @IBOutlet var btnNeedRide: UIButton!
    @IBOutlet var imgNeedTow: UIImageView!
    @IBOutlet var btnNeedTow: UIButton!
    @IBOutlet var imgHighPriority: UIImageView!
    @IBOutlet var btnHighPriority: UIButton!
    @IBOutlet var btnRescheduleAppointment: UIButton!
    @IBOutlet var viewAcceptDecline: UIView!
    @IBOutlet var btnDecline: UIButton!
    @IBOutlet var btnAccept: UIButton!
 
    
    //Mark:- variables
    var estimate_id = ""
    var showStatusView = false
    var appointmentStatus = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegatesAndDisplayView()
        self.wsGetEstimate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.BackNavigationButton()
        setNav(className: "Appointment Detail")
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionBackButton() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Button Action
    @IBAction func btnRescheduleAppointment(_ sender: Any) {
        /*
         self.hideKeyboard()
         
         if let str = checkValidation() {
         Http.alert("", str)
         } else {
         wsEstimateRequest()
         }
 */
    }
    
    @IBAction func btnDecline(_ sender: Any) {
        /*
        let params = NSMutableDictionary()
        params["status"] = "Decline"
        params["estimate_id"] = estimateId
        
        Http.instance().json(api_acceptDenyEstimate, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyEstimateVC") as! MyEstimateVC
                    self.navigationController?.pushViewController(vc, animated: false)
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
 */
    }
    
    @IBAction func btnAccept(_ sender: Any) {
        /*
        let params = NSMutableDictionary()
        params["status"] = "Accepted"
        params["estimate_id"] = estimateId
        
        Http.instance().json(api_acceptDenyEstimate, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyEstimateVC") as! MyEstimateVC
                    self.navigationController?.pushViewController(vc, animated: false)
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
 */
    }
    
    //MARK:- Fuctions

    func delegatesAndDisplayView() {

        tfIssueDescription.text = ""
        tfIssueDescription.placeholder = "Describe issues as best as possible"
        tfIssueDescription.placeHolderColor = UIColor.lightGray
        tfIssueDescription.paddingView(8)
        btnUploadFile.setTitle("Files uploaded", for: .normal)
        btnUploadFile.setTitleColor(UIColor.black, for: .normal)

        imgIsVehicleDrivable.image = UIImage(named: "swich_off.png")
        imgNeedRide.image = UIImage(named: "swich_off.png")
        imgNeedTow.image = UIImage(named: "swich_off.png")
        imgHighPriority.image = UIImage(named: "swich_off.png")
        
        btnIsVehicleDrivable.isSelected = false
        btnNeedRide.isSelected = false
        btnNeedTow.isSelected = false
        btnHighPriority.isSelected = false
        
        tfIssueDescription.border(UIColor.clear, 3, 1)
        viewStatus.border(UIColor.clear, 3, 1)
        viewCarDetails.border(UIColor.clear, 3, 1)
        btnUploadFile.border(UIColor.clear, 3, 1)
    }
  
    func manageAppointmentStatusView()  {
        /*
         appointmentStatus
        if status == "Quoted" {
            showStatusView = true
            self.wsGetEstimate()
        } else {
            showStatusView = false
        }
        
        if showStatusView {
    
        } else {

        }
        */
        
    }
    
    
    //MARK:- WS GetEstimate ---------------------------------------------------------------
    func wsGetEstimate() {

        let params = NSMutableDictionary()
        params["estimate_id"] = estimate_id
        
        Http.instance().json(api_get_estimate, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    self.delegatesAndDisplayView()
                    
                    if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                        let dict = result.object(at: 0) as! NSDictionary
                        
                        //appointmentdetails
                        self.lblAppointmentName.text! = "My Appoinment"
                        self.lblDate.text! = string(dict, "appointment_datetime").converDate("yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy")
                        self.lblTime.text! = string(dict, "appointment_datetime").converDate("yyyy-MM-dd HH:mm:ss", "HH:mm")
                       // self.lblStatus.text! = string(dict, "status")///
                        
                         self.lblStatus.text! = string(dict, "appointment_status")
                        // car details
                        self.lblVin.text! = string(dict, "vin_number")
                        let strCode = string(dict, "color_code")
                        let colorCode = strCode.replacingOccurrences(of: "#", with: "")
                        self.lblCarColor.backgroundColor = UIColor(hex: colorCode)
                        self.lblColorText.text! = string(dict, "color")
                        self.lblObd.text! = string(dict, "make")
                        self.tfIssueDescription.text! = string(dict, "description")
                        self.tfIssueDescription.isUserInteractionEnabled = false
                        
                        if string(dict, "is_drivable").lowercased() == "no" {
                             self.imgIsVehicleDrivable.image = UIImage(named: "swich_off.png")
                        } else {
                            self.imgIsVehicleDrivable.image = UIImage(named: "swich_on.png")
                        }
                        
                        if string(dict, "is_tow_needed").lowercased() == "no" {
                            self.imgNeedTow.image = UIImage(named: "swich_off.png")
                        } else {
                            self.imgNeedTow.image = UIImage(named: "swich_on.png")
                        }
                        
                        if string(dict, "high_priority").lowercased() == "no" {
                            self.imgHighPriority.image = UIImage(named: "swich_off.png")
                        } else {
                            self.imgHighPriority.image = UIImage(named: "swich_on.png")
                        }
                        
                        if string(dict, "need_ride").lowercased() == "no" {
                            self.imgNeedRide.image = UIImage(named: "swich_off.png")
                        } else {
                            self.imgNeedRide.image = UIImage(named: "swich_on.png")
                        }
                        
                        if let arrImg = dict.object(forKey: "images") as? NSArray {
                            self.imgPicture1.image = UIImage(named: "camera.png")
                            self.imgPicture2.image = UIImage(named: "camera.png")
                            self.imgPicture3.image = UIImage(named: "camera.png")
                            self.imgPicture4.image = UIImage(named: "camera.png")
                            
                           if arrImg.count == 1 {
                                let first = arrImg.object(at: 0) as! NSDictionary
                                let vehicle_image1 = string(first, "filename")
                                let Sachin1 = (kAppDelegate.server?.base_url)! + vehicle_image1
                                
                                if vehicle_image1 != "" {
                                    let url = NSURL(string: Sachin1)
                                    self.imgPicture1.af_setImage(withURL: url as URL!)
                                }
                              
                            } else if arrImg.count == 2 {
                                
                                let first = arrImg.object(at: 0) as! NSDictionary
                                let vehicle_image1 = string(first, "filename")
                                let Sachin1 = (kAppDelegate.server?.base_url)! + vehicle_image1
                                if vehicle_image1 != "" {
                                    let url = NSURL(string: Sachin1)
                                    self.imgPicture1.af_setImage(withURL: url as URL!)
                                }

                                let second = arrImg.object(at: 1) as! NSDictionary
                                let vehicle_image2 = string(second, "filename")
                                let Sachin2 = (kAppDelegate.server?.base_url)! + vehicle_image2
                                
                                if vehicle_image2 != "" {
                                    let url = NSURL(string: Sachin2)
                                    self.imgPicture2.af_setImage(withURL: url as URL!)
                                }
                         
                             } else if arrImg.count == 3 {
                                
                                let first = arrImg.object(at: 0) as! NSDictionary
                                let vehicle_image1 = string(first, "filename")
                                let Sachin1 = (kAppDelegate.server?.base_url)! + vehicle_image1
                                if vehicle_image1 != "" {
                                    let url = NSURL(string: Sachin1)
                                    self.imgPicture1.af_setImage(withURL: url as URL!)
                                }
                                
                                let second = arrImg.object(at: 1) as! NSDictionary
                                let vehicle_image2 = string(second, "filename")
                                let Sachin2 = (kAppDelegate.server?.base_url)! + vehicle_image2
                                
                                if vehicle_image2 != "" {
                                    let url = NSURL(string: Sachin2)
                                    self.imgPicture2.af_setImage(withURL: url as URL!)
                                }
                                
                                let third = arrImg.object(at: 2) as! NSDictionary
                                let vehicle_image3 = string(third, "filename")
                                let Sachin3 = (kAppDelegate.server?.base_url)! + vehicle_image3
                                
                                if vehicle_image3 != "" {
                                    let url = NSURL(string: Sachin3)
                                    self.imgPicture3.af_setImage(withURL: url as URL!)
                                }
                                
                             } else if arrImg.count == 4 {
                                
                                let first = arrImg.object(at: 0) as! NSDictionary
                                let vehicle_image1 = string(first, "filename")
                                let Sachin1 = (kAppDelegate.server?.base_url)! + vehicle_image1
                                
                                if vehicle_image1 != "" {
                                    let url = NSURL(string: Sachin1)
                                    self.imgPicture1.af_setImage(withURL: url as URL!)
                                }
                                
                                let second = arrImg.object(at: 1) as! NSDictionary
                                let vehicle_image2 = string(second, "filename")
                                let Sachin2 = (kAppDelegate.server?.base_url)! + vehicle_image2
                                
                                if vehicle_image2 != "" {
                                    let url = NSURL(string: Sachin2)
                                    self.imgPicture2.af_setImage(withURL: url as URL!)
                                }
                                
                                let third = arrImg.object(at: 2) as! NSDictionary
                                let vehicle_image3 = string(third, "filename")
                                let Sachin3 = (kAppDelegate.server?.base_url)! + vehicle_image3
                                
                                if vehicle_image3 != "" {
                                    let url = NSURL(string: Sachin3)
                                    self.imgPicture3.af_setImage(withURL: url as URL!)
                                }
                                
                                let fourth = arrImg.object(at: 3) as! NSDictionary
                                let vehicle_image4 = string(fourth, "filename")
                                let Sachin4 = (kAppDelegate.server?.base_url)! + vehicle_image4
                                if vehicle_image4 != "" {
                                    let url = NSURL(string: Sachin4)
                                    self.imgPicture4.af_setImage(withURL: url as URL!)
                                }
                            }
                        }
                    }
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
    }
}//Sachin :]

