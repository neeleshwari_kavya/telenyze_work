//
//  MyAppointmentsVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 07/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class MyAppointmentsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var lblNoRecord: UILabel!
    
    var arrCurrentAppointment = NSMutableArray()
    var arrPastAppointment = NSMutableArray()
    var boolWsPage = Bool()
    var pageNo = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayAndDelegates()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblNoRecord.isHidden = true
        self.menuNavigationButton()
        setNav(className: "My Appointments")
        pageNo = 1
        wsAppointmentList()
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "MenuButton"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title  = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- FUNCTION
    func displayAndDelegates() {
        tblView.delegate = self
        tblView.dataSource = self
    }
    
    //MARK:- TABLEVIEW_DELEGATE
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return arrCurrentAppointment.count
        } else {
            return arrPastAppointment.count
        }
    }
    
    var dictData = NSDictionary()
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAppointmentCell", for: indexPath) as! MyAppointmentCell
        
        if indexPath.section == 0 {
            
            if arrCurrentAppointment.count != 0 {
                dictData = arrCurrentAppointment.object(at: indexPath.row) as! NSDictionary
                
                if indexPath.row == (arrCurrentAppointment.count - 1) {
                    if !boolWsPage {
                        boolWsPage = true
                        if arrCurrentAppointment.count % 50 == 0 {
                            print("indexPath.row:\(indexPath.row)")
                            pageNo += 1
                            self.wsAppointmentList()
                        }
                    }
                }
            }
        } else {
            if arrPastAppointment.count != 0 {
                dictData = arrPastAppointment.object(at: indexPath.row) as! NSDictionary
                
                if indexPath.row == (arrPastAppointment.count - 1) {
                    if !boolWsPage {
                        boolWsPage = true
                        if arrPastAppointment.count % 50 == 0 {
                            print("indexPath.row:\(indexPath.row)")
                            pageNo += 1
                            self.wsAppointmentList()
                        }
                    }
                }
            }
        }
        
        var dateTime = string(dictData, "appointment_datetime")
        dateTime = dateTime.converDate("yyyy-MM-dd HH:mm:ss", "d MMM yyyy,HH:mm")
        let arr = dateTime.components(separatedBy: ",")
        
        print("dictData>>>\(dictData)")
        
        cell.lblName.text = string(dictData, "shop_name")
        cell.lblStatus.text = "Status: \(string(dictData, "status"))"
        cell.lblDate.text = arr[0]
        cell.lblTime.text = arr[1]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentDetailsVC") as! AppointmentDetailsVC
            dictData = arrCurrentAppointment.object(at: indexPath.row) as! NSDictionary
            vc.estimate_id = string(dictData, "estimate_id")
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentDetailsVC") as! AppointmentDetailsVC
            dictData = arrPastAppointment.object(at: indexPath.row) as! NSDictionary
            vc.estimate_id = string(dictData, "estimate_id")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //tableView viewForHeaderInSection
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var ht = CGFloat()
        if section == 0 {
            if arrCurrentAppointment.count == 0 {
                ht = 0
            }else{
                ht = 40
            }
        } else {
            if arrPastAppointment.count == 0 {
                ht = 0
            }else{
                ht = 40
            }
        }
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.frame.size.width, height: ht))
        
        let lblSectionTitle = UILabel(frame: CGRect(x: 8, y: 0, width: self.tblView.frame.size.width - 8, height: ht))
        lblSectionTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblSectionTitle.textColor = appColor.whiteColorOpeque
        lblSectionTitle.numberOfLines = 1
        
        if section == 0 {
            lblSectionTitle.text = "\(arrCurrentAppointment.count) Current Appointments"
        } else {
            lblSectionTitle.text = "Past Appointments"
        }
        
        lblSectionTitle.textColor = UIColor.white
        viewHeader.addSubview(lblSectionTitle)
        viewHeader.backgroundColor = UIColor.clear
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            if arrCurrentAppointment.count == 0{
                return 0
            }else{
                return 40
            }
        }else{
            if arrPastAppointment.count == 0{
                return 0
            }else{
                return 40
            }
        }
    }
    
    //MARK: WS Appointment List
    func wsAppointmentList() {
        let defaults = UserDefaults.standard
        let myString = defaults.value(forKey: "savedId")
        let vehicle_id = myString
        
        let params = NSMutableDictionary()
        params["page"] = pageNo
        params["vehicle_id"] = vehicle_id
        
        Http.instance().json(api_appointment_list, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            print("json----->>>>\(json)")
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    if let dictList = json?.object(forKey: "result") as? NSDictionary{
                        var dictAppointmentList = NSMutableDictionary()
                        
                        if self.boolWsPage {
                            dictAppointmentList = (dictList as NSDictionary).mutableCopy() as! NSMutableDictionary
                            self.boolWsPage = false
                        } else {
                            dictAppointmentList = dictList.mutableCopy() as! NSMutableDictionary
                        }
                        
                        if let arrCurrent = dictAppointmentList.object(forKey: "current") as? NSArray {
                            self.arrCurrentAppointment = arrCurrent.mutableCopy() as! NSMutableArray
                        }
                        
                        if let arrPast = dictAppointmentList.object(forKey: "past") as? NSArray {
                            self.arrPastAppointment = arrPast.mutableCopy() as! NSMutableArray
                        }
                        
                        if (self.arrCurrentAppointment.count != 0) || (self.arrPastAppointment.count != 0) {
                            self.tblView.reloadData()
                        }
                    }
                } else {
                    //Http.alert("", string(json! , "message"))
                    self.lblNoRecord.isHidden = false
                }
            }
        }
    }
    
}//Sachin :]

class MyAppointmentCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblStatusImg: UILabel!
    
}

