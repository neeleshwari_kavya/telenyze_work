//
//  WarningListVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 2 on 14/03/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import Foundation
import HarishFrameworkSwift4

class WarningListVC: UIViewController , UITableViewDelegate , UITableViewDataSource , WarningListCellDelegates {
    
    @IBOutlet var subView: UIView!
    @IBOutlet var lblScore: UILabel!
    @IBOutlet var lblResult: UILabel!
    @IBOutlet var lblNormal: UILabel!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var lblShadow: UILabel!
    
    //MARK:- VARIABLES
    var classTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         classTitle = "Engine Detail"
         setNav(className: classTitle)
        self.BackNavigationButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- fuction
    func displayAndDelegates()  {
        tblView.delegate = self
        tblView.dataSource = self
        let intScore = "2000"
        let strResult = "on".capitalized
        let strNormal = "Off".capitalized
        lblScore.numberOfLines = 2
        lblResult.numberOfLines = 2
        lblNormal.numberOfLines = 2
        
        lblScore.attributedText = getMyAtrributedString(strFirst: "Score".capitalized, fontColorFirst: UIColor.white, strSecond: "\n\(intScore)", fontColorSecond: UIColor.white)
        lblResult.attributedText = getMyAtrributedString(strFirst: "Result".capitalized, fontColorFirst: UIColor.white, strSecond: "\n\(strResult)", fontColorSecond: UIColor.white)
        lblNormal.attributedText = getMyAtrributedString(strFirst: "Normal".capitalized, fontColorFirst: UIColor.white, strSecond: "\n\(strNormal)", fontColorSecond: UIColor.white)

        lblShadow.shadow()
        
    }
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionBackButton()  {
        
        self.navigationController?.popToRootViewController(animated: true)
    }

    //MARK:Navigation Buttons
    func refreshRightNavigationButton() {
        let button1 = UIBarButtonItem(image: UIImage(named: "refresh.png"), style: .plain, target: self, action: #selector(actionRefreshButton)) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem = button1
    }
    
    func actionRefreshButton()  {
//        intShopCount = 0
//        intShopCountList = 0
//        arrRepairShopList = NSMutableArray()
       // self.ws_ShopList()
        
    }
    
    func getMyAtrributedString(strFirst:String, fontColorFirst:UIColor , strSecond:String, fontColorSecond:UIColor) -> NSMutableAttributedString {
        
        let attributedStr1 = [
            NSFontAttributeName : UIFont.systemFont(ofSize: 20.0),
            NSForegroundColorAttributeName : fontColorFirst]
        
        let attributedStr2 = [
            NSFontAttributeName : UIFont.boldSystemFont(ofSize: 20.0),
            NSForegroundColorAttributeName : fontColorSecond]
        
        let myFinalString1 = NSMutableAttributedString(string: strFirst, attributes: attributedStr1 )
        
        let myFinalString2 = NSMutableAttributedString(string: strSecond, attributes: attributedStr2 )
        
        myFinalString1.append(myFinalString2)
        
        return myFinalString1
    }
    
    //MARK:- UITableViewDelegate and UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "WarningListCell") as! WarningListCell
        
       // let imgN = imageWithImage(image: UIImage(named: "warning_info.png")!, scaledToSize: CGSize(width: 20.0, height: 20.0))
        let imgN = imageWithImage(image: UIImage(named: "warning_info.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
        cell.imgWarningInfo.image = imgN
        cell.imgWarningInfo.shadow()
        
        cell.intIndex = indexPath.row
        cell.delegateWarningListCell = self
        
        return cell
    }
    
    //MARK:- table view delegate methods
    
    func btnFirstWarning(intIndex:Int){
        print("btnFirstWarning intIndex>>>\(intIndex)")
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WarningDetailVC") as! WarningDetailVC
       // vc.estimateId = string(dict, "id")
       // vc.arrData = arrEstimateList.mutableCopy() as! NSMutableArray
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func btnSecondWarning(intIndex:Int){
        print("btnSecondWarning  intIndex>>>\(intIndex)")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WarningDetailVC") as! WarningDetailVC
        // vc.estimateId = string(dict, "id")
        // vc.arrData = arrEstimateList.mutableCopy() as! NSMutableArray
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func btnFindMore(intIndex:Int){
        print("btnFindMore  intIndex>>>\(intIndex)")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WarningDetailVC") as! WarningDetailVC
        // vc.estimateId = string(dict, "id")
        // vc.arrData = arrEstimateList.mutableCopy() as! NSMutableArray
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}//Neeleshwari......

protocol WarningListCellDelegates {
    func btnFirstWarning(intIndex:Int)
    func btnSecondWarning(intIndex:Int)
    func btnFindMore(intIndex:Int)
}

class WarningListCell: UITableViewCell {
    
    @IBOutlet var viewBG: UIView!
    @IBOutlet var btnFirstWarning: UIButton!
    @IBOutlet var btnSecondWarning: UIButton!
    @IBOutlet var lblDetail: UILabel!
    @IBOutlet var btnFindMore: UIButton!
    @IBOutlet var imgWarningInfo: UIImageView!
    
    var delegateWarningListCell : WarningListCellDelegates?
    var intIndex = Int()
    
    //MARK:- Button Actions
    
    @IBAction func btnFirstWarning(_ sender: Any) {
        delegateWarningListCell?.btnFirstWarning(intIndex: intIndex)
    }
    
    @IBAction func btnSecondWarning(_ sender: Any) {
       delegateWarningListCell?.btnSecondWarning(intIndex: intIndex)
    }
    
    @IBAction func btnFindMore(_ sender: Any) {
         delegateWarningListCell?.btnFindMore(intIndex: intIndex)
    }
    
}



