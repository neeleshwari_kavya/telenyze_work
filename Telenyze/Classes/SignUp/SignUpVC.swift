//
//  SignUpVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 01/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4
import CoreLocation

class SignUpVC: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate,CLLocationManagerDelegate, UIWebViewDelegate {
    
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tfFirstName: SkyFloatingLabelTextField!
    @IBOutlet var tfLastName: SkyFloatingLabelTextField!
    @IBOutlet var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet var tfPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet var tfUserId: SkyFloatingLabelTextField!
    @IBOutlet var tfCreatePass: SkyFloatingLabelTextField!
    @IBOutlet var tfConfirmPass: SkyFloatingLabelTextField!
    @IBOutlet var btnSignUp: UIButton!
    
    //MARK:- VIEW_POPUP
    @IBOutlet var viewPopup: UIView!
    @IBOutlet var viewWebShow: UIView!
    @IBOutlet var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //variables
    var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addDoneButtonOnKeyboard()
        self.delegatesAndDisplayView()
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        PlaceHolderWhiteColor()
        self.navigationController?.isNavigationBarHidden = false
        self.BackNavigationButton()
        setNavTransparent(className: "Sign Up")
        self.hideKeyboard()
    }
    
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionBackButton()  {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
        self.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS.
    @IBAction func actionSignUp(_ sender: Any) {
        hideKeyboard()
        
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            wsUserSignUp()
        }
    }
    
    @IBAction func actionTAndC(_ sender: Any) {
        hideKeyboard()
        popupShow()
        wsTermAndConditions()
    }
    
    @IBAction func actionClose(_ sender: Any) {
        removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: viewWebShow)
    }
    
    //MARK: FUNCTIONS.
    func hideKeyboard() {
        self.view.endEditing(true)
        scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scroll.contentInset = UIEdgeInsets.zero
    }
    
    func delegatesAndDisplayView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.delegate = self
        self.subView.addGestureRecognizer(tap)
        
        self.webView.delegate = self
        self.webView.isOpaque = false
        self.webView.backgroundColor = appColor.appPrimaryColor
    }
    
    func checkValidation() -> String? {
        
        if tfFirstName.text?.count == 0 || tfLastName.text?.count == 0 || tfEmail.text?.count == 0 || tfPhoneNumber.text?.count == 0 || tfUserId.text?.count == 0 ||  tfCreatePass.text?.count == 0 ||  tfConfirmPass.text?.count == 0 {
            
            return AlertMSG.blankRequiredFields
            
        } else {
            
            if (tfEmail.text?.count)! < 6 || !Validate.email(tfEmail.text!) {
                return AlertMSG.invalidEmailFormat
                
            } else if (tfPhoneNumber.text?.count)! < 10 {
                return AlertMSG.invalidMobileNumber
                
            } else if (tfCreatePass.text?.count)! < 6 {
                return AlertMSG.invalidPassword
                
            } else if tfCreatePass.text != tfConfirmPass.text {
                return AlertMSG.pwdMissMatch
            }
        }
        
        return nil
    }
    //MARK:- TEXTFIELD_DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if tfFirstName == textField {
            tfLastName.becomeFirstResponder()
        } else if tfLastName == textField {
            tfEmail.becomeFirstResponder()
        } else if tfEmail == textField {
            tfPhoneNumber.becomeFirstResponder()
        } else if tfPhoneNumber == textField {
            tfUserId.becomeFirstResponder()
        } else if tfUserId == textField {
            tfCreatePass.becomeFirstResponder()
        } else if tfCreatePass == textField {
            tfConfirmPass.becomeFirstResponder()
        } else if tfConfirmPass == textField {
            tfConfirmPass.resignFirstResponder()
            scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var length = (textField.text?.count)! + string.count - range.length
        
        if textField == tfFirstName {
            let characterSet = CharacterSet.init(charactersIn: kAppDelegate.ACCEPTABLE_CHARACTERS).inverted
            length = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            let filter = string.components(separatedBy: characterSet).joined(separator:"")
            return ((string == filter) && newString.length <= length)
            
        } else if textField == tfLastName {
            let characterSet = CharacterSet.init(charactersIn: kAppDelegate.ACCEPTABLE_CHARACTERS).inverted
            length = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            let filter = string.components(separatedBy: characterSet).joined(separator:"")
            return ((string == filter) && newString.length <= length)
        }
        
        if textField == tfFirstName {
            return (length > 50) ? false : true
        } else if textField == tfLastName {
            return (length > 50) ? false : true
        } else if textField == tfEmail {
            return (length > 50) ? false : true
        } else if textField == tfPhoneNumber {
            return (length > 10) ? false : true
        } else if textField == tfUserId {
            return (length > 50) ? false : true
        } else if textField == tfCreatePass {
            return (length > 20) ? false : true
        } else if textField == tfConfirmPass {
            return (length > 20) ? false : true
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scroll.contentSize = CGSize(width: view.frame.size.width, height: btnSignUp.frame.origin.y + btnSignUp.frame.size.height + 70)
    }
    
    /////////__________***********************_________
    //MARK:- KEYBOARD NOTIFICATION METHODS
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scroll.contentInset = UIEdgeInsets.zero
        } else {
            scroll.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        
        scroll.scrollIndicatorInsets = scroll.contentInset
    }
    /////////__________***********************_________
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = appColor.appPrimaryColor
        doneToolbar.tintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let next: UIBarButtonItem = UIBarButtonItem(title: "NEXT", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.nextOfDoneTool))
        let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.hideKeyboard))
        
        var items:[UIBarButtonItem] = []
        items.append(next)
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.tfPhoneNumber.inputAccessoryView = doneToolbar
    }
    
    func nextOfDoneTool() {
        tfUserId.becomeFirstResponder()
    }
    
    func done() {
        tfPhoneNumber.resignFirstResponder()
        scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func PlaceHolderWhiteColor() {
        tfFirstName.placeHolderColor = UIColor.white
        tfLastName.placeHolderColor = UIColor.white
        tfEmail.placeHolderColor = UIColor.white
        tfPhoneNumber.placeHolderColor = UIColor.white
        tfEmail.placeHolderColor = UIColor.white
        tfUserId.placeHolderColor = UIColor.white
        tfCreatePass.placeHolderColor = UIColor.white
        tfConfirmPass.placeHolderColor = UIColor.white
    }
    
    //MARK:- FUNCTION FOR POPVIEW
    func popupShow() {
        viewPopup.frame = self.view.frame
        viewPopup.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.window?.addSubview(viewPopup)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = (self as UIGestureRecognizerDelegate)
        self.viewPopup.addGestureRecognizer(tap)
        
        self.viewWebShow.leftAnchor.constraint(equalTo: viewPopup.leftAnchor, constant: 15).isActive = true
        self.viewWebShow.rightAnchor.constraint(equalTo: viewPopup.rightAnchor, constant: -15).isActive = true
        
        self.viewWebShow.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        self.viewWebShow.heightAnchor.constraint(equalToConstant: 450).isActive = true
        self.test(viewTest: viewWebShow)
    }
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewPopup: UIView, viewWebShow: UIView) {
        let orignalT: CGAffineTransform = viewWebShow.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewWebShow.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewPopup.removeFromSuperview()
            viewWebShow.transform = orignalT
        })
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        //viewPopup.removeFromSuperview()
        removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: viewWebShow)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if viewWebShow.bounds.contains(touch.location(in: viewWebShow)) {
            return false
        }
        return true
    }
    
    //MARK: WS TermAndConditions
    func wsTermAndConditions() {
        
        let params = NSMutableDictionary()
        params["slug"] = "terms-and-conditions"
        
        Http.instance().json(api_terms_aboutUs_privacyPolicy, params, "POST", ai: false, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            print(json!)
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // Http.alert("", string(json! , "message"))
                    if let result = (json as AnyObject).object(forKey: "result") as? NSDictionary{
                        if let description = result.object(forKey: "description"){
                            let myVariable = "<font face='regular' size='2' color= 'white'>%@"
                            let myHtmlString = String(format: myVariable, description as! CVarArg)
                            self.webView.loadHTMLString(myHtmlString, baseURL: nil)
                            
                            /* //link color yellow
                             let myString = String(format: "<html><head><script> document.ontouchmove = function(event) { if (document.body.scrollHeight == document.body.clientHeight) event.preventDefault(); } </script><style type='text/css'>* { margin:1; padding:1; } p { color:white; font-family:Helvetica; font-size:14px; } a { color:blue; text-decoration:none; }</style></head><body><p>%@</p></body></html>", arguments: [description as! CVarArg])
                             
                             self.webView.loadHTMLString(myString, baseURL: nil)
                             */
                        }
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    //MARK:- Webview delegates
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = true
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        activityIndicator.isHidden = true
    }
    
    //MARK:- wsUserSignUp
    
    func wsUserSignUp() {
        let params = NSMutableDictionary()
        
        params["first_name"] = tfFirstName.text
        params["last_name"] = tfLastName.text
        params["username"] = tfUserId.text
        params["email"] = tfEmail.text
        params["password"] = tfConfirmPass.text
        params["contact_no"] = tfPhoneNumber.text
        params["device_id"] = kAppDelegate.DeviceID
        params["device_type"] = kAppDelegate.device_type//"iphone"
        UIDevice.current.deviceInfo(params)
        
        let pick = kAppDelegate.curLocation()
        
        if pick != nil {
            if pick?.coordinate.latitude != 0.0 && pick?.coordinate.longitude != 0.0 {
                params["lat"] = (pick?.coordinate.latitude)!
                params["long"] = (pick?.coordinate.longitude)!
            }
        }
        
        params["notification_id"] = "123"
        print("params-\(params)-")
        
        Http.instance().json(api_UserSignUp, params, "POST", ai: true, popup: true, prnt: true) { (json, params, strJson)  in
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    Http.alert("", string(json! , "message"))
                    
                    if let result = (json as AnyObject).object(forKey: "result") as? NSDictionary {
                        
                       /* let user = User()
                        
                        user.address = string(result, "address")
                        user.app_version = string(result, "app_version")
                        user.appoitment_request = string(result, "appoitment_request")
                        user.auth_token = string(result, "auth_token")
                        user.contact_no = string(result, "contact_no")
                        user.created_at = string(result, "created_at")
                        user.deleted_at = string(result, "deleted_at")
                        user.device_id = string(result, "device_id")
                        user.device_type = string(result, "device_type")
                        user.distance = string(result, "distance")
                        user.email = string(result, "email")
                        
                        user.email_status = string(result, "email_status")
                        user.emergency_email = string(result, "emergency_email")
                        user.emergency_no = string(result, "emergency_no")
                        user.first_name = string(result, "first_name")
                        user.forgot_string = string(result, "forgot_string")
                        user.forgot_string_expiry = string(result, "forgot_string_expiry")
                        user.from_back_end = string(result, "from_back_end")
                        user.geo_fence_radius = string(result, "geo_fence_radius")
                        user.id = string(result, "id")
                        user.ip = string(result, "ip")
                        
                        user.last_name = string(result, "last_name")
                        user.lat = string(result, "lat")
                        user.location = string(result, "location")
                        user.long = string(result, "long")
                        user.new_message = string(result, "new_message")
                        user.no_of_vehicle = string(result, "no_of_vehicle")
                        user.notification_id = string(result, "notification_id")
                        user.notification_status = string(result, "notification_status")
                        user.password = string(result, "password")
                        user.profile_picture = string(result, "profile_picture")
                        
                        user.send_me_location = string(result, "send_me_location")
                        user.set_scan_period = string(result, "set_scan_period")
                        user.shop_promotional = string(result, "shop_promotional")
                        user.sound_status = string(result, "sound_status")
                        user.status = string(result, "status")
                        user.temperature = string(result, "temperature")
                        user.type = string(result, "type")
                        user.update = string(result, "update")
                        user.update_period = string(result, "update_period")
                        user.updated_at = string(result, "updated_at")
                        
                        user.username = string(result, "username")
                        user.vehicle_service = string(result, "vehicle_service")
                        user.verification_status = string(result, "verification_status")
                        user.vibration_status = string(result, "vibration_status")
                        User.save(user)*/
                        
                        kAppDelegate.dictUserInfo = result.mutableCopy() as! NSMutableDictionary
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmVC") as! ConfirmVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
}//Sachin :]

