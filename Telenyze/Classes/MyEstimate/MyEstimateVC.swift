//
//  MyEstimateVC.swift
//  Telenyze
//
//  Created by kavyaMacMini4 on 01/03/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class MyEstimateVC: UIViewController, UITableViewDelegate, UITableViewDataSource , UIGestureRecognizerDelegate{
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var viewAlert: UIView!
    @IBOutlet var viewAlertPopUp: UIView!
    @IBOutlet var lblAlertMsg: UILabel!
    @IBOutlet var btnAlertOK: UIButton!
    
    //MARK:- VARIABLES
    var shop_id = ""
    var arrEstimateList = NSMutableArray()
    var boolWsPage = Bool()
    var page = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNav(className: "My Estimate")
        self.BackNavigationButton()
        self.addNewEstimateButton()
        
        kAppDelegate.shopId = shop_id
        page = 1
        self.ws_MyEstimate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
     //MARK:- Button Actions
    @IBAction func btnAlertOK(_ sender: Any) {
        removeSubViewWithAnimation(viewMain: viewAlert, viewPopUP: viewAlertPopUp)
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        tblView.delegate = self
        tblView.dataSource = self
       tblView.estimatedRowHeight = 100.0
       tblView.rowHeight = UITableViewAutomaticDimension
    }
    
    func addNewEstimateButton() {
        let buttonAdd = UIButton.init(type: .custom)
        buttonAdd.setImage(UIImage.init(named: "plus_car.png"), for: UIControlState.normal)
        buttonAdd.addTarget(self, action:#selector(btnAddNewEstimateAction), for:.touchUpInside)
        buttonAdd.frame = CGRect.init(x: 0, y: 0, width: 15, height: 15)
        let barButton = UIBarButtonItem.init(customView: buttonAdd)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func btnAddNewEstimateAction() {
        print("button press message btn")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EstimateRequestVC") as! EstimateRequestVC
        vc.shop_id = kAppDelegate.shopId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionBackButton()  {
        
        let viewControllers = self.navigationController!.viewControllers as [UIViewController]
        for aViewController:UIViewController in viewControllers {
            if aViewController.isKind(of: RepairShopDetailVC.self) {
                _ = self.navigationController?.popToViewController(aViewController, animated: false)
            }
        }
    }
  
    //MARK:- TABLEVIEW DELEGATE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEstimateList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyEstimateCell", for: indexPath) as! MyEstimateCell
        
        let dict = arrEstimateList[indexPath.row] as! NSDictionary
    
        cell.lblShop.text! = "Shop: " + string(dict, "shop_name")
        cell.lblStatus.text! = "Status: " + string(dict, "status")
        cell.lblDate.text! = "Date: " + string(dict, "updated_at").converDate("yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy")
        cell.lblEstimate.text! = "Estimate: " +  string(dict, "estimate_price")
        cell.lblDescription.text! = "Description: " + string(dict, "description")
        cell.lblDescription.sizeToFit()

        if indexPath.row == (arrEstimateList.count - 1) {
            if !boolWsPage {
                boolWsPage = true
                if arrEstimateList.count % 50 == 0 {
                    print("indexPath.row:\(indexPath.row)")
                    page += 1
                    self.ws_MyEstimate()
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrEstimateList[indexPath.row] as! NSDictionary
        
        print("arrEstimateList>>\(arrEstimateList)")
        
        let status = string(dict, "status")
        let is_appointment = string(dict, "is_appointment")
        
       // print("status------\(status)")
      //  print("is_appointment------\(is_appointment)")
        
        if status == "Quoted" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EstimateRequestVC") as! EstimateRequestVC
            vc.status = "Quoted"
            vc.estimateId = string(dict, "id")
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if status == "Accepted" {
          
            if is_appointment == "1" {
              addPopupShowAlert(viewMain: viewAlert, viewPopUP: viewAlertPopUp)
            }else{
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAppointmentVC") as! AddAppointmentVC
                vc.estimateId = string(dict, "id")
                vc.arrData = arrEstimateList.mutableCopy() as! NSMutableArray
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    //MARK:- FUNCTION FOR POPVIEW
    func addPopupShowAlert(viewMain: UIView, viewPopUP: UIView)  {
        viewMain.frame = self.view.frame
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.window?.addSubview(viewMain)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = (self as UIGestureRecognizerDelegate)
        viewMain.addGestureRecognizer(tap)
        
        viewPopUP.leftAnchor.constraint(equalTo: viewPopUP.leftAnchor, constant: 15).isActive = true
        viewPopUP.rightAnchor.constraint(equalTo: viewPopUP.rightAnchor, constant: -15).isActive = true
        
        viewMain.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        viewMain.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        //   self.subView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        //  self.subView.heightAnchor.constraint(equalToConstant: 568).isActive = true
        
        self.test(viewTest: viewPopUP)
    }
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewMain: UIView, viewPopUP: UIView) {
        let orignalT: CGAffineTransform = viewPopUP.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewMain.removeFromSuperview()
            viewPopUP.transform = orignalT
        })
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        //viewPopup.removeFromSuperview()
        removeSubViewWithAnimation(viewMain: viewAlert, viewPopUP: viewAlertPopUp)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if viewAlert.bounds.contains(touch.location(in: viewAlertPopUp)) {
            return false
        }
        
        return true
    }
    
    //MARK:- ------------------ MyEstimate List ----------------------//
    func ws_MyEstimate() {
        let defaults = UserDefaults.standard
        let myString = defaults.value(forKey: "savedId")
        let vehicle_id = myString
        
        let params = NSMutableDictionary()
        params["page"] = page
        params["vehicle_id"] = vehicle_id
        params["shop_id"] = kAppDelegate.shopId
        
        Http.instance().json(api_estimateList, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrEstimateList = NSMutableArray()
                    if let arr = json?.object(forKey: "result") as? NSArray {
                        // self.arrList  = arr.mutableCopy() as! NSMutableArray
                        
                        if self.boolWsPage {
                            self.arrEstimateList.addObjects(from: arr as [AnyObject])
                            self.boolWsPage = false
                        } else {
                            self.arrEstimateList = (arr.mutableCopy() as? NSMutableArray)!
                        }
              
                        self.tblView.reloadData()
                    }
                } else {
                    //Http.alert("", string(json! , "message"))
                }
            }
        }
    }
}//Sachin :]

class MyEstimateCell : UITableViewCell {
    
    @IBOutlet var lblShop: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblEstimate: UILabel!
    @IBOutlet var lblDescription: UILabel!
    
}
