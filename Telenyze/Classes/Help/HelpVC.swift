//
//  HelpVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 2 on 08/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import Foundation
import HarishFrameworkSwift4

class HelpVC: UIViewController , UIGestureRecognizerDelegate , UITextFieldDelegate , UITextViewDelegate  {
    
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tvSubject: UITextView!
    @IBOutlet var tvMessage: UITextView!
    @IBOutlet var btnSubmit: UIButton!
    
    //variables
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegatesAndDisplayView()
        self.registerForKeyboardNotifications()
        self.hideKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNav(className: "Help")
        menuNavigationButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "MenuButton"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- Button Actions
    
    func leftNavButtonAction() {
        print("leftNavButtonAction")
    }
    func rightNavButtonAction() {
        print("rightNavButtonAction")
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        self.hideKeyboard()
        
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            wsHelpSupport()
        }
    }
    
    //MARK: FUNCTIONS.
    func delegatesAndDisplayView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.delegate = self
        self.subView.addGestureRecognizer(tap)
        
        self.tvSubject.delegate = self
        self.tvMessage.delegate = self
        
        self.tvSubject.layer.borderColor = UIColor.white.cgColor
        self.tvSubject.layer.borderWidth = 2.0
        
        self.tvMessage.layer.borderColor = UIColor.white.cgColor
        self.tvMessage.layer.borderWidth = 2.0
        
        self.tvSubject.contentInset = UIEdgeInsetsMake(7, 5, 7, 5)
        self.tvMessage.contentInset = UIEdgeInsetsMake(5, 5, 5, 5)
        
        self.tvSubject.textContainer.maximumNumberOfLines = 1
        
        self.tvSubject.textColor = UIColor.clear
        self.tvMessage.textColor = UIColor.clear
        
        self.tvSubject.tintColor = UIColor.clear
        self.tvMessage.tintColor = UIColor.clear
        
        self.tvSubject.textColor = UIColor.white
        self.tvMessage.textColor = UIColor.white
        
        self.tvSubject.tintColor = UIColor.white
        self.tvMessage.tintColor = UIColor.white
        
        self.tvSubject.resignFirstResponder()
        self.tvMessage.resignFirstResponder()
        
        self.tvSubject.reloadInputViews()
        self.tvMessage.reloadInputViews()
        
         self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func checkValidation() -> String? {
        
        if tvSubject.text?.count == 0  {
            
            return AlertMSG.blankSubject
        }else if tvMessage.text?.count == 0 {
            
            return AlertMSG.blankMessage
        }
        return nil
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
        scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scrlView.contentInset = UIEdgeInsets.zero
    }
    
    //MARK:- TEXTVIEW_DELEGATE
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if UIScreen.main.bounds.size.height < 667{
            if (textView.frame.origin.y >= 220) {
                scrlView.setContentOffset(CGPoint(x: 0, y: CGFloat(Int(textView.frame.origin.y - 220))) , animated: true)
            }
        }
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var maxCharacter = Int()
        
        if textView == tvSubject {
            maxCharacter = 50
        }else{
            maxCharacter = 250
        }
        
        if (text == "\n") {
            if textView == tvSubject {
                tvMessage.becomeFirstResponder()
            }else{
                tvMessage.resignFirstResponder()
            }
            return false
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return (numberOfChars < maxCharacter)
    }
    /////////__________***********************_________
    //MARK:- KEYBOARD NOTIFICATION METHODS
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrlView.contentInset = UIEdgeInsets.zero
        } else {
            scrlView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        scrlView.scrollIndicatorInsets = scrlView.contentInset
    }
    
    /////////__________***********************_________
    
    //MARK:- Help_support
    func wsHelpSupport() {
        
        let params = NSMutableDictionary()
        params["subject"] = tvSubject.text
        params["message"] = tvMessage.text
        
        Http.instance().json(api_help_support, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    Http.alert("", string(json! , "message"))
                    self.tvSubject.text = ""
                    self.tvMessage.text = ""
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
}//////////////---------************************_____________












