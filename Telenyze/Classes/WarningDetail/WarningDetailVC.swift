
//
//  WarningDetailVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 2 on 14/03/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import Foundation
import HarishFrameworkSwift4

class WarningDetailVC: UIViewController {

    @IBOutlet var imgWarningInfo: ImageView!
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    @IBOutlet var lblAppearDate: UILabel!
    
    @IBOutlet var lblDescriptionTitle: UILabel!
    @IBOutlet var lblDescriptionDetails: UILabel!

    var dictDetails = NSMutableDictionary()
    var classTitle = ""
    
    //MARK:- VARIABLES
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        classTitle = "Warning Detail"
        setNav(className: classTitle)
        self.crossNavigationButton()
        //share details of warning list
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
     
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- fuction
    func displayAndDelegates()  {
        lblDescriptionDetails.text = "ajsdfjkads jsdkafadjs fksdaf jaksfdjdskfadsf dsjfksajfkasf sd fjksdjfksd s dfas fjsadkfsd fds fasf sdaf sdkjfkdsfj ksdf sadjfksd k ajsdfjkads jsdkafadjs fksdaf jaksfdjdskfadsf dsjfksajfkasf sd fjksdjfksd s dfas fjsadkfsd fds fasf sdaf sdkjfkdsfj ksdf sadjfksd kajsdfjkads jsdkafadjs fksdaf jaksfdjdskfadsf dsjfksajfkasf sd fjksdjfksd s dfas fjsadkfsd fds fasf sdaf sdkjfkdsfj ksdf sadjfksd kajsdfjkads jsdkafadjs fksdaf jaksfdjdskfadsf dsjfksajfkasf sd fjksdjfksd s dfas fjsadkfsd fds fasf sdaf sdkjfkdsfj ksdf sadjfksd kajsdfjkads jsdkafadjs fksdaf jaksfdjdskfadsf dsjfksajfkasf sd fjksdjfksd s dfas fjsadkfsd fds fasf sdaf sdkjfkdsfj ksdf sadjfksd kajsdfjkads jsdkafadjs fksdaf jaksfdjdskfadsf dsjfksajfkasf sd fjksdjfksd s dfas fjsadkfsd fds fasf sdaf sdkjfkdsfj ksdf sadjfksd kajsdfjkads jsdkafadjs fksdaf jaksfdjdskfadsf dsjfksajfkasf sd fjksdjfksd s dfas fjsadkfsd fds fasf sdaf sdkjfkdsfj ksdf sadjfksd kajsdfjkads jsdkafadjs fksdaf jaksfdjdskfadsf dsjfksajfkasf sd fjksdjfksd s dfas fjsadkfsd fds fasf sdaf sdkjfkdsfj ksdf sadjfksd kajsdfjkads jsdkafadjs fksdaf jaksfdjdskfadsf dsjfksajfkasf sd fjksdjfksd s dfas fjsadkfsd fds fasf sdaf sdkjfkdsfj ksdf sadjfksd kajsdfjkads jsdkafadjs fksdaf jaksfdjdskfadsf dsjfksajfkasf sd fjksdjfksd s dfas fjsadkfsd fds fasf sdaf sdkjfkdsfj ksdf sadjfksd k sdfjksjaf OK"
        lblDescriptionDetails.numberOfLines = 0
        lblDescriptionDetails.lineBreakMode = .byWordWrapping
        lblDescriptionDetails.sizeToFit()
       lblDescriptionDetails.backgroundColor = UIColor.white

        //let imgN = imageWithImage(image: UIImage(named: "warning_info.png")!, scaledToSize: CGSize(width: 20.0, height: 20.0))
        let imgN = imageWithImage(image: UIImage(named: "warning_info.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
        
        imgWarningInfo.image = imgN
        imgWarningInfo.shadow()
    }
    
    func crossNavigationButton()  {
        let imgN = imageWithImage(image: UIImage(named: "cross.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
        
        let button1 = UIBarButtonItem(image: imgN, style: .plain, target: self, action: #selector(actionCrossButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionCrossButton()  {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
}

