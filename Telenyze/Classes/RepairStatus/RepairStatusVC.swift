//
//  RepairStatusVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 2 on 22/03/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import Foundation
import HarishFrameworkSwift4

class RepairStatusVC: UIViewController {
   
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var viewProgressBar: MBCircularProgressBarView!
    @IBOutlet var lblVehicleStatus: UILabel!
    @IBOutlet var imgStatus: ImageView!
    @IBOutlet var viewDetails: View!
    @IBOutlet var lblPickUPTime: UILabel!
    @IBOutlet var lblEstimateAmount: UILabel!
    @IBOutlet var lblOrderNumber: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblShopName: UILabel!
    @IBOutlet var lblShopAddress: UILabel!
    @IBOutlet var lblShopContactNumber: UILabel!
    
    //MARK:- VARIABLES
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNav(className: "Repair Status")
        self.crossNavigationButton()
        self.wsVehicleHealthStatus()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- FUCTIONS
    func displayAndDelegates()  {
       viewProgressBar.value = 95
        
    }
    
    func crossNavigationButton()  {
        let imgN = imageWithImage(image: UIImage(named: "cross.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
        
        let button1 = UIBarButtonItem(image: imgN, style: .plain, target: self, action: #selector(actionCrossButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionCrossButton()  {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    
    //MARK:- ws wsVehicleHealthStatus
    func wsVehicleHealthStatus(){
        
        let params = NSMutableDictionary()
        
       // params["vehicle_id"] = "2"
        params["code"] = "1"
        
        Http.instance().json(api_health_status, params, "POST", ai: true, popup: true, prnt: true) { (json, params, strJson)  in
            
            print("json>>>\(json)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // Http.alert("", string(json! , "message"))
                    
                    
                    /*
                     let viewControllers = self.navigationController!.viewControllers as [UIViewController]
                     for aViewController:UIViewController in viewControllers {
                     if aViewController.isKind(of: SignInVC.self) {
                     _ = self.navigationController?.popToViewController(aViewController, animated: true)
                     }
                     }
                     */
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
   

    
}//==================_______Neeleshwari________======================
/* //response vehicle health status
json>>>Optional({
    message = "Records Found !!";
    result =     (
        {
            ALRTS =             (
            );
            LOC =             {
                alt = 0;
                lat = 0;
                lng = 0;
            };
            RES =             {
                BAT = "13.7";
                DTC = "";
                DTCI = 0;
                EMI = N;
                IAT = 75;
                MIL = OFF;
                MPG = N;
                MPH = 30;
                PERF = N;
                RPM = 1707;
                TEMP = N;
            };
            UTC = 1511121359303;
        }
    );
    success = 1;
})
*/



