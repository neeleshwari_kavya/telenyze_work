//
//  MyCarsVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 1 on 16/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class MyCarsVC: UIViewController, UITableViewDataSource, UITableViewDelegate , UIGestureRecognizerDelegate, UITextFieldDelegate {
    
    @IBOutlet var lblVehicleStatus: UILabel!
    @IBOutlet var lblStatusDate: UILabel!
    @IBOutlet var imgCamera: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblVin: UILabel!
    @IBOutlet var lblColor: UILabel!
    @IBOutlet var lblObd: UILabel!
    @IBOutlet var lblCarColor: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var viewVehicleStatusConstraintHeight: NSLayoutConstraint!
    
    @IBOutlet var viewCarDetailsConstraintY: NSLayoutConstraint!
    
    //MARK:- POPUP_VIEW
    @IBOutlet var lblPopupTitle: UILabel!
    @IBOutlet var viewPopup: UIView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tblViewVehicle: UITableView!
    @IBOutlet var tfSearch: UITextField!
    @IBOutlet var lblNoRecord: UILabel!
    @IBOutlet var btnStringClear: UIButton!
    
    //Mark:- Variables
    var popShow = ""
    var arrAstimateList = NSMutableArray()
    var arrVehicleList = NSMutableArray()
    var arrTable = NSMutableArray()
    var firstVehicleId = ""
    var firstVehicleName = ""
    var defaultSelected = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoRecord.isHidden = true
        btnStringClear.isHidden = true
        tblViewVehicle.delegate = self
        tblViewVehicle.dataSource = self
        self.automaticallyAdjustsScrollViewInsets = false
        viewVehicleStatusConstraintHeight.constant = 0
        viewCarDetailsConstraintY.constant = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        menuNavigationButton()
        popShow = "no"
        self.ws_vehicleList()
    }
    
    @IBAction func actionWarning(_ sender: Any) {
      //  Http.alert("", "under working")
        print("actionWarning")
        
    }
    
    //MARK:- TABLEVIEW_DELEGATE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblView {
            return arrAstimateList.count
        } else {
            return arrTable.count
        }
        
     //   return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCarsCell", for: indexPath) as! MyCarsCell
            let dict = arrAstimateList.object(at: indexPath.row) as! estimateList
            cell.lblDate.text! = dict.created_at.converDate("yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy")
            cell.lblWallent.text! = "$" + dict.estimate_price
            cell.lblIssue.text! = dict.descrip.capFirstLetter()
            cell.lblShop.text! = dict.shop_name.capFirstLetter()
    
            return cell
            
        } else {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "MyCarsVehicleCell", for: indexPath) as! MyCarsVehicleCell
            let dict = arrTable.object(at: indexPath.row) as! vehicleList
            cell1.lblName.text = dict.make
            
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblViewVehicle {
            let dict = arrTable.object(at: indexPath.row) as! vehicleList
            
            print("dict>>>\(dict)")
            setNavMyCars(carName:dict.make)
     /*
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: "savedVehicle")
            defaults.removeObject(forKey: "savedId")
            defaults.set(dict.make, forKey: "savedVehicle")
            defaults.set(dict.id, forKey: "savedId")
            */
            
            removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: subView)
            self.ws_VehicleDetail()
        }
    }
    
    //MARK:- POPUP_ACTIONS ------------------------------------------------
    @IBAction func actionStringClear(_ sender: Any) {
        tfSearch.text = ""
        btnStringClear.isHidden = true
        self.arrTable = self.arrVehicleList
        
        /*if tfSearch.text == "" {
            lblNoRecord.isHidden = true
        } else {
            lblNoRecord.isHidden = false
        }*/
        
        tblViewVehicle.reloadData()
    }
    
    @IBAction func actionPopupClose(_ sender: Any) {
        self.view.endEditing(true)
        removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: subView)
    }
    
    //MARK:- FUNCTION FOR POPVIEW ----------------------------------------
    func popupShow() {
        viewPopup.frame = self.view.frame
        viewPopup.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.window?.addSubview(viewPopup)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = (self as UIGestureRecognizerDelegate)
        self.viewPopup.addGestureRecognizer(tap)
        
        self.subView.leftAnchor.constraint(equalTo: viewPopup.leftAnchor, constant: 15).isActive = true
        self.subView.rightAnchor.constraint(equalTo: viewPopup.rightAnchor, constant: -15).isActive = true
        
        self.subView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        self.subView.heightAnchor.constraint(equalToConstant: 568).isActive = true
        self.test(viewTest: subView)
    }
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewPopup: UIView, viewWebShow: UIView) {
        let orignalT: CGAffineTransform = viewWebShow.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewWebShow.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewPopup.removeFromSuperview()
            viewWebShow.transform = orignalT
        })
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: subView)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if subView.bounds.contains(touch.location(in: subView)) {
            return false
        }
        return true
    }
    
    //MARK: TEXTFIELD DELEGATE
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfSearch {
            tfSearch.resignFirstResponder()
        }
        
        return true
    }
    
    //MARK:- ARRAY DATA SEARCH-*-*-*-*-*-*-*--*-*
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength: Int = textField.text!.count + string.count - range.length
        
        if tfSearch.text == "0" {
            btnStringClear.isHidden = true
        } else {
            btnStringClear.isHidden = false
        }
        
        if textField == tfSearch {
            if string == "\n" {
                textField.resignFirstResponder()
                return false
            } else {
                if newLength == 0 {
                    arrTable = arrVehicleList
                    tblViewVehicle.reloadData()
                } else {
                    search(strSearch: textField.text! + string)
                }
            }
        }
        
        /*if arrTable.count == 0 {
            lblNoRecord.isHidden = false
        } else {
            lblNoRecord.isHidden = true
        }*/
        
        return true
    }
    
    func search(strSearch:String) {
        let arr =  NSMutableArray()
        
        for i in 0 ..< arrVehicleList.count {
            let dict = arrVehicleList.object(at: i) as! vehicleList
            print(strSearch)
            if dict.make.lowercased().subString(strSearch.lowercased()) {
                arr.add(dict)
            }
        }
        
        arrTable = arr
        tblViewVehicle.reloadData()
    }
    
    //---------------------- Navigation View --------------------------//
    
    func setNavMyCars(carName:String) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = appColor.appPrimaryColor
        
        let titleViewNav = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 100, height: 40))
        let btnMenuCarList = UIButton(frame: CGRect(x: 0 , y: 0, width:  self.view.frame.size.width - 150, height: 40))
        
        btnMenuCarList.semanticContentAttribute = .forceRightToLeft
        btnMenuCarList.addTarget(self, action:#selector(actionMenuCarList), for:.touchUpInside)
        titleViewNav.addSubview(btnMenuCarList)
        self.navigationItem.titleView = titleViewNav
        
        let lbl1 = UILabel(frame: CGRect.init(x: 0 , y: 0, width: 50, height: 40))
        lbl1.text = carName
        lbl1.font = UIFont.boldSystemFont(ofSize: 16.0)
        lbl1.textColor = UIColor.white
        lbl1.numberOfLines = 1
        lbl1.sizeToFit()
        lbl1.center.y = btnMenuCarList.center.y
        titleViewNav.addSubview(lbl1)
        if lbl1.frame.size.width > self.view.frame.size.width - 170 {
            lbl1.frame.size.width = self.view.frame.size.width - 170
        }
        
        let imageView = UIImageView()
        imageView.image = UIImage(named: "arrow-dwn_car")
        imageView.frame = CGRect(x: lbl1.frame.origin.y + lbl1.frame.size.width , y: 5, width: 20, height: 20)
        imageView.center.y = btnMenuCarList.center.y
        imageView.contentMode = .scaleAspectFit
        titleViewNav.addSubview(imageView)
        
        let btnEditCar = UIButton(type: .custom)
        btnEditCar.setImage(UIImage(named: "edit_car.png"), for: .normal)
        btnEditCar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnEditCar.addTarget(self, action: #selector(actionEditCar), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btnEditCar)
        
        let btnAddCar = UIButton(type: .custom)
        btnAddCar.setImage(UIImage(named: "plus_car.png"), for: .normal)
        btnAddCar.frame = CGRect(x: 0, y: 5, width: 30, height: 30)
        btnAddCar.addTarget(self, action: #selector(actionAddCar), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btnAddCar)
        
        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
        
        btnMenuCarList.backgroundColor = UIColor.clear
        imageView.backgroundColor = UIColor.clear
        btnMenuCarList.layer.borderColor = UIColor.clear.cgColor
        btnMenuCarList.layer.borderWidth = 2.0
        lbl1.backgroundColor = UIColor.clear
    }
    
    func menuNavigationButton() {
        let button1 = UIBarButtonItem(image: UIImage(named: "MenuButton"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionMenuButton() {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
    
    func actionMenuCarList() {
        popShow = "yes"
        self.ws_vehicleList()
    }
    
    func actionAddCar() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMoreCarVC") as! AddMoreCarVC
        vc.strDeleteBtnHide = "newCar"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func actionEditCar() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMoreCarVC") as! AddMoreCarVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //---------------------- Navigation View --------------------------
    
    //MARK:- Web_Service_Call
    func ws_VehicleDetail() {
       
        //let myString = defaults.value(forKey: "savedId")
        let params = NSMutableDictionary()
        var myString = ""
        
        if defaultSelected.count == 0{
           // self.firstVehicleId = id
          //  self.firstVehicleName = make
            print("firstVehicleId>>>>>>\(self.firstVehicleId)")
            print("firstVehicleName>>>>>>\(self.firstVehicleName)")
            myString = firstVehicleId
            
        }else{
            let defaults = UserDefaults.standard
            myString = defaults.value(forKey: "savedId") as! String
        }
        print("myString>>>>>>\(myString)")
        params["vehicle_id"] = myString

        Http.instance().json(api_VehicleDetail, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            print("params>>>\(params)")
            print("json>>>\(json)")
             print("strJson>>>\(strJson)")
            
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    
                    if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                        
                        let dict = result.object(at: 0) as! NSDictionary
                        self.lblColor.text! = string(dict, "color")
                        
                        let strCode = string(dict, "color_code")
                        let colorCode = strCode.replacingOccurrences(of: "#", with: "")
                         self.lblCarColor.backgroundColor = UIColor(hex: colorCode)
                        
                        let make = string(dict, "make")
                        self.lblObd.text! = string(dict, "obd_device_make")
                        let vehicle_image = string(dict, "vehicle_image")
                        let Sachin = (kAppDelegate.server?.base_url)! + vehicle_image
                        
                        if vehicle_image.isEmpty {
                            self.imgCamera.image = UIImage(named: "noimage.jpg")
                        } else {
                            let url = NSURL(string: Sachin)
                            self.imgCamera.af_setImage(withURL: url as URL!)
                        }
                        
                        self.lblVin.text! = string(dict, "vin_number")
                        let year = string(dict, "year")
                        self.lblName.text! = make + " | " + year + " year"
                    }
                    
                    if let estimate = (json as AnyObject).object(forKey: "estimate") as? NSArray {
                        
                        for i in 0..<estimate.count {
                            let dict = estimate[i] as! NSDictionary
                            let ob = estimateList()
                            ob.created_at = string(dict, "created_at")
                            ob.id = string(dict, "id")
                            ob.descrip = string(dict, "description")
                            ob.estimate_price = string(dict, "estimate_price")
                            ob.high_priority = string(dict, "high_priority")
                            ob.id = string(dict, "id")
                            ob.is_drivable = string(dict, "is_drivable")
                            ob.is_tow_needed = string(dict, "is_tow_needed")
                            ob.need_ride = string(dict, "need_ride")
                            ob.id = string(dict, "id")
                            ob.shop_id = string(dict, "shop_id")
                            ob.shop_name = string(dict, "shop_name")
                            ob.status = string(dict, "status")
                            ob.updated_at = string(dict, "updated_at")
                            ob.users_id = string(dict, "users_id")
                            ob.vehicle_id = string(dict, "vehicle_id")
                            self.arrAstimateList.add(ob)
                        }
                       
                        self.tblView.reloadData()
                    }
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
    }
    
    func ws_vehicleList() {
        print("kAppDelegate.arrGLVehicleList.count----\(kAppDelegate.arrGLVehicleList.count)")
        
        //if kAppDelegate.arrGLVehicleList.count == 0 {
            let params = NSMutableDictionary()
            
            params["page"] = "1"
            params["id"] = ""
            params["search_value"] = ""
            
            Http.instance().json(api_vehiclelisting, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
                
                if json != nil {
                    let json = json as? NSDictionary
                    if number(json! , "success").boolValue {
                        self.arrTable = NSMutableArray()
                        self.arrVehicleList = NSMutableArray()
                        
                        if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                            
                            for i in 0..<result.count {
                                let dict = result[i] as! NSDictionary
                                let ob = vehicleList()
                                ob.make = string(dict, "make")
                                ob.id = string(dict, "id")
                                self.arrVehicleList.add(ob)
                            }
                            
                            kAppDelegate.arrGLVehicleList = self.arrVehicleList
                            self.arrTable = self.arrVehicleList
                            self.tblViewVehicle.reloadData()
                            
                            let defaults = UserDefaults.standard
                          //  let EmptyDict = defaults.value(forKey: "savedVehicle")

                            var make = ""
                            var id = ""
                            
                           // if EmptyDict == nil {
                                if self.arrVehicleList.count > 0 {
                                    if let ob = self.arrVehicleList.object(at: 0) as? vehicleList {
                                        make = ob.make
                                        id = ob.id
                                        self.firstVehicleId = id
                                        self.firstVehicleName = make
                                        print("firstVehicleId>>>>>>\(self.firstVehicleId)")
                                        print("firstVehicleName>>>>>>\(self.firstVehicleName)")
                                    }
                               // }
                                
                                defaults.removeObject(forKey: "savedVehicle")
                                defaults.removeObject(forKey: "savedId")
                                defaults.set(make, forKey: "savedVehicle")
                                defaults.set(id, forKey: "savedId")
                                
                                let defaults = UserDefaults.standard
                                if let myString = defaults.value(forKey: "savedVehicle") {
                                    self.setNavMyCars(carName:myString as! String)
                                }
                                
                            } else {
                                if let myString = defaults.value(forKey: "savedVehicle") {
                                    self.setNavMyCars(carName:myString as! String)
                                }
                            }
                            
                            self.ws_VehicleDetail()
                            
                            if self.popShow == "yes" {
                                self.popupShow()
                            }
                        }
                    } else {
                        Http.alert("", string(json! , "message"))
                    }
                }
            }
        /*
        } else {
            
            self.arrTable = NSMutableArray()
            self.arrVehicleList = NSMutableArray()
            self.arrTable = kAppDelegate.arrGLVehicleList
            self.arrVehicleList = kAppDelegate.arrGLVehicleList
            self.tblViewVehicle.reloadData()
            
            let defaults = UserDefaults.standard
            if let myString = defaults.value(forKey: "savedVehicle") {
                self.setNavMyCars(carName:myString as! String)
            }
            
            self.ws_VehicleDetail()
            
            if self.popShow == "yes" {
                self.popupShow()
            }
        }
    */
    }
}//Sachin :]

class MyCarsCell: UITableViewCell {
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblWallent: UILabel!
    @IBOutlet var lblIssue: UILabel!
    @IBOutlet var lblShop: UILabel!
}

class MyCarsVehicleCell: UITableViewCell {
    @IBOutlet var lblName: UILabel!
}
