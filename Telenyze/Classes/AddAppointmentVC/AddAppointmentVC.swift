//
//  AppointmentVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 2 on 15/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.


import Foundation
import HarishFrameworkSwift4

class AddAppointmentVC: UIViewController, CalenderViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var viewCalender: CalenderView!
    @IBOutlet var lblMonth: UILabel!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnPrevious: UIButton!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var tfIssueDetail: UITextField!
    @IBOutlet var btnSubmit: UIButton!
    
    //variables
    var estimateId = ""
    var selectedDate = ""
    var selectedTime = ""
    var dayName = ""
    var arrWeeklyTime = NSMutableArray()
    
    var arrTime = NSMutableArray()
    var arrData = NSMutableArray()
    //new
   // var shop_id = ""
    // var arrWeeklyTime2 = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dispalyAndDelegates()
        
        self.neelesshwariAppointmentDetails()
        
         print("kAppDelegate.dictShopNeeleshwari>>>", kAppDelegate.dictShopNeeleshwari)
        print("arrData--------\(arrData)")

    }
     /////////////////////////==========================
    /*
     if (arrData.count != 0) {
     let dict = arrData.object(at: 0) as! NSDictionary
     if let shop_weekly_timing =  dict.object(forKey: "shop_weekly_timing") as? NSArray {
     for i in 0..<shop_weekly_timing.count {
     let dict = shop_weekly_timing[i] as! NSDictionary
     let ob = weekTime()
     ob.close = string(dict, "close")
     ob.created_at = string(dict, "created_at")
     ob.day = string(dict, "day")
     ob.deleted_at = string(dict, "deleted_at")
     ob.id = string(dict, "id")
     ob.open = string(dict, "open")
     ob.shop_id = string(dict, "shop_id")
     kAppDelegate.shopId = string(dict, "shop_id")
     ob.status = string(dict, "status")
     ob.updated_at = string(dict, "updated_at")
     self.arrWeeklyTime.add(ob)
     print("arrWeeklyTime>>>\(arrWeeklyTime)")
     }
     }
     }
     */
    /////////////////////////=================================
    /*
     let date = Date()
     let formatter = DateFormatter()
     formatter.dateFormat = "EEEE yyyy-MM-dd"
     let currentDate = formatter.string(from: date)
     let current = formatter.string(from: date).converDate("EEEE yyyy-MM-dd", "EEEE yyyy-MM-dd")
     
     
     var status = ""
     var day = ""
     
     print("*************** Off day data *****************")
     
     for i in 0..<arrWeeklyTime.count {
     let dict = arrWeeklyTime.object(at: i) as! weekTime
     status = dict.status
     day = dict.day
     print("status------\(status)")
     print("day------\(day)")
     }
     */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.hideKeyboard()
        self.crossNavigationButton()
        setNav(className: "Make Appointment")
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
        self.title  = ""
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    func neelesshwariAppointmentDetails() {
        
        let dictMYShopDetails = kAppDelegate.dictShopNeeleshwari.mutableCopy() as! NSMutableDictionary
        
        print("dictMYShopDetails>>>\(dictMYShopDetails)")
        
        if dictMYShopDetails.count != 0{
            if let shop_weekly_timing =  dictMYShopDetails.object(forKey: "shop_weekly_timing") as? NSArray {
                for i in 0..<shop_weekly_timing.count {
                    let dict = shop_weekly_timing[i] as! NSDictionary
                    let ob = weekTime()
                    ob.close = string(dict, "close")
                    ob.created_at = string(dict, "created_at")
                    ob.day = string(dict, "day")
                    ob.deleted_at = string(dict, "deleted_at")
                    ob.id = string(dict, "id")
                    ob.open = string(dict, "open")
                    ob.shop_id = string(dict, "shop_id")
                    kAppDelegate.shopId = string(dict, "shop_id")
                    ob.status = string(dict, "status")
                    ob.updated_at = string(dict, "updated_at")
                    self.arrWeeklyTime.add(ob)
                    print("arrWeeklyTime>>>\(arrWeeklyTime)")
                }
            }
        }
    }
    
    //MARK:- Fuctions
    func crossNavigationButton()  {
        let imgN = imageWithImage(image: UIImage(named: "cross.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
        
        let button1 = UIBarButtonItem(image: imgN, style: .plain, target: self, action: #selector(actionCrossButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionCrossButton()  {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    func dispalyAndDelegates()   {
        collectionView.isHidden = true
        collectionView.delegate = self
        collectionView.dataSource = self
        viewCalender.delegate = self
        let imgNext = imageWithImage(image: UIImage(named: "arrow_next.png")!, scaledToSize: CGSize(width: 8.0, height: 12.0))
        
        btnNext.setImage(imgNext, for: .normal)
        btnPrevious.setImage(imgNext, for: .normal)
        self.btnPrevious.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        collectionView.delegate = self
        collectionView.dataSource = self
        ////collectionView.collectionViewLayout
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        flow.minimumInteritemSpacing = 5
        flow.minimumLineSpacing = 5
        collectionView.backgroundColor = appColor.profileBGColor
        
        tfIssueDetail.delegate = self
        tfIssueDetail.placeHolderColor = UIColor.darkGray
        tfIssueDetail.placeholder = "Add any details to the issue..."
        tfIssueDetail.paddingView(5)
    }
    
    //MARK:- Actions
    @IBAction func btnNext(_ sender: Any) {
        viewCalender.showNextMonth()
    }
    
    @IBAction func btnPrevious(_ sender: Any) {
        viewCalender.showPreviousMonth()
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        
        self.hideKeyboard()
        
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            wsAddAppointment()
        }
    }
    
    //MARK:- Delegate methods calendar
    func currentMonthYear(_ monthYear: String, _ view: CalenderView) {
        lblMonth.text = monthYear
    }
    
    func calenderDateClicked(_ btn: CalenderButton) {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE yyyy-MM-dd"
        let currentDate = formatter.string(from: date)
        let current = formatter.string(from: date).converDate("EEEE yyyy-MM-dd", "yyyy-MM-dd")
        print("current-------\(current)")
        
        arrTime = NSMutableArray()
        let dateClicked = "\(btn.date as Date)"
        selectedDate = dateClicked.converDate("yyyy-MM-dd HH:mm:ss z", "EEEE yyyy-MM-dd")
        print("selectedDate-------\(selectedDate)")
        //=========================================================
        if currentDate != selectedDate {
            dayName = dateClicked.converDate("yyyy-MM-dd HH:mm:ss z", "EEEE")
            
            var close = ""
            var open = ""
            
            if dayName == "Sunday" {
                for i in 0..<arrWeeklyTime.count {
                    let dict = arrWeeklyTime.object(at: i) as! weekTime
                    if dict.day == "0" {
                        close = dict.close
                        open = dict.open
                    }
                }
            } else if dayName == "Monday" {
                for i in 0..<arrWeeklyTime.count {
                    let dict = arrWeeklyTime.object(at: i) as! weekTime
                    if dict.day == "1" {
                        close = dict.close
                        open = dict.open
                    }
                }
            } else if dayName == "Tuesday" {
                for i in 0..<arrWeeklyTime.count {
                    let dict = arrWeeklyTime.object(at: i) as! weekTime
                    if dict.day == "2" {
                        close = dict.close
                        open = dict.open
                    }
                }
            } else if dayName == "Wednesday" {
                for i in 0..<arrWeeklyTime.count {
                    let dict = arrWeeklyTime.object(at: i) as! weekTime
                    if dict.day == "3" {
                        close = dict.close
                        open = dict.open
                    }
                }
            } else if dayName == "Thursday" {
                for i in 0..<arrWeeklyTime.count {
                    let dict = arrWeeklyTime.object(at: i) as! weekTime
                    if dict.day == "4" {
                        close = dict.close
                        open = dict.open
                    }
                }
            } else if dayName == "Friday" {
                for i in 0..<arrWeeklyTime.count {
                    let dict = arrWeeklyTime.object(at: i) as! weekTime
                    if dict.day == "5" {
                        close = dict.close
                        open = dict.open
                    }
                }
            } else if dayName == "Saturday" {
                for i in 0..<arrWeeklyTime.count {
                    let dict = arrWeeklyTime.object(at: i) as! weekTime
                    if dict.day == "6" {
                        close = dict.close
                        open = dict.open
                    }
                }
            }
            
            let start = open
            var start1 = Int(start.converDate("HH:mm:ss", "HH"))
            let end = close
            let end1 = Int(end.converDate("HH:mm:ss", "HH"))
            
            for _ in 0..<24 {
                start1 = start1!
                if start1! > end1!{
                    break
                } else {
                    arrTime.add(start1!)
                    start1 = start1! + 1
                }
            }
            
            collectionView.reloadData()
            collectionView.isHidden = false
            
        } else {
            collectionView.isHidden = true
        }
    }
    
    //MARK:- UICollectionViewDelegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTime.count
    }
    
    var selectindex:Int = -1
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MakeAppointmentCell", for: indexPath) as! MakeAppointmentCell
        
        let time = String(arrTime.object(at: indexPath.row) as! Int)
        cell.lblTime.text = time + ":00"
        
        if indexPath.row == selectindex {
            cell.lblTime.backgroundColor = appColor.appPrimaryColor//kAppDelegate.AppColor
            cell.lblTime.layer.borderColor = UIColor.clear.cgColor
            
        } else {
            cell.lblTime.border(UIColor.white, 3, 1.0)
            cell.lblTime.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MakeAppointmentCell", for: indexPath) as! MakeAppointmentCell
        
        let time = String(arrTime.object(at: indexPath.row) as! Int)
        cell.lblTime.text = time + ":00"
        let timeFormat = cell.lblTime.text!
        selectedTime = timeFormat.converDate("HH:mm", "HH:mm:ss")
        
        print("selectedTime---------------\(selectedTime)")//
        
        selectindex = indexPath.row
        collectionView.reloadData()
    }
    
    //MARK:- Check validations
    func checkValidation() -> String? {
        
        if tfIssueDetail.text?.count == 0  {
            return AlertMSG.blankIssueDetails
            
        } else if selectedTime.count == 0 {
            return AlertMSG.blankDate
            
        } else if selectedTime.count == 0 {
            return AlertMSG.blankTime
        }
        
        return nil
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scrollView.contentInset = UIEdgeInsets.zero
    }
    
    //MARK:- TEXTFIELD_DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfIssueDetail.resignFirstResponder()
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scrollView.contentInset = UIEdgeInsets.zero
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var length = (textField.text?.count)! + string.count - range.length
        let characterSet = CharacterSet.init(charactersIn: kAppDelegate.ACCEPTABLE_CHARACTERSWITHNO).inverted
        length = 250
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        let filter = string.components(separatedBy: characterSet).joined(separator:"")
        return ((string == filter) && newString.length <= length)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.contentSize = CGSize(width: view.frame.size.width, height: btnSubmit.frame.origin.y + btnSubmit.frame.size.height + 10)
    }
    
    /////////__________***********************_________
    //MARK:- KEYBOARD NOTIFICATION METHODS
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrollView.contentInset = UIEdgeInsets.zero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    
    /////////__________****************_________***************/////////
    //MARK:- ws AddAppointment
    func wsAddAppointment() {
        
        let defaults = UserDefaults.standard
        let myString = defaults.value(forKey: "savedId")
        let vehicle_id = myString
        let strDate = selectedDate.converDate("EEEE yyyy-MM-dd", "yyyy-MM-dd")
        let params = NSMutableDictionary()
        
        let appointment_datetime = strDate + " " + selectedTime
        print("selectedDate---------------\(appointment_datetime)")
        
        params["vehicle_id"] = vehicle_id
        params["shop_id"] = kAppDelegate.shopId
        params["estimate_id"] = estimateId
        params["appointment_datetime"] = appointment_datetime
        params["issue_details"] = tfIssueDetail.text!
        
        Http.instance().json(api_add_appointment, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    Http.alert("", string(json! , "message"))
                    self.tfIssueDetail.text = ""
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyAppointmentsVC") as! MyAppointmentsVC
                    self.navigationController?.pushViewController(vc, animated: false)
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    /*
     ================================================================
     api -http://ec2-54-193-77-74.us-west-1.compute.amazonaws.com/telenyze/api//shop/add_appointment-
     params -{
     "appointment_datetime" = "2018-02-29 9:30:00";
     "estimate_id" = "";
     "issue_details" = "Car issue";
     "shop_id" = "";
     "vehicle_id" = "";
     
     }-
     json -{
     "expire_token" = 0;
     message = "The Shop field is required.";
     success = 0;
     }-
     */
    
}//class ends here/////

class MakeAppointmentCell: UICollectionViewCell {
    @IBOutlet var lblTime: UILabel!
}

class weekTime : NSObject {
    var close = ""
    var created_at = ""
    var day = ""
    var deleted_at = ""
    var id = ""
    var open = ""
    var shop_id = ""
    var status = ""
    var updated_at = ""
}




