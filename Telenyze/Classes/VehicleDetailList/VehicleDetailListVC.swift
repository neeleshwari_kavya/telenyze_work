//
//  VehicleDetailListVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 05/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class VehicleDetailListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tblView: UITableView!

    var strVehicleName = ""
    var strStatusWithDate = ""
    
   var arrRES = NSMutableArray()
   var arrAlerts = NSMutableArray()
   var arrIssueList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      self.displayAndDelegates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //vehicleSelected = "BMW Q^500"
        setBackButton()
        setNav(className: "\(strVehicleName) Status")
        self.addCustomRefreshButtonNavigationBar()
        
        print("arrIssueList>>>\(arrIssueList)")
        /*
        // var dateUpdated = ""
        if let dateUTC = (json as AnyObject).object(forKey: "UTC") as? String {
            print("dateUTC>>>>\(dateUTC)")
            
            let someDateTime = Date(timeIntervalSince1970: Double(dateUTC)!) // Feb 2, 1997, 10:26 AM
            print("someDateTime>>>>\(someDateTime)")
        }
        */
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- FUNCTION
    
    func displayAndDelegates() {
        tblView.delegate = self
        tblView.dataSource = self
        
        let dummyViewHeight = CGFloat(40)
        self.tblView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.bounds.size.width, height: dummyViewHeight))
        self.tblView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        
        print("strVehicleName-->>>\(strVehicleName)")
        print("strStatusWithDate-->>>\(strStatusWithDate)")
        
        let dictResult =  self.arrIssueList.object(at: 0)
        
        if let alets = (dictResult as AnyObject).object(forKey: "ALRTS") as? NSArray {
            print("alets>>>>\(alets)")
            self.arrAlerts = (alets as? NSArray)?.mutableCopy() as! NSMutableArray
            print(" self.arrAlerts>>>>\( self.arrAlerts)")
        }
        
        if let res = (dictResult as AnyObject).object(forKey: "RES") as? NSArray {
            print("arrRES>>>>\(arrRES)")
            self.arrRES = (res as? NSArray)?.mutableCopy() as! NSMutableArray
            print(" arrRES>>>>\( self.arrRES)")
        }
        
    }
    
    //Button Actions
    @IBAction func actionRepairService(_ sender: Any) {
        print("actionRepairService")
        
    }
    @IBAction func actionAssistanceService(_ sender: Any) {
         print("actionAssistanceService")
    }
    
    @IBAction func actionInsuraceService(_ sender: Any) {
         print("actionInsuraceService")
    }
    @IBAction func actionDealers(_ sender: Any) {
         print("actionDealers")
    }
    
  
 /*
    func ViewFrameSet() {

        lblAlertTitle.frame.origin.y = tblViewFirstConstraintHeight.constant + tblView.frame.origin.y + 10
        lblViewHeader.frame.origin.y = lblAlertTitle.frame.size.height + lblAlertTitle.frame.origin.y + 10
        tblViewTwo.frame.origin.y = lblViewHeader.frame.size.height + lblViewHeader.frame.origin.y
        lblServiceTitle.frame.origin.y = tblViewFirstConstraintHeight.constant + tblViewTwo.frame.origin.y + 10
        viewService.frame.origin.y = lblServiceTitle.frame.size.height + lblServiceTitle.frame.origin.y + 10
        scroll.contentSize = CGSize(width: view.frame.size.width, height: viewService.frame.origin.y + viewService.frame.size.height + 10)
    }
    */
    func addCustomRefreshButtonNavigationBar() {
        let btnRefresh = UIButton.init(type: .custom)
        btnRefresh.setImage(UIImage.init(named: "refresh.png"), for: UIControlState.normal)
        btnRefresh.addTarget(self, action:#selector(btnRefreshAction), for:.touchUpInside)
        btnRefresh.frame = CGRect.init(x: 0, y: 0, width: 15, height: 15)
        let barButton = UIBarButtonItem.init(customView: btnRefresh)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func btnRefreshAction() {
        print("Right navigation Button")
        //do stuff here
        
    }
    
    //MARK:- TABLEVIEW_DELEGATE

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            
            // if arrCurrentAppointment.count != 0 {
            //  dictData = arrCurrentAppointment.object(at: indexPath.row) as! NSDictionary
            // }
          
            return 7
        }else if section == 1 {
            if arrAlerts.count == 0{
                
                  return 0
            }else{
                  return arrAlerts.count + 1
            }
            
        } else {
            return 1
        }
        
    }
    /*
    var dictData = NSDictionary()
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAppointmentCell", for: indexPath) as! MyAppointmentCell
        
        return cell
    }
    */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
        if indexPath.section == 0 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleDetailListCell") as! VehicleDetailListCell
            
            cell.img.layer.borderColor  = appColor.greenColor.cgColor
            cell.img.layer.borderWidth = 2.0
            cell.img.layer.cornerRadius = cell.img.layer.frame.size.height/2
            
             let dictData = arrRES.object(at: indexPath.row) as! NSDictionary
            print("arrRES>>>\(arrRES)")
            
            print("dictData>>>\(dictData)")
            
            let BAT = string(dictData, "BAT")
            let DTC = string(dictData, "DTC")
            let DTCI = string(dictData, "DTCI")
            let EMI = string(dictData, "EMI")
            let IAT = string(dictData, "IAT")
            let MIL = string(dictData, "MIL")
            let MPG = string(dictData, "MPG")
            let MPH = string(dictData, "MPH")
            let PERF = string(dictData, "PERF")
            let RPM = string(dictData, "RPM")
            let TEMP = string(dictData, "TEMP")
            
             print("BAT>>>\(BAT)")
             print("DTCI>>>\(DTCI)")
             print("EMI>>>\(EMI)")
             print("IAT>>>\(IAT)")
             print("MIL>>>\(MIL)")
             print("MPG>>>\(MPG)")
             print("MPH>>>\(MPH)")
             print("PERF>>>\(PERF)")
             print("RPM>>>\(RPM)")
             print("TEMP>>>\(TEMP)")
            
            if indexPath.row == 0{
                //1.battery
                cell.lblName.text = "Battery"
                cell.lblStatus.text = "\(BAT) V"
                
                if Int(BAT)! < 11 ||  Int(BAT)! > 15{
                     cell.img.image = UIImage(named: "ic_battery_green.png")
                    cell.lblStatus.textColor = appColor.redColor
                }else{
                    cell.img.image = UIImage(named: "ic_battery_red.png")
                      cell.lblStatus.textColor = appColor.redColor
                }
                
            }else  if indexPath.row == 1{
                //2.Emission
                 cell.lblName.text = "Emission"
                
                if EMI == "H" || EMI == "L"{
                   cell.img.image = UIImage(named: "ic_emission_red.png")
                    cell.lblStatus.textColor = appColor.redColor
                }else{
                    cell.img.image = UIImage(named: "ic_emission_green.png")
                      cell.lblStatus.textColor = appColor.redColor
                }
                
            }else  if indexPath.row == 2{
                //3.MPG
                
                 cell.lblName.text = "MPG"
                
                if MPG == "H" || MPG == "L"{
                    cell.img.image = UIImage(named: "ic_mpg_red.png")
                    cell.lblStatus.textColor = appColor.redColor
                }else{
                     cell.img.image = UIImage(named: "ic_mpg_green.png")
                      cell.lblStatus.textColor = appColor.redColor
                }
                
            }else  if indexPath.row == 3{
                //4.coolent temperature
               
                 cell.lblName.text = "Coolant Temperature"
                
                if  TEMP == "L"{
                      cell.img.image = UIImage(named: "ic_coolent_temp_red.png")
                    cell.lblStatus.textColor = appColor.redColor
                     cell.lblStatus.text = "LOW"
                }else if TEMP == "H" {
                    cell.img.image = UIImage(named: "ic_coolent_temp_red.png")
                    cell.lblStatus.textColor = appColor.redColor
                    cell.lblStatus.text = "HIGH"
                }else{
                      cell.img.image = UIImage(named: "ic_coolent_temp_green.png")
                      cell.lblStatus.textColor = appColor.redColor
                     cell.lblStatus.text = "NORMAL"
                }
                
            }else  if indexPath.row == 4{
                //5.Performance
                
                 cell.lblName.text = "Performance"
                
                if PERF == "L"{
                    cell.img.image = UIImage(named: "ic_performance_red.png")
                    cell.lblStatus.textColor = appColor.redColor
                     cell.lblStatus.text = "LOW"
                }else  if PERF == "H" {
                    cell.img.image = UIImage(named: "ic_performance_red.png")
                    cell.lblStatus.textColor = appColor.redColor
                     cell.lblStatus.text = "HIGH"
                    
                }else{
                    cell.img.image = UIImage(named: "ic_performance_green.png")
                    cell.lblStatus.textColor = appColor.redColor
                     cell.lblStatus.text = "NORMAL"
                }
                
            }else  if indexPath.row == 5{
                //6.fault code
                 cell.lblName.text = "Fault Code"
  
                if MIL == "ON" {
                    cell.img.image = UIImage(named: "ic_fault_green.png")
                    cell.lblStatus.textColor = appColor.redColor
                     cell.lblStatus.text = "ON"
                }else if Int(DTCI)! == 0 {
                    cell.img.image = UIImage(named: "ic_fault_green.png")
                    cell.lblStatus.textColor = appColor.redColor
                     cell.lblStatus.text = "0"
                }else{
                    cell.img.image = UIImage(named: "ic_fault_green.png")
                    cell.lblStatus.textColor = appColor.redColor
                     cell.lblStatus.text = "NORMAL"
                }
                
            }else  if indexPath.row == 6{
                //7.check engine
                cell.lblName.text = "Check Engine"
                
                 if MIL == "ON" {
                   cell.img.image = UIImage(named: "ic_check_engine_red.png")
                    cell.lblStatus.textColor = appColor.redColor
                     cell.lblStatus.text = "ON"
                }else if Int(DTCI)! == 0 {
                    cell.img.image = UIImage(named: "ic_check_engine_red.png")
                      cell.lblStatus.textColor = appColor.redColor
                     cell.lblStatus.text = "0"
                }else{
                     cell.img.image = UIImage(named: "ic_check_engine_green.png")
                    cell.lblStatus.textColor = appColor.greenColor
                     cell.lblStatus.text = "NORMAL"
                }
            }else {
                
                
            }

            return cell
        }else if indexPath.section == 1 {
            
            // if arrCurrentAppointment.count != 0 {
            //  dictData = arrCurrentAppointment.object(at: indexPath.row) as! NSDictionary
            // }
           
            if indexPath.row == 0{
                let cell = tblView.dequeueReusableCell(withIdentifier: "VehicleAlertStaticCell") as! VehicleAlertStaticCell
                
                
                return cell
            }else{
                
                let cell2 = tblView.dequeueReusableCell(withIdentifier: "VehicleAlertListCell") as! VehicleAlertListCell
                
             
                    let dictData = arrAlerts.object(at: indexPath.row) as! NSDictionary
                    print("arrAlerts>>>\(arrAlerts)")
                    
                cell2.lblCode.text = string(dictData, "CODE")
                cell2.lblType.text = string(dictData, "TYP")
                cell2.lblDescrip.text = string(dictData, "DES")
                
                return cell2
            }

        } else {
             let cell3 = tblView.dequeueReusableCell(withIdentifier: "ServiceCell") as! ServiceCell
            
            return cell3
        }
       
        /*
        var dateTime = string(dictData, "appointment_datetime")
        dateTime = dateTime.converDate("yyyy-MM-dd HH:mm:ss", "d MMM yyyy,HH:mm")
        let arr = dateTime.components(separatedBy: ",")
        
        print("dictData>>>\(dictData)")
        
        cell.lblName.text = string(dictData, "shop_name")
        cell.lblStatus.text = "Status: \(string(dictData, "status"))"
        cell.lblDate.text = arr[0]
        cell.lblTime.text = arr[1]
        
        */
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt section...\(indexPath.section)")
        print("didSelectRowAt indexPath...\(indexPath)")
      
        if indexPath.section == 0 {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WarningListVC") as! WarningListVC
            // vc.estimateId = string(dict, "id")
            // vc.arrData = arrEstimateList.mutableCopy() as! NSMutableArray
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
    //tableView viewForHeaderInSection
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var ht = CGFloat()
        /*
        if section == 0 {
            if arrCurrentAppointment.count == 0 {
                ht = 0
            }else{
                ht = 40
            }
        }else if section == 0 {
            if arrCurrentAppointment.count == 0 {
                ht = 0
            }else{
                ht = 40
            }
        
        } else {
            if arrPastAppointment.count == 0 {
                ht = 0
            }else{
                ht = 40
            }
            */
        
        //}
        ht = 40
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.frame.size.width, height: ht))
        
        let lblSectionTitle = UILabel(frame: CGRect(x: 0, y: 0, width: self.tblView.frame.size.width/2-20, height: ht))
        lblSectionTitle.font = UIFont.boldSystemFont(ofSize: 14.0)
        lblSectionTitle.textColor = appColor.whiteColorOpeque
        lblSectionTitle.numberOfLines = 2
       
        let lblUpdatedOnDate = UILabel(frame: CGRect(x: self.tblView.frame.size.width/2-20, y: 0, width: self.tblView.frame.size.width/2+20, height: ht))
        lblUpdatedOnDate.font = UIFont.boldSystemFont(ofSize: 12.0)
        lblUpdatedOnDate.textColor = appColor.whiteColorOpeque
        lblUpdatedOnDate.numberOfLines = 2
        lblUpdatedOnDate.textAlignment = .right
        
        if section == 0 {
            lblSectionTitle.text = "4 Issues Detected"//"\(arrCurrentAppointment.count) Current Appointments"
            lblUpdatedOnDate.text = "Last Updated:02/04/2018"//"\(arrCurrentAppointment.count) Current Appointments"
            
        }else if section == 1 {
            lblSectionTitle.text = "Alert"//"\(arrCurrentAppointment.count) Current Appointments"
             lblUpdatedOnDate.text = ""
        }else {
            lblSectionTitle.text = "Choose services"
            lblUpdatedOnDate.text = ""
        }
        
        viewHeader.addSubview(lblSectionTitle)
        viewHeader.addSubview(lblUpdatedOnDate)
        
        lblSectionTitle.backgroundColor = UIColor.green
        lblUpdatedOnDate.backgroundColor = UIColor.brown
        viewHeader.backgroundColor = UIColor.red
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }

   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
           return 65
        }else  if indexPath.section == 1 {
            if indexPath.row == 0{
                
                return 30
            }else{
                return 40
            }
        }else{
           return 80
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        /*
        if section == 0 {
            if arrCurrentAppointment.count == 0{
                return 0
            }else{
                return 40
            }
        }else{
            if arrPastAppointment.count == 0{
                return 0
            }else{
                return 40
            }
        }
        */
        return 40
  }
    
    /*
     override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
     return .leastNormalMagnitude
     }
 */

}//Sachin :]
///===========================================================

class VehicleDetailListCell: UITableViewCell {
    
    @IBOutlet var img: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblStatus: UILabel!
    
}
class VehicleAlertStaticCell: UITableViewCell {
    
    @IBOutlet var lblCode: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblDescrip: UILabel!
}

class VehicleAlertListCell: UITableViewCell {
    
    @IBOutlet var lblCode: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblDescrip: UILabel!
}

class ServiceCell: UITableViewCell {
    

}

