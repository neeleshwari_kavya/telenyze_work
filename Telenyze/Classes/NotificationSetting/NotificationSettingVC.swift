//
//  NotificationSettingVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 08/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class NotificationSettingVC: UIViewController {
    
    @IBOutlet var imgGetNotiSwich: UIImageView!
    @IBOutlet var imgAppRequestSwich: UIImageView!
    @IBOutlet var imgVehicleServiceSwitch: UIImageView!
    @IBOutlet var imgBackEndSwich: UIImageView!
    @IBOutlet var imgShopSwich: UIImageView!
    @IBOutlet var imgNewMsgSwich: UIImageView!
    
    //MARK:- VARIABLES
    var strSwich1 = ""
    var strSwich2 = ""
    var strSwich3 = ""
    var strSwich4 = ""
    var strSwich5 = ""
    var strSwich6 = ""

    let imgR_Off = UIImage(named: "swich_off.png")
    let imgR_On = UIImage(named: "swich_on.png")
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.BackNavigationButton()
        setNav(className: "Notification settings")

        let strNotificationStatus = string(kAppDelegate.dictUserInfo, "notification_status")
        
        let strAppoitmentRequest = string(kAppDelegate.dictUserInfo, "appoitment_request")
        
        let strVehicleService = string(kAppDelegate.dictUserInfo, "vehicle_service")
        
        let strFromBackEnd = string(kAppDelegate.dictUserInfo, "from_back_end")
        
        let strShopPromotional = string(kAppDelegate.dictUserInfo, "shop_promotional")
        
        let strNewMessage = string(kAppDelegate.dictUserInfo, "new_message")
        
        print("strNotificationStatus-=-=-=-=\(strNotificationStatus)")
        print("strAppoitmentRequest-=-=-=-=\(strAppoitmentRequest)")
        print("strVehicleService-=-=-=-=\(strVehicleService)")
        print("strNotificationStatus-=-=-=-=\(strNotificationStatus)")
        print("strShopPromotional-=-=-=-=\(strShopPromotional)")
        print("strNewMessage-=-=-=-=\(strNewMessage)")
        
        if strNotificationStatus == "on" {
            strSwich1 = "on"
            imgGetNotiSwich.image = imgR_On
        }else{
            strSwich1 = "off"
            imgGetNotiSwich.image = imgR_Off
        }
        
        if strAppoitmentRequest == "on"{
            strSwich2 = "on"
            imgAppRequestSwich.image = imgR_On
        }else{
            strSwich2 = "off"
            imgAppRequestSwich.image = imgR_Off
        }
        
        if strVehicleService == "on"{
            strSwich3 = "on"
            imgVehicleServiceSwitch.image = imgR_On
        }else{
            strSwich3 = "off"
            imgVehicleServiceSwitch.image = imgR_Off
        }
        
        if strFromBackEnd == "on"{
            strSwich4 = "on"
            imgBackEndSwich.image = imgR_On
        }else{
            strSwich4 = "off"
            imgBackEndSwich.image = imgR_Off
        }
        
        if strShopPromotional == "on"{
            strSwich5 = "on"
            imgShopSwich.image = imgR_On
        }else{
            strSwich5 = "off"
            imgShopSwich.image = imgR_Off
        }
        
        if strNewMessage == "on"{
            strSwich6 = "on"
            imgNewMsgSwich.image = imgR_On
        }else{
            strSwich6 = "off"
            imgNewMsgSwich.image = imgR_Off
        }
    }
    
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionBackButton() {
        _ = self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func actionSwichBtnAll(_ sender: Any) {
        let button = sender as! UIButton
        let indexbtn = button.tag
        
        if indexbtn == 1 {
            if strSwich1 == "on" {
                ws_Setting(para: "notification_status", value: "off")
                
            } else {
                ws_Setting(para: "notification_status", value: "on")
            }
            
        } else if indexbtn == 2 {
            if strSwich2 == "on" {
                ws_Setting(para: "appoitment_request", value: "off")
                
            } else {
                ws_Setting(para: "appoitment_request", value: "on")
            }
            
        } else if indexbtn == 3 {
            if strSwich3 == "on" {
                ws_Setting(para: "vehicle_service", value: "off")
                
            } else {
                ws_Setting(para: "vehicle_service", value: "on")
            }
            
        } else if indexbtn == 4 {
            if strSwich4 == "on" {
                ws_Setting(para: "from_back_end", value: "off")
                
            } else {
                ws_Setting(para: "from_back_end", value: "on")
            }
            
        } else if indexbtn == 5 {
            if strSwich5 == "on" {
                ws_Setting(para: "shop_promotional", value: "off")
                
            } else {
                ws_Setting(para: "shop_promotional", value: "on")
            }
            
        } else if indexbtn == 6 {
            if strSwich6 == "on" {
                ws_Setting(para: "new_message", value: "off")
                
            } else {
                ws_Setting(para: "new_message", value: "on")
            }
        }
    }
    
    func ws_Setting(para :String , value : String) {
        
        let params = NSMutableDictionary()
        params[para] = value
        print("params \n\(params)")
        print("value\(value)")
        print("tokenClass.getToken()\(tokenClass.getToken())")
        Http.instance().json(api_settings, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            print("tokenClass.getToken()-----\(tokenClass.getToken())")
            
            
            if json != nil {
                let json = json as? NSDictionary
                print("json-=-==-\(json!)")
                if number(json! , "success").boolValue {
                    let result = (json as AnyObject).object(forKey: "result") as? NSDictionary
             
                    if self.strSwich1 == "on" {
                        self.strSwich1 = "off"
                        self.imgGetNotiSwich.image = self.imgR_Off
                    } else {
                        self.strSwich1 = "on"
                        self.imgGetNotiSwich.image = self.imgR_On
                    }
                    
                    if self.strSwich2 == "on" {
                        self.strSwich2 = "off"
                        self.imgAppRequestSwich.image = self.imgR_Off
                    } else {
                        self.strSwich2 = "on"
                        self.imgAppRequestSwich.image = self.imgR_On
                    }
                    
                    if self.strSwich3 == "on" {
                        self.strSwich3 = "off"
                        self.imgVehicleServiceSwitch.image = self.imgR_Off
                    } else {
                        self.strSwich3 = "on"
                        self.imgVehicleServiceSwitch.image = self.imgR_On
                    }
                    
                    if self.strSwich4 == "on" {
                        self.strSwich4 = "off"
                        self.imgBackEndSwich.image = self.imgR_Off
                    } else {
                        self.strSwich4 = "on"
                        self.imgBackEndSwich.image = self.imgR_On
                    }
                    
                    if self.strSwich5 == "on" {
                        self.strSwich5 = "off"
                        self.imgShopSwich.image = self.imgR_Off
                    } else {
                        self.strSwich5 = "on"
                        self.imgShopSwich.image = self.imgR_On
                    }
                    
                    if self.strSwich6 == "on" {
                        self.strSwich6 = "off"
                        self.imgNewMsgSwich.image = self.imgR_Off
                    } else {
                        self.strSwich6 = "on"
                        self.imgNewMsgSwich.image = self.imgR_On
                    }
                    
                    /*let user = UserInfo.user1() as! User
                    user.notification_status = string(result!, "notification_status")
                    user.appoitment_request = string(result!, "appoitment_request")
                    user.vehicle_service = string(result!, "vehicle_service")
                    user.from_back_end = string(result!, "from_back_end")
                    user.shop_promotional = string(result!, "shop_promotional")
                    user.new_message = string(result!, "new_message")
                    User.save(user)*/
                    
                    kAppDelegate.dictUserInfo.removeObject(forKey: "notification_status")
                    kAppDelegate.dictUserInfo.removeObject(forKey: "appoitment_request")
                    kAppDelegate.dictUserInfo.removeObject(forKey: "vehicle_service")
                    kAppDelegate.dictUserInfo.removeObject(forKey: "from_back_end")
                    kAppDelegate.dictUserInfo.removeObject(forKey: "shop_promotional")
                    kAppDelegate.dictUserInfo.removeObject(forKey: "new_message")
                    
                    kAppDelegate.dictUserInfo["notification_status"] = string(result! , "notification_status")
                    kAppDelegate.dictUserInfo["appoitment_request"] = string(result! , "appoitment_request")
                    kAppDelegate.dictUserInfo["vehicle_service"] = string(result! , "vehicle_service")
                    kAppDelegate.dictUserInfo["from_back_end"] = string(result! , "from_back_end")
                    kAppDelegate.dictUserInfo["shop_promotional"] = string(result! , "shop_promotional")
                    kAppDelegate.dictUserInfo["new_message"] = string(result! , "new_message")
                    self.viewWillAppear(true)
                  
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
   
}//Sachin :]
