//
//  ConfirmVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 02/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit

class ConfirmVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func actionSkip(_ sender: Any) {
        
        let viewControllers = self.navigationController!.viewControllers as [UIViewController]
        for aViewController:UIViewController in viewControllers {
            if aViewController.isKind(of: SignInVC.self) {
                _ = self.navigationController?.popToViewController(aViewController, animated: false)
            }
        }
    }
    
    @IBAction func actionCheckEmail(_ sender: Any) {
        let googleUrlString = "https://www.google.co.in/"
        
        if let googleUrl = NSURL(string: googleUrlString) {
            // show alert to choose app
            if UIApplication.shared.canOpenURL(googleUrl as URL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(googleUrl as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(googleUrl as URL)
                }
            }
        }
    }
    /*
     override var prefersStatusBarHidden: Bool {
     return true
     }
     */
}

