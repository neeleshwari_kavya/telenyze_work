//
//  DashboardVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 05/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import HarishFrameworkSwift4

class DashboardVC: UIViewController, MKMapViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var tfMsg: UITextField!
    @IBOutlet var lblVehicleName: UILabel!
    @IBOutlet var lblStatusWithDate: UILabel!
    @IBOutlet var mkMap: MKMapView!
    @IBOutlet var lblNotificationNo: UILabel!
    @IBOutlet var lblCarLocation: UILabel!
    @IBOutlet var imgWarning: UIImageView!
    
    //MARK:- POPUP_VIEW
    @IBOutlet var lblPopupTitle: UILabel!
    @IBOutlet var viewPopup: UIView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var tfSearch: UITextField!
    @IBOutlet var lblNoRecord: UILabel!
    @IBOutlet var btnStringClear: UIButton!
    @IBOutlet var viewRepair: View!
    
    //MARK:- Alerts //new work on 20 march
    @IBOutlet var viewAlerts: UIView!
    @IBOutlet var viewPopUPAlert: UIView!
    @IBOutlet var btnAlertCANCEL: UIButton!
    @IBOutlet var tblViewAlerts: UITableView!
    @IBOutlet var imgParking: UIImageView!
    @IBOutlet var lblCurrentStatusSYSTEM: UILabel!
  //Notification
    @IBOutlet var viewPopUPNotification: UIView!
    @IBOutlet var btnNotificationOK: UIButton!
    @IBOutlet var tblViewNotification: UITableView!
    
    //Variables
    let btnHeaderCar = UIButton(type: .custom)
    let btnHeaderCloud = UIButton(type: .custom)
    var popShow = ""
    var arrVehicleList = NSMutableArray()
    var arrTable = NSMutableArray()
    var locationManager: CLLocationManager!
    var arrNotications = NSMutableArray()
    var arrAlerts = NSMutableArray()
    var arrIssueList = NSMutableArray()
    
    var Get_vinID = ""
    var timer: Timer?
    var totalAlertCount = "25"

    override func viewDidLoad() {
       
        super.viewDidLoad()
        
        
        mkMap.mapType = MKMapType.standard
        lblNoRecord.isHidden = true
        btnStringClear.isHidden = true
        self.automaticallyAdjustsScrollViewInsets = false
        self.displayAndDelegates()
       // addPopupShowNotificati
        
        print("dictCatchNotifications ----------->>>\(kAppDelegate.dictCatchNotifications)")
        
        viewPopUPNotification.isHidden = true
        
       // let someDateTime = Date(timeIntervalSince1970: Double(1511121389953)) // Feb 2, 1997, 10:26 AM
       // print("someDateTime>>>>\(someDateTime)")
        
     // let str =   UTCToLocal(date: "1511121389953")
        
       // let SADA = Date.init(timeIntervalSince1970: 1415637900)
        
        let date = NSDate(timeIntervalSince1970: 1415637900)
        print("str DateTime>>>>\(date)")
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "hh:mm a, dd MMM yyyy"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        print( " _ts value is \(dateString)")
        
        
        let localDateAsString = UTCToLocal(date: dateString, fromFormat: "hh:mm a, dd MMM yyyy", toFormat: "dd MMM yyyy")

        print("str DateTime>>>>\(localDateAsString)")
        
    }
    
    func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        
        return dateFormatter.string(from: dt!)
    }
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        menuNavigationButton()
        popShow = "no"
        self.ws_vehicleList()
        self.currentLocation()
        self.ws_MessageCount()
        self.setNavigationData()
        
        timer = Timer.scheduledTimer(timeInterval: 0.3,
                                     target: self,
                                     selector: #selector(self.run(_:)),
                                     userInfo: nil,
                                     repeats: true)
        actionHeaderCar()
        ws_NotificationList()
      
        self.setNavigationData()
       
    }
    
    func setNavigationData()  {

        var strCloudImage =  UIImage()
        var isNotificationHidden = false //
        
        self.totalAlertCount = String(self.arrNotications.count + self.arrAlerts.count)
        
        if Int(totalAlertCount) == 0 {
            isNotificationHidden = true
            viewRepair.backgroundColor = appColor.greenColor
        }else{
            isNotificationHidden = false
            viewRepair.backgroundColor = appColor.redColor
        }
        
        let reach = Reachability.init(hostname: "google.com")
        if (reach?.isReachable)! {
            strCloudImage = UIImage(named: "imgOnline.png")!
            //green image
        } else {
            strCloudImage = UIImage(named: "header_icon_cloud.png")!
            //gray image
        }

        setNavDashboard(className: "Telenyze", strNotification: totalAlertCount, strCloudImage: strCloudImage, isNotificationHidden: isNotificationHidden)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func displayAndDelegates(){
        viewRepair.border(UIColor.red, 0, 5.0)
        
        tblViewAlerts.delegate = self
        tblViewAlerts.dataSource = self
        
        lblNotificationNo.isHidden = true
        lblNotificationNo.sizeToFit()
    }
    
    func setNavDashboard(className:String,strNotification:String,strCloudImage:UIImage,isNotificationHidden:Bool)  {
        
        let btnNotificationNav = UIButton(type: .custom)
        let lblNotificationCountNav = UILabel()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = appColor.appPrimaryColor
        
        let titleViewNav = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 60, height: 40))
        self.navigationItem.titleView = titleViewNav
        
        let lbl1 = UILabel(frame: CGRect.init(x: 0 , y: 0, width: 50, height: 40))
        lbl1.text = className
        lbl1.font = UIFont.boldSystemFont(ofSize: 18.0)
        lbl1.textColor = UIColor.white
        lbl1.numberOfLines = 1
        lbl1.sizeToFit()
        lbl1.center.y = titleViewNav.center.y
        if lbl1.frame.size.width > self.view.frame.size.width - 170 {
            lbl1.frame.size.width = self.view.frame.size.width - 170
        }
        titleViewNav.addSubview(lbl1)
        
        let imgCar = imageWithImage(image: UIImage(named: "header_icon_car.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
        btnHeaderCar.setImage(imgCar, for: .normal)
        btnHeaderCar.frame = CGRect(x: lbl1.frame.origin.x + lbl1.frame.size.width + 10, y: 0, width: 20, height: 40)
        btnHeaderCar.addTarget(self, action: #selector(actionHeaderCar), for: .touchUpInside)
        btnHeaderCar.contentMode = .scaleAspectFit
        titleViewNav.addSubview(btnHeaderCar)
        
        
        let imgCloud = imageWithImage(image: strCloudImage, scaledToSize: CGSize(width: 15.0, height: 15.0))
        btnHeaderCloud.setImage(imgCloud, for: .normal)
        btnHeaderCloud.frame = CGRect(x: btnHeaderCar.frame.origin.x + btnHeaderCar.frame.size.width + 5, y: 0, width: 20, height: 40)
        btnHeaderCloud.contentMode = .scaleAspectFit
        btnHeaderCloud.addTarget(self, action: #selector(actionHeaderCloud), for: .touchUpInside)
        titleViewNav.addSubview(btnHeaderCloud)
        
        //////////////
       let imgMessage = imageWithImage(image: UIImage(named: "email.png")!, scaledToSize: CGSize(width: 20.0, height: 15.0))
        
        let btnMessage = UIButton(type: .custom)
        
        btnMessage.setImage(imgMessage, for: .normal)
        btnMessage.frame =  CGRect(x: titleViewNav.frame.size.width - 40, y: 5, width: 30, height: 30)
        btnMessage.addTarget(self, action: #selector(btnActionMessage), for: .touchUpInside)
        btnMessage.contentMode = .scaleAspectFit
        titleViewNav.addSubview(btnMessage)
        
        let imgN = imageWithImage(image: UIImage(named: "header_icon_bell.png")!, scaledToSize: CGSize(width: 20.0, height: 15.0))
        
        btnNotificationNav.setImage(imgN, for: .normal)
        btnNotificationNav.frame = CGRect(x: titleViewNav.frame.size.width - 80, y: 5, width: 30, height: 30)
        btnNotificationNav.contentMode = .scaleAspectFit
        btnNotificationNav.addTarget(self, action: #selector(actionNotification), for: .touchUpInside)
        titleViewNav.addSubview(btnNotificationNav)
        btnNotificationNav.backgroundColor = UIColor.clear
        
      
        lblNotificationCountNav.frame =  CGRect.init(x: btnNotificationNav.frame.origin.x + 15 , y: 0, width: 20, height: 20)
        
        lblNotificationCountNav.text = strNotification
        lblNotificationCountNav.font = UIFont.systemFont(ofSize: 10.0)
        lblNotificationCountNav.textColor = UIColor.white
        lblNotificationCountNav.numberOfLines = 1
        lblNotificationCountNav.backgroundColor = appColor.blueColor
        
      //  lblNotification.frame =  CGRect.init(x: btnNotificationNav.frame.origin.x + btnNotificationNav.frame.size.width - (lblNotification.frame.size.width) , y: 0, width: 20, height: 20)
    
        lblNotificationCountNav.textAlignment = .center
        titleViewNav.addSubview(lblNotificationCountNav)
        lblNotificationCountNav.layer.cornerRadius = lblNotificationCountNav.frame.size.width/2
        lblNotificationCountNav.clipsToBounds = true
      
        lblNotificationCountNav.isHidden = isNotificationHidden
        btnNotificationNav.isHidden = isNotificationHidden
        lblNotificationCountNav.text = totalAlertCount
        
        lblNotificationNo.isHidden = isNotificationHidden
        lblNotificationNo.text = String(arrNotications.count)
    }
    
    //MARK:- Fuctions
    func run(_ timer: AnyObject) {
        //print("Do your remaining stuff here...")
        checkIsInternetAvailable()
    }
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "MenuButton"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
  
    func actionHeaderCar()  {
        print("actionHeaderCar")
        ws_CarHeaderStatus()
    }
    
    func actionHeaderCloud()  {
        print("actionHeaderCloud")
        checkIsInternetAvailable()
    }
    
    func actionNotification()  {
        print("actionNotification")
        self.addPopupShowNotifications(viewMain: viewAlerts, viewPopUP: viewPopUPAlert)
    }
    
    func btnActionMessage()  {
        print("btnActionMessage")
    }
    
    //MARK:- Button Actions
    @IBAction func actionMsgSend(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionVehicleList(_ sender: Any) {
        self.view.endEditing(true)
        popShow = "yes"
        self.ws_vehicleList()
    }
    
    @IBAction func actionVehicleDetailList(_ sender: Any) {
        self.view.endEditing(true)
        //Http.alert("", "Under Working")
        //  var strVehicleSelected = ""
        //var strUpdatedOn = ""
        /*
         // var dateUpdated = ""
         if let dateUTC = (json as AnyObject).object(forKey: "UTC") as? String {
         print("dateUTC>>>>\(dateUTC)")
         
         let someDateTime = Date(timeIntervalSince1970: Double(dateUTC)!) // Feb 2, 1997, 10:26 AM
         print("someDateTime>>>>\(someDateTime)")
         }
 */

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VehicleDetailListVC") as! VehicleDetailListVC
        
        vc.strVehicleName = lblVehicleName.text!
        vc.strStatusWithDate = lblStatusWithDate.text!
         vc.arrIssueList = arrIssueList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionParking(_ sender: Any) {
        self.view.endEditing(true)
        Http.alert("", "Under Working")
    }
    
    @IBAction func actionRepairService(_ sender: Any) {
        self.view.endEditing(true)
        //Sachin comment
        
        //Http.alert("", "Under Working")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RepairShopVC") as! RepairShopVC
        kAppDelegate.menuPush = "backbtn"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionAssistanceService(_ sender: Any) {
        self.view.endEditing(true)
        Http.alert("", "Under Working")
    }
    
    @IBAction func actionInsuraceService(_ sender: Any) {
        self.view.endEditing(true)
        Http.alert("", "Under Working")
    }
    
    @IBAction func actionDealers(_ sender: Any) {
        self.view.endEditing(true)
        Http.alert("", "Under Working")
    }
    
    //MARK:- POPUP_ACTIONS ----------------------------------------
    @IBAction func actionStringClear(_ sender: Any) {
        tfSearch.text = ""
        btnStringClear.isHidden = true
        self.arrTable = self.arrVehicleList
        print("arrVehicleList-------\(arrVehicleList)")
        print("arrTable--------\(arrTable)")
        
        /*if tfSearch.text == "" {
            lblNoRecord.isHidden = true
        } else {
            lblNoRecord.isHidden = false
        }*/
        tblView.reloadData()
    }
    
    @IBAction func actionPopupClose(_ sender: Any) {
        self.view.endEditing(true)
        removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: subView)
    }
    
    @IBAction func btnNotificationOK(_ sender: Any) {
            viewPopUPNotification.isHidden = true
    }
    
    //MARK:- FUNCTION FOR POPVIEW ----------------------------------------
    func popupShow() {
        viewPopup.frame = self.view.frame
        viewPopup.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.window?.addSubview(viewPopup)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = (self as UIGestureRecognizerDelegate)
        self.viewPopup.addGestureRecognizer(tap)
        
        self.subView.leftAnchor.constraint(equalTo: viewPopup.leftAnchor, constant: 15).isActive = true
        self.subView.rightAnchor.constraint(equalTo: viewPopup.rightAnchor, constant: -15).isActive = true
        
        self.subView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        self.subView.heightAnchor.constraint(equalToConstant: 568).isActive = true
        self.test(viewTest: subView)
    }
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewPopup: UIView, viewWebShow: UIView) {
        let orignalT: CGAffineTransform = viewWebShow.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewWebShow.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewPopup.removeFromSuperview()
            viewWebShow.transform = orignalT
        })
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: subView)}

    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if subView.bounds.contains(touch.location(in: subView)) {
            return false
        }
        if viewAlerts.bounds.contains(touch.location(in: viewPopUPAlert)) {
            return false
        }
        return true
    }
    
    //MARK:- TABLEVIEW DELETE ----------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return arrTable.count
        if tableView == tblView{
            return arrTable.count
        }else if tableView == tblViewNotification{
            return arrNotications.count
        }else{
            return self.arrNotications.count// arrTable.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if tableView == tblView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardCell", for: indexPath) as! DashboardCell
            
            let dict = arrTable.object(at: indexPath.row) as! vehicleList
            cell.lblName.text = dict.make
            
            return cell
        }else if tableView == tblViewNotification{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuVendorNoticationCell", for: indexPath) as! MenuVendorNoticationCell
            let dict = arrNotications.object(at: indexPath.row) as! NSDictionary
            cell.lblNotification.text = string(dict, "title")
         
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuVendorAlertCell", for: indexPath) as! MenuVendorAlertCell
            let dict = arrAlerts.object(at: indexPath.row) as! NSDictionary
             cell.lblCode.text = string(dict, "CODE")
             cell.lblType.text = string(dict, "TYP")
             cell.lblDescription.text = string(dict, "DES")
            
            return cell
        }
      
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    if tableView == tblView{
        
        let dict = arrTable.object(at: indexPath.row) as! vehicleList
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "savedVehicle")
        defaults.removeObject(forKey: "savedId")
        defaults.set(dict.make, forKey: "savedVehicle")
        defaults.set(dict.id, forKey: "savedId")
        lblVehicleName.text! = dict.make.capFirstLetter()
           Get_vinID = dict.vin_number
        removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: subView)
        self.vehicleDetailData()
    
        }else{
            
        }
       
    }
    
    func vehicleDetailData() {
        let strVehicleName = (lblVehicleName.text!).uppercased()
        print("strVehicleName...\(strVehicleName)")

           // vc.strVehicleName = lblVehicleName.text!
       // vc.strStatusWithDate = lblStatusWithDate.text!
        var issueURL = ""
        
        if strVehicleName.contains("HONDA"){
            issueURL = "http://ec2-54-193-77-74.us-west-1.compute.amazonaws.com/telenyze/WBAAV53421FJ70536-2018-205.dash"
            
        }else  if strVehicleName.contains("BMW"){
            issueURL = "http://ec2-54-193-77-74.us-west-1.compute.amazonaws.com/telenyze/BMW-2018-205.dash"
            
        }else{
            issueURL = "http://ec2-54-193-77-74.us-west-1.compute.amazonaws.com/telenyze/kbtelenyze-toyota-51-2018.dash"
        }
        print("issueURL...\(issueURL)")
        ws_VehicalIssueList(issueURL:issueURL)
    }
    
    
    func ws_VehicalIssueList(issueURL:String) {
        //print("token-->>>\(tokenClass.getToken())")
        Http.instance().json(issueURL, nil, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                self.arrIssueList = (json as? NSArray)?.mutableCopy() as! NSMutableArray
                print("self.arrIssueList---->>>\(self.arrIssueList)")
                let dictResult =  self.arrIssueList.object(at: 0)
                
                if let alets = (dictResult as AnyObject).object(forKey: "ALRTS") as? NSArray {
                    print("alets>>>>\(alets)")
                    self.arrAlerts = (alets as? NSArray)?.mutableCopy() as! NSMutableArray
              
                    print(" self.arrAlerts>>>>\( self.arrAlerts)")
                    self.lblStatusWithDate.text = "\(self.arrAlerts.count) issues updated : "
                    self.tblViewAlerts.reloadData()
                }
            }
        }
    }
    
    /*
     func ws_VehicalIssueList(issueURL:String) {
        //print("token-->>>\(tokenClass.getToken())")
        Http.instance().json(issueURL, nil, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                   /*
                    if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                        print("result>>>>\(result)")
                    }
                    */
                    
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    */
    //////=============================
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfMsg {
            tfMsg.resignFirstResponder()
        } else if textField == tfSearch {
            tfSearch.resignFirstResponder()
        }
        
        return true
    }
    
    //MARK:- ARRAY DATA SEARCH-*-*-*-*-*-*-*--*-*
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength: Int = textField.text!.count + string.count - range.length
        
        if tfSearch.text == "0" {
            btnStringClear.isHidden = true
        } else {
            btnStringClear.isHidden = false
        }
        
        if textField == tfMsg {
            return (newLength > 250) ? false : true
        }
        
        if textField == tfSearch {
            if string == "\n" {
                textField.resignFirstResponder()
                return false
                
            } else {
                if newLength == 0 {
                    arrTable = arrVehicleList
                    print("arrVehicleList------\(arrVehicleList)")
                    print("arrTable--------\(arrTable)")
                    tblView.reloadData()
                } else {
                    search(strSearch: textField.text! + string)
                }
            }
        }
        
        /*if arrTable.count == 0 {
            lblNoRecord.isHidden = false
        } else {
            lblNoRecord.isHidden = true
        }*/
        
        return true
    }
    
    func search(strSearch:String) {
        let arr =  NSMutableArray()
        print("arrVehicleList--------\(arrVehicleList)")
        
        print("arrTable--------\(arrTable)")
        
        for i in 0 ..< arrVehicleList.count {
            let dict = arrVehicleList.object(at: i) as! vehicleList
            print("strSearch----\(strSearch)")
            if dict.make.lowercased().subString(strSearch.lowercased()) {
                arr.add(dict)
            }
        }
        
        arrTable = arr
        tblView.reloadData()
    }
    
    //MARK:- CURRENT_LAT_LONG
    func currentLocation() {
        
        let pick = kAppDelegate.curLocation()
        
        if pick != nil {
            if pick?.coordinate.latitude != 0.0 && pick?.coordinate.longitude != 0.0 {
                let sourceLocation = CLLocationCoordinate2D(latitude: CLLocationDegrees((pick?.coordinate.latitude)!), longitude: CLLocationDegrees((pick?.coordinate.longitude)!))
                let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
                let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
                let sourceAnnotation = MKPointAnnotation()
                sourceAnnotation.title = "Current location"
                
                if let location = sourcePlacemark.location {
                    sourceAnnotation.coordinate = location.coordinate
                }
                
                self.mkMap.showAnnotations([sourceAnnotation], animated: true )
                let directionRequest = MKDirectionsRequest()
                directionRequest.source = sourceMapItem
                directionRequest.transportType = .automobile
            }
        }
    }
    
    //MARK:- ANNIOTATION IDENTIFIER
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        let pinImage = UIImage(named: "imgCurrrentL.png")
        annotationView!.image = pinImage
        return annotationView
    }

    //MARK:- Web_Service_Call ----------------------------------
    func ws_vehicleList() {
        
        if kAppDelegate.arrGLVehicleList.count == 0 {
            let params = NSMutableDictionary()
            
            params["page"] = "1"
            params["id"] = ""
            params["search_value"] = ""
            
            Http.instance().json(api_vehiclelisting, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
                
                if json != nil {
                    let json = json as? NSDictionary
                    if number(json! , "success").boolValue {
                        
                        self.arrTable = NSMutableArray()
                        self.arrVehicleList = NSMutableArray()
                        
                        if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                            
                            for i in 0..<result.count {
                                let dict = result[i] as! NSDictionary
                                let ob = vehicleList()
                                ob.make = string(dict, "make")
                                ob.id = string(dict, "id")
                                self.arrVehicleList.add(ob)
                            }
                            
                            self.arrTable = self.arrVehicleList
                            kAppDelegate.arrGLVehicleList = self.arrVehicleList
                            self.tblView.reloadData()
                            var make = ""
                            var id = ""
                            var vin_nu = ""
                            
                            let defaults = UserDefaults.standard
                            let EmptyDict = defaults.value(forKey: "savedVehicle")
                            
                            if EmptyDict == nil {
                                if self.arrVehicleList.count > 0 {
                                    if let ob = self.arrVehicleList.object(at: 0) as? vehicleList {
                                        make = ob.make
                                        id = ob.id
                                        vin_nu = ob.vin_number
                                    }
                                }
                                
                                defaults.removeObject(forKey: "savedVehicle")
                                defaults.removeObject(forKey: "savedId")
                                defaults.set(make, forKey: "savedVehicle")
                                defaults.set(id, forKey: "savedId")
                                self.lblVehicleName.text = make
                                self.Get_vinID = vin_nu
                                self.vehicleDetailData()
                                
                            } else {
                                
                                let myString = defaults.value(forKey: "savedVehicle")
                                self.lblVehicleName.text! = myString as! String
                                self.Get_vinID = vin_nu
                                self.vehicleDetailData()
                            }
                        
                            if self.popShow == "yes" {
                                self.popupShow()
                            }
                        }
                    } else {
                        Http.alert("", string(json! , "message"))
                    }
                }
            }
        } else {
            
            self.arrTable = NSMutableArray()
            self.arrVehicleList = NSMutableArray()
            self.arrTable = kAppDelegate.arrGLVehicleList
            self.arrVehicleList = kAppDelegate.arrGLVehicleList
            self.tblView.reloadData()
            
            let defaults = UserDefaults.standard
            if let myString = defaults.value(forKey: "savedVehicle") {
                self.lblVehicleName.text! = myString as! String
                //Sourabh don't know.......
            }
            
            if self.popShow == "yes" {
                self.popupShow()
            }
        }
    }
    
    //Notification work ===========================Neeleshwari=================
    //Mark:Alerts=============================Neeleshwari

    //MARK:- FUNCTION FOR  Notifications POPVIEW
    func addPopupShowNotifications(viewMain: UIView, viewPopUP: UIView)  {
        viewMain.frame = self.view.frame
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.window?.addSubview(viewMain)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAlert(sender:)))
        tap2.delegate = (self as UIGestureRecognizerDelegate)
        viewMain.addGestureRecognizer(tap2)
        viewPopUP.leftAnchor.constraint(equalTo: viewPopUP.leftAnchor, constant: 15).isActive = true
        viewPopUP.rightAnchor.constraint(equalTo: viewPopUP.rightAnchor, constant: -15).isActive = true
        
        viewPopUP.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        viewPopUP.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.test2(viewTest: viewPopUP)
    }
    
    func test2(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewMain: UIView, viewPopUP: UIView) {
        let orignalT: CGAffineTransform = viewPopUP.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewMain.removeFromSuperview()
            viewPopUP.transform = orignalT
        })
    }
    
    @objc func handleTapAlert(sender: UITapGestureRecognizer? = nil) {
        removeSubViewWithAnimation(viewMain: viewAlerts, viewPopUP: viewPopUPAlert)
    }
    //Mark:sourabh sir work=============================
    
    func checkIsInternetAvailable(){
        let reach = Reachability.init(hostname: "google.com")
        
        if (reach?.isReachable)! {
            let imgCar = imageWithImage(image: UIImage(named: "imgOnline.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
            self.btnHeaderCloud.setImage(imgCar, for: .normal)
            self.btnHeaderCloud.isUserInteractionEnabled = false
            
             lblCurrentStatusSYSTEM.text = "Server Connected"
            
        }else if !(reach?.isReachable)!{
            lblCurrentStatusSYSTEM.text = "Server Not Connected"
        }else if !CLLocationManager.locationServicesEnabled() {
            lblCurrentStatusSYSTEM.text = "Location Off"
        }else if CLLocationManager.locationServicesEnabled() {
            lblCurrentStatusSYSTEM.text = "Location On"
        }else{
            let imgCar = imageWithImage(image: UIImage(named: "header_icon_cloud.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
            self.btnHeaderCloud.setImage(imgCar, for: .normal)
            self.btnHeaderCloud.isUserInteractionEnabled = false
        }
    }

     //MARK:- ws_CarHeaderStatus()
    
    func ws_CarHeaderStatus() {
        let URl = api_CarHeader_status + Get_vinID + "-" + string(kAppDelegate.dictUserInfo, "username") + "-obd.sts"
        print(URl)
        
        Http.instance().json("http://ec2-54-193-77-74.us-west-1.compute.amazonaws.com/telenyze/usrdata/kbtelenyze/12345678-kbtelenyze-obd.sts", nil, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            print(strJson)
            var data = NSString()
            data = "\(String(describing: json))" as NSString
            print(data)
            if data == "Optional(1)" {
                let imgCar = imageWithImage(image: UIImage(named: "header_icon_car_green.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
                self.btnHeaderCar.setImage(imgCar, for: .normal)
                self.btnHeaderCar.isUserInteractionEnabled = false
            }else{
                let imgCar = imageWithImage(image: UIImage(named: "header_icon_car.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
                self.btnHeaderCar.setImage(imgCar, for: .normal)
                self.btnHeaderCar.isUserInteractionEnabled = true
            }
        }
    }
    
    //MARK:- ws_NotificationList()  ----------------------------------
    func ws_NotificationList() {
        //print("token-->>>\(tokenClass.getToken())")
        Http.instance().json(api_notificationList, nil, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                        self.arrNotications = result.mutableCopy() as! NSMutableArray
                        print("arrNotications>>>>\(self.arrNotications)")
                         self.setNavigationData()
                         self.setViewRepairColorConditions()
                        self.tblViewAlerts.reloadData()
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }

    func setViewRepairColorConditions()  {

        for i in 0..<arrNotications.count{
            let dict = arrNotications[i] as! NSDictionary
            
            let type = string(dict, "type")
            let title = string(dict, "title")
            print("type....\(type)")
             print("title....\(title)")
            
            if type == "SHOP_CUSTOM_MSG"{
                print("SHOP_CUSTOM_MSG")
                
                if title == "Normal"{
                     viewRepair.backgroundColor = appColor.greenColor
                }else  if title == "Warning"{
                    
                    viewRepair.backgroundColor = appColor.yellowColor
                }else  if title == "Urgent"{
                     viewRepair.backgroundColor = appColor.redColor
                }else{
                    
                }
            }else if type == "QUOTED_ON_ESTIMATE" || type == "ACCEPT_APPOINTMENT" || type == "RESCHEDULE_APPOINTMENT" || type == "COMPLETED_APPOINTMENT"{
               // self.lblNotificationNo.text = "\(self.arrNotications.count)"
                print("shop type")
            }else{
                print("Other type")
                
            }
        }
    }

    //neeleshwari's work=============================
    //MARK:- new work for repair shop list and details=============================Neeleshwari

    func ws_MessageCount()  {
        
        Http.instance().json(api_notificationCount, nil, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                        print("result neeleshwari check notifications----->>>\(result)")
                       // self.arrNotications = result.mutableCopy() as! NSMutableArray
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }

     //new work for repair shop list and details=============================
    
    //MARK: Button Actions
    
    @IBAction func btnAlertCANCEL(_ sender: Any) {
        
        removeSubViewWithAnimation(viewMain: viewAlerts, viewPopUP: viewPopUPAlert)
    }

    //Mark:Alerts=============================Neeleshwari
}//Sachin :]

class DashboardCell : UITableViewCell {
    @IBOutlet var lblName: UILabel!
}

func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
    UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
    image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
    let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return newImage
}

//MARK:-MenuVendorNoticationCell
class MenuVendorAlertCell : UITableViewCell {
    
    @IBOutlet var lblCode: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblDescription: UILabel!
    
    
}




