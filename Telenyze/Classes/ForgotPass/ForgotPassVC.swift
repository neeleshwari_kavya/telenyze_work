//
//  ForgotPassVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 01/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class ForgotPassVC: UIViewController, UITextFieldDelegate , UIGestureRecognizerDelegate{
    
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet var btnSubmit: UIButton!
    
    //MARK:- VARIABLES
    var email = ""
    var username = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerForKeyboardNotifications()
        self.delegatesAndDisplayView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        tfEmail.placeHolderColor = UIColor.white
        self.BackNavigationButton()
        setNavTransparent(className: "Forgot Password")
        self.hideKeyboard()
    }
    
    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionBackButton()  {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
        self.deregisterFromKeyboardNotifications()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func buttonAction(sender: UIButton!) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Actions.
    @IBAction func actionSubmit(_ sender: Any) {
        
        self.hideKeyboard()
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            self.wsForgotPassword()
        }
    }
    
    //MARK: FUNCTIONS.
    func delegatesAndDisplayView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.delegate = self
        self.subView.addGestureRecognizer(tap)
        
        tfEmail.delegate = self
        tfEmail.tintColor = UIColor.white
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
        scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scroll.contentInset = UIEdgeInsets.zero
    }
    
    /////////__________***********************_________
    //MARK:- KEYBOARD NOTIFICATION METHODS
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scroll.contentInset = UIEdgeInsets.zero
        } else {
            scroll.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        
        scroll.scrollIndicatorInsets = scroll.contentInset
    }
    /////////__________***********************_________
    
    func checkValidation() -> String? {
        let string1 = tfEmail.text!
        if tfEmail.text?.count == 0 {
            return AlertMSG.blankEmailForgotPwd
        }else if (string1 as NSString).contains("@") && !Validate.email(tfEmail.text!) {
            
            return AlertMSG.invalidEmailFormat
        }
        return nil
    }
    
    //MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfEmail {
            tfEmail.resignFirstResponder()
            scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        if textField == tfEmail {
            return (length > 50) ? false : true
        }
        
        return true
    }
    //MARK: WS ForgotPassword
    func wsForgotPassword() {
        let string1 = tfEmail.text!
        
        if (string1 as NSString).contains("@") {
            email = tfEmail.text!
            username = ""
        } else {
            email = ""
            username = tfEmail.text!
        }
        
        let params = NSMutableDictionary()
        params["email"] = email
        params["username"] = username
        
        Http.instance().json(api_forgot_password, params, "POST", ai: true, popup: true, prnt: true) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                   // Http.alert("", string(json! , "message"))
                    let vc =  self.storyboard?.instantiateViewController(withIdentifier: "ResetPassVC") as! ResetPassVC
                    vc.strEmailUserName = self.tfEmail.text!
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
}//Sachin :]



