//
//  RepairShopDetailVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 13/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
// updated on 22marchh to main project ******=====+++updated

import UIKit
import MapKit
import HarishFrameworkSwift4

class RepairShopDetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MKMapViewDelegate, CLLocationManagerDelegate , UIGestureRecognizerDelegate {
    
    @IBOutlet var collectionAppoinment: UICollectionView!
    
    //MARK:- DETAIL_VIEW
    @IBOutlet var detailView: UIView!
    @IBOutlet var lblRatingAndReview: UILabel!
    @IBOutlet var lblPhoneNo: UILabel!
    @IBOutlet var lblDetailTime: UILabel!
    @IBOutlet var lblSetting: UILabel!
    @IBOutlet var map: MKMapView!
    @IBOutlet var imgDetail: UIImageView!
    
    //MARK:- SHOP_PROMOTION
    @IBOutlet var PromotionView: UIView!
    @IBOutlet var collectionPromotion: UICollectionView!
    
    //MARK:- SHOP_DISCOUNT
    @IBOutlet var DiscountView: UIView!
    @IBOutlet var pagingScroll: UIScrollView!
    
    //MARK:- feedback
    @IBOutlet var lblFeddbackTitle: UILabel!
    @IBOutlet var feedbackView: UIView!
   
    //MARK:- feedback_review
    @IBOutlet var tblView: UITableView!
    
    //MARK:- ReviewView
    @IBOutlet var showAllReviewView: UIView!
    @IBOutlet var btnEstimate: UIButton!
    
    //MARK:- Notifications //new work on 20 march
    @IBOutlet var tblViewNotifications: UITableView!
    @IBOutlet var ViewNotifications: UIView!
    @IBOutlet var viewPopUPNotification: UIView!
    @IBOutlet var btnNotificationOK: UIButton!
    @IBOutlet var btnAppointment: UIButton!
    
    @IBOutlet var viewEstimateButton: UIView!
    
    //MARK:- VARIBLES
    var shop_id = ""
    var lat = ""
    var long = ""
    var shopName = ""
    var fav_status = ""
    var rating_status = ""
    var indexOfView = 0
    var totalX: CGFloat = 0
    var imgArr = NSMutableArray()
    var strLatitude = String()
    var strLongitude = String()
    var arrPromotionlist = NSMutableArray()
    var arrDiscountList = NSMutableArray()
    var arrFeedbackList = NSMutableArray()
    var arrAppointments = NSMutableArray()
    var strNotificationIDS = ""
    
    var strAnnotationTitle = String()
    var locationManager: CLLocationManager!
    
    let itemsPerRow: CGFloat = 2
    let sectionInsets = UIEdgeInsets(top: 30.0, left: 14.0, bottom: 30.0, right: 14.0)
    
    var scrollView: UIScrollView!
    var arrNotificationList = NSMutableArray()
    var intEstimateStatus = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollViewAdd()
        self.displayAndDelegates()
        self.BackNavigationButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        map.mapType = MKMapType.standard
        collectionAppoinment.delegate = self
        collectionAppoinment.dataSource = self
        collectionPromotion.delegate = self
        collectionPromotion.dataSource = self
        self.ws_ShopDetail()
    }

    override func viewDidAppear(_ animated: Bool) {

    }
    func scrollViewAdd() {
        
        let screensize: CGRect = UIScreen.main.bounds
        let screenWidth = screensize.width
        let screenHeight = screensize.height
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 64, width: screenWidth, height: screenHeight - 64))
        view.addSubview(scrollView)
        scrollView.addSubview(collectionAppoinment)
        scrollView.addSubview(detailView)
        scrollView.addSubview(PromotionView)
        scrollView.addSubview(DiscountView)
        scrollView.addSubview(lblFeddbackTitle)
        scrollView.addSubview(feedbackView)
        scrollView.addSubview(tblView)
        scrollView.addSubview(showAllReviewView)
       // scrollView.addSubview(btnEstimate)
        scrollView.addSubview(viewEstimateButton)
        
        scrollView.isHidden = true
        feedbackView.frame.size.height = 0
        print("Sachin-----\(scrollView.frame.origin.y)")
    }
    
    func CustomNavigationBtn() {
        
        if self.arrNotificationList.count != 0{
            self.addPopupShowNotifications(viewMain: ViewNotifications, viewPopUP: viewPopUPNotification)
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = appColor.appPrimaryColor
        
        let titleViewNav = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 60, height: 40))
        var lbl1 = UILabel(frame: CGRect(x: 0, y: 0, width: titleViewNav.frame.size.width - 75, height: 40))
      
        if kAppDelegate.menuPush == "menubtn" {
        
            if rating_status == "1" {
                lbl1.frame = CGRect(x: 0, y: 0, width: titleViewNav.frame.size.width - 105, height: 40)
            }else{
                lbl1.frame = CGRect(x: 0, y: 0, width: titleViewNav.frame.size.width - 140, height: 40)
            }
        }else{
            
            if rating_status == "1" {
                lbl1.frame = CGRect(x: 0, y: 0, width: titleViewNav.frame.size.width - 75, height: 40)
            }else{
                lbl1.frame = CGRect(x: 0 , y: 0, width: titleViewNav.frame.size.width - 105, height: 40)
            }
        }
        
        lbl1.text = shopName
        lbl1.font = UIFont.boldSystemFont(ofSize: 16.0)
        lbl1.textColor = UIColor.white
        lbl1.numberOfLines = 2
        lbl1.lineBreakMode = .byWordWrapping
        titleViewNav.addSubview(lbl1)
        self.navigationItem.titleView = titleViewNav
        self.BackNavigationButtonMain()
          // titleViewNav.backgroundColor = UIColor.red
        //////////////
       
        let imgMessage = imageWithImage(image: UIImage(named: "email.png")!, scaledToSize: CGSize(width: 20.0, height: 15.0))
        let btnMessage = UIButton(type: .custom)
        btnMessage.setImage(imgMessage, for: .normal)
        btnMessage.frame =  CGRect(x: titleViewNav.frame.size.width - 35, y: 5, width: 30, height: 30)
        btnMessage.addTarget(self, action: #selector(btnNavigationMessageAction), for: .touchUpInside)
        btnMessage.contentMode = .scaleAspectFit
        titleViewNav.addSubview(btnMessage)
        
        //fav buttons
        var imgN = UIImage()
        
        if fav_status == "1" {
            
            imgN = imageWithImage(image: UIImage(named: "imgYellowHeart.png")!, scaledToSize: CGSize(width: 22.0, height: 20.0))
        } else {
            
            imgN = imageWithImage(image: UIImage(named: "imgHeart.png")!, scaledToSize: CGSize(width: 22.0, height: 20.0))
        }
        
        let btnFavShop = UIButton(type: .custom)
        btnFavShop.setImage(imgN, for: .normal)
        btnFavShop.frame = CGRect(x: titleViewNav.frame.size.width - 70, y: 5, width: 30, height: 30)
        btnFavShop.contentMode = .scaleAspectFit
        btnFavShop.addTarget(self, action: #selector(btnFavShopAction), for: .touchUpInside)
        titleViewNav.addSubview(btnFavShop)

        //================================================/////

        print("rating_status------\(rating_status)")
        let btnStar = UIButton(type: .custom)
        
        if rating_status == "1" {
        }else{
            let imgStar = imageWithImage(image: UIImage(named: "star.png")!, scaledToSize: CGSize(width: 22.0, height: 20.0))
           
            btnStar.setImage(imgStar, for: .normal)
            btnStar.frame = CGRect(x: titleViewNav.frame.size.width - 105, y: 5, width: 30, height: 30)
            btnStar.contentMode = .scaleAspectFit
            btnStar.addTarget(self, action: #selector(btnStarRateShopAction), for: .touchUpInside)
            titleViewNav.addSubview(btnStar)
        }
        
       //Call button ========================================///////
        
        if kAppDelegate.menuPush == "menubtn" {
            //setNav(className: "My Shops")
            //call shop number
            let imgCallShop = imageWithImage(image: UIImage(named: "call.png")!, scaledToSize: CGSize(width: 22.0, height: 20.0))
            
            let btnCallShop = UIButton(type: .custom)
            btnCallShop.setImage(imgCallShop, for: .normal)
            
            if rating_status == "1" {
                 btnCallShop.frame = CGRect(x: titleViewNav.frame.size.width - 105, y: 5, width: 30, height: 30)
            }else{
                 btnCallShop.frame = CGRect(x: titleViewNav.frame.size.width - 140, y: 5, width: 30, height: 30)
            }
           
            btnCallShop.contentMode = .scaleAspectFit
            btnCallShop.addTarget(self, action: #selector(btnCallShopAction), for: .touchUpInside)
            titleViewNav.addSubview(btnCallShop)
            //btnCallShop.backgroundColor = UIColor.green
        } else {
            //setNav(className: "Repair Shops")
        }
        ///============================
    }
    
    func btnStarRateShopAction() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RatingViewVC") as! RatingViewVC
        vc.shop_id = shop_id
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func btnCallShopAction() {
   
        var strMobNumber = self.lblPhoneNo.text!
//        print("strMobNumber--->>>\(strMobNumber)")
        if strMobNumber.count != 0{
            
            strMobNumber = strMobNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: "")
            
            callNumber(strMobNumber)
        }
    }
    
    func btnNavigationMessageAction() {
        print("button press message btn")
       // Http.alert("", "Under working")
      //  self.addPopupShowNotifications(viewMain: ViewNotifications, viewPopUP: viewPopUPNotification)
    }
    
    func btnFavShopAction() {
        let params = NSMutableDictionary()
        params["shop_id"] = shop_id
        
        Http.instance().json(api_addRemoveFav, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    
                    let msg = string(json!, "message")
                    let response = "Successfully added in favourite list !!"
                    
                    if response == msg {
                        self.fav_status = "1"
                        self.CustomNavigationBtn()
                    } else {
                        self.fav_status = "0"
                        self.CustomNavigationBtn()
                    }
                } else {
                }
            }
        }
    }

    func BackNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func BackNavigationButtonMain()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }

    func actionBackButton()  {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func actionFeedBack(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RatingViewVC") as! RatingViewVC
        vc.shop_id = shop_id
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func actionShowAllReview(_ sender: Any) {
        tblView.frame.size.height = CGFloat(arrFeedbackList.count * 65)
        tblView.reloadData()
        tblView.isScrollEnabled = false
        showAllReviewView.isHidden = true
        //  btnEstimate.frame.origin.y = tblView.frame.origin.y + 10 + tblView.frame.size.height
        //        self.btnAppointment.frame.origin.y =  self.btnEstimate.frame.origin.y
        //    scrollView.contentSize = CGSize(width: 0, height: btnEstimate.frame.origin.y + btnEstimate.frame.size.height + 10)
        
        viewEstimateButton.frame.origin.y = tblView.frame.origin.y + 10 + tblView.frame.size.height
          scrollView.contentSize = CGSize(width: 0, height: viewEstimateButton.frame.origin.y + viewEstimateButton.frame.size.height + 10)
    }
    
    @IBAction func actionEstimate(_ sender: Any) {
      //  self.ws_MyEstimate()
        print("intEstimateStatus>>>\(intEstimateStatus)")
        
        if intEstimateStatus != 0{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyEstimateVC") as! MyEstimateVC
            vc.shop_id = self.shop_id
            kAppDelegate.shopId = self.shop_id
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EstimateRequestVC") as! EstimateRequestVC
            vc.shop_id = self.shop_id
            kAppDelegate.shopId = self.shop_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnAppointment(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAppointmentVC") as! AddAppointmentVC
        
      //  vc.shop_id = self.shop_id
       kAppDelegate.shopId = self.shop_id
        
          //  vc.shop_id = self.shop_id
      //  vc.estimateId = string(dict, "id")
      //  vc.arrData = arrEstimateList.mutableCopy() as! NSMutableArray
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:- TABLEVIEW_DELEGATE ============Changes on 21 march======= neeleshwari
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return arrFeedbackList.count
        if tableView == tblView {
            
            return arrFeedbackList.count
        }else{
            
            return arrNotificationList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblView {
            
            let cell = tblView.dequeueReusableCell(withIdentifier: "RepairShopFeedbackCell", for: indexPath) as! RepairShopFeedbackCell
            
            let dict = arrFeedbackList.object(at: indexPath.row) as! feedback
            
            cell.lblMyName.text = dict.first_name.capFirstLetter()
            cell.lblMyFeedback.text = dict.feedback.capFirstLetter()
            cell.lblRatingNo.text = dict.avg_rating
            cell.lblDate.text = dict.created_at.converDate("yyyy-MM-dd HH:mm:ss", "dd MMM")
            
            return cell
        }else{
            
            let cell = tblViewNotifications.dequeueReusableCell(withIdentifier: "MenuVendorNoticationCell", for: indexPath) as! MenuVendorNoticationCell
            
            let dict = arrNotificationList.object(at: indexPath.row) as! NSDictionary
            
            cell.lblNotification.text = string(dict, "title")
//            cell.lblMyFeedback.text = dict.feedback.capFirstLetter()
//            cell.lblRatingNo.text = dict.avg_rating
//            cell.lblDate.text = dict.created_at.converDate("yyyy-MM-dd HH:mm:ss", "dd MMM")
            
            return cell
        }
    }
    
    //MARK:- COLLECTIONVIEW DEGEGATE
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionAppoinment {
            print(arrAppointments.count)
            return arrAppointments.count
        } else if collectionView == collectionPromotion {
            return arrPromotionlist.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionAppoinment {
            
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionAppoinmentCell", for: indexPath as IndexPath) as! collectionAppoinmentCell
            
            let dict = arrAppointments[indexPath.row] as! NSDictionary
            
            cellA.lblMyAppoinment.text! = "My Appointment"
            cellA.lblDate.text! = string(dict, "appointment_datetime").converDate("yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy")
            cellA.lblTime.text! = string(dict, "appointment_datetime").converDate("yyyy-MM-dd HH:mm:ss", "HH:mm")
            cellA.lblStatus.text! = string(dict, "status")
            return cellA
            
        } else {
            
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "RepairShopPromotionCell", for: indexPath) as! RepairShopPromotionCell
            
            let dict = arrPromotionlist.object(at: indexPath.row) as! promotionShops
            let Sachin = (kAppDelegate.server?.base_url)! + dict.image
            
            if dict.image.isEmpty {
                cellB.shopImg.image = UIImage(named: "noimage.jpg")
            } else {
                let url = NSURL(string: Sachin)
                cellB.shopImg.af_setImage(withURL: url as URL!)
            }
            
            return cellB
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionPromotion {
            let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
            let availableWidth = view.frame.width - paddingSpace
            let widthPerItem = availableWidth / itemsPerRow
            return CGSize(width: widthPerItem + 5, height: widthPerItem + 5)
            
        } else if collectionView == collectionAppoinment {
            return CGSize(width: collectionAppoinment.frame.size.width, height: collectionAppoinment.frame.size.height)
            
        } else {
            return CGSize()
        }
    }

    //MARK:- FUNTION_FOR_IMAGE_SCROLL
    func makeImgForSwipe() {
        if imgArr.count == 0 {
            pagingScroll.isHidden = true
        } else {
            pagingScroll.isHidden = false
        }
        
        for i in 0..<imgArr.count {
            let imgView = UIImageView()
            imgView.frame = CGRect(x: totalX, y: 0, width: pagingScroll.frame.size.width, height: pagingScroll.frame.size.height)
            
            totalX += imgView.frame.size.width
            var str = NSString()
            str = imgArr.object(at: i) as! NSString
            let dictImage = (kAppDelegate.server?.base_url)! + (str as String)
            
            if i == 0 {
                //lblTitle.text = string(dict, "ImageTitle")
            }
            
            let url = URL(string: dictImage)
            if  let data = try? Data(contentsOf: url!) {
                imgView.image = UIImage(data: data)
            }
            pagingScroll.addSubview(imgView)
            pagingScroll.autoresizesSubviews = true
        }
        
        pagingScroll.contentSize = CGSize(width: totalX, height: 0)
        pagingScroll.isScrollEnabled = false
        
        let rightGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRight))
        rightGestureRecognizer.direction = (.right)
        pagingScroll.addGestureRecognizer(rightGestureRecognizer)
        
        let leftGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeLeft))
        leftGestureRecognizer.direction = (.left)
        pagingScroll.addGestureRecognizer(leftGestureRecognizer)
    }
    
    func swipeLeft() {
        self.view.isUserInteractionEnabled = false
        
        if indexOfView == imgArr.count - 1 {
            self.view.isUserInteractionEnabled = true
        } else {
            self.indexOfView += 1
            let offSetX = self.pagingScroll.contentOffset.x
            let point = CGPoint(x: offSetX + pagingScroll.frame.size.width, y: 0)
            self.pagingScroll.setContentOffset(point, animated: true)
            
            let deadlineTime = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    func swipeRight() {
        self.view.isUserInteractionEnabled = false
        if indexOfView == 0 {
            self.view.isUserInteractionEnabled = true
        } else {
            self.indexOfView -= 1
            let offSetX = self.pagingScroll.contentOffset.x
            let point = CGPoint(x: offSetX - pagingScroll.frame.size.width, y: 0)
            self.pagingScroll.setContentOffset(point, animated: true)
            
            let deadlineTime = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    //------------------------------ Framing Start ------------------------------//
    func SachinFrameFirst() {
        
        if self.arrAppointments.count == 0 {
            self.collectionAppoinment.isHidden = true
            self.detailView.frame.origin.y = self.collectionAppoinment.frame.origin.y
            self.PromotionView.frame.origin.y = self.detailView.frame.origin.y + self.detailView.frame.size.height
            self.DiscountView.frame.origin.y = self.PromotionView.frame.origin.y + self.PromotionView.frame.size.height
            self.lblFeddbackTitle.frame.origin.y = self.DiscountView.frame.origin.y + self.DiscountView.frame.size.height
            
        } else {
            self.collectionAppoinment.isHidden = false
            self.detailView.frame.origin.y = self.collectionAppoinment.frame.origin.y + 10 + self.collectionAppoinment.frame.size.height
            self.PromotionView.frame.origin.y = self.detailView.frame.origin.y + self.detailView.frame.size.height
            self.DiscountView.frame.origin.y = self.PromotionView.frame.origin.y + self.PromotionView.frame.size.height
            self.lblFeddbackTitle.frame.origin.y = self.DiscountView.frame.origin.y + self.DiscountView.frame.size.height
        }
        
        SameAllFrame()
    }
    
    func SachinFrameSecond() {
        
        print("arrPromotionlist.count-----\(arrPromotionlist.count)")
        print("arrDiscountList.count-----\(arrDiscountList.count)")
        
        if arrPromotionlist.count >= 1 {
            PromotionView.isHidden = false
            self.PromotionView.frame.origin.y = self.detailView.frame.origin.y + self.detailView.frame.size.height
            self.DiscountView.frame.origin.y = self.PromotionView.frame.origin.y + self.PromotionView.frame.size.height
            self.lblFeddbackTitle.frame.origin.y = self.DiscountView.frame.origin.y + self.DiscountView.frame.size.height
            SameAllFrame()
            
        } else {
            PromotionView.isHidden = true
            self.DiscountView.frame.origin.y = self.detailView.frame.origin.y + self.detailView.frame.size.height
            self.lblFeddbackTitle.frame.origin.y = self.DiscountView.frame.origin.y + self.DiscountView.frame.size.height
            SameAllFrame()
        }
        
        if arrDiscountList.count >= 1 {
            DiscountView.isHidden = false
            
            if arrPromotionlist.count >= 1 {
                self.PromotionView.frame.origin.y = self.detailView.frame.origin.y + self.detailView.frame.size.height
                self.DiscountView.frame.origin.y = self.PromotionView.frame.origin.y + self.PromotionView.frame.size.height
                self.lblFeddbackTitle.frame.origin.y = self.DiscountView.frame.origin.y + self.DiscountView.frame.size.height
            } else {
                
                PromotionView.isHidden = true
                self.DiscountView.frame.origin.y = self.detailView.frame.origin.y + self.detailView.frame.size.height
                self.lblFeddbackTitle.frame.origin.y = self.DiscountView.frame.origin.y + self.DiscountView.frame.size.height
            }
        } else {
            DiscountView.isHidden = true
            
            if arrPromotionlist.count >= 1 {
                self.PromotionView.frame.origin.y = self.detailView.frame.origin.y + self.detailView.frame.size.height
                self.lblFeddbackTitle.frame.origin.y = self.PromotionView.frame.origin.y + self.PromotionView.frame.size.height
                
            } else {
                DiscountView.isHidden = true
                PromotionView.isHidden = true
                self.lblFeddbackTitle.frame.origin.y = self.detailView.frame.origin.y + self.detailView.frame.size.height
            }
        }
        
        SameAllFrame()
  //      self.scrollView.contentSize = CGSize(width: 0, height: self.btnEstimate.frame.origin.y + self.btnEstimate.frame.size.height + 10)
        
        self.scrollView.contentSize = CGSize(width: 0, height: self.viewEstimateButton.frame.origin.y + self.viewEstimateButton.frame.size.height + 10)
        
        self.ws_reviewList()
    }
    
    func SameAllFrame() {
        
        print("rating_status------\(rating_status)")
        
        if rating_status == "1" {
            feedbackView.isHidden = true
            self.tblView.frame.origin.y = self.lblFeddbackTitle.frame.origin.y + self.lblFeddbackTitle.frame.size.height
            self.showAllReviewView.frame.origin.y = self.tblView.frame.origin.y + self.tblView.frame.size.height
           // self.btnEstimate.frame.origin.y = self.showAllReviewView.frame.origin.y + 10 + self.showAllReviewView.frame.size.height
              //  self.btnAppointment.frame.origin.y =  self.btnEstimate.frame.origin.y
            self.viewEstimateButton.frame.origin.y = self.showAllReviewView.frame.origin.y + 10 + self.showAllReviewView.frame.size.height
            
            
        } else {
            feedbackView.isHidden = false
            self.feedbackView.frame.origin.y = self.lblFeddbackTitle.frame.origin.y + self.lblFeddbackTitle.frame.size.height
            self.tblView.frame.origin.y = self.feedbackView.frame.origin.y + 10 + self.feedbackView.frame.size.height
            self.showAllReviewView.frame.origin.y = self.tblView.frame.origin.y + self.tblView.frame.size.height
       //     self.btnEstimate.frame.origin.y = self.showAllReviewView.frame.origin.y + 10 + self.showAllReviewView.frame.size.height
           //     self.btnAppointment.frame.origin.y =  self.btnEstimate.frame.origin.y
             self.viewEstimateButton.frame.origin.y = self.showAllReviewView.frame.origin.y + 10 + self.showAllReviewView.frame.size.height
        }
    }
    
    //----------------------------- Frame End ----------------------------------//
    //MARK:-ws_ShopDetail ====================================================
    func ws_ShopDetail() {
        let defaults = UserDefaults.standard
        let myString = defaults.value(forKey: "savedId")
        let vehicle_id = myString
        
         var notification_ids = ""
       
        for i in 0..<arrNotificationList.count{
            if let dict :NSDictionary = arrNotificationList.object(at: i) as? NSDictionary{
                let id = string(dict, "id")
                print("id>>>\(id)")
            if i == 0{
               notification_ids.append(id)
                
            }else{
                notification_ids.append(",")
                notification_ids.append(id)
                }
            }
         }
        print("notification_ids>>>\(notification_ids)")
        
        let params = NSMutableDictionary()
        params["shop_id"] = shop_id
        params["lat"] = lat
        params["long"] = long
        params["vehicle_id"] = vehicle_id
        params["notification_ids"] = notification_ids

        Http.instance().json(api_shopListDetail, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            print("json>>>\(json!)")
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    kAppDelegate.dictShopNeeleshwari = json?.mutableCopy() as! NSMutableDictionary
                    
                   print("kAppDelegate.dictShopNeeleshwari>>>", kAppDelegate.dictShopNeeleshwari)
                    
                    if let dict = (json as AnyObject).object(forKey: "result") as? NSDictionary {
                        self.strLatitude = string(dict, "lat")
                        self.strLongitude = string(dict, "long")
                        let rating = string(dict, "rating")
                        self.fav_status = string(dict, "fav_status")
                        let review = " (" + string(dict, "review") + " Reviews)"
                       
                        self.intEstimateStatus = Int(string(dict, "estimate_status"))!
                        print("self.intEstimateStatus>>>>\(self.intEstimateStatus)")
                        
                        
                        let vehicle_image = string(dict, "filename")
                        let Sachin = (kAppDelegate.server?.base_url)! + vehicle_image
                        
                        if vehicle_image.isEmpty {
                            self.imgDetail.image = UIImage(named: "logo.png")
                        } else {
                            let url = NSURL(string: Sachin)
                            self.imgDetail.af_setImage(withURL: url as URL!)
                        }
                        
                        let attrs1 = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 15), NSForegroundColorAttributeName : UIColor.gray]
                        
                        let attrs2 = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 15), NSForegroundColorAttributeName : appColor.appPrimaryColor]
                        
                        let String1 = NSMutableAttributedString(string: rating, attributes:attrs1)
                        let String2 = NSMutableAttributedString(string: review, attributes:attrs2)
                        
                        String1.append(String2)
                        self.lblRatingAndReview.attributedText = String1
                        
                        self.lblPhoneNo.text! = string(dict, "contact_number")
                        self.lblDetailTime.text! = string(dict, "created_at")
                        self.lblSetting.text! = string(dict, "address").capFirstLetter()
                        self.rating_status = string(dict, "rating_status")
                        
                        self.selectedShopLocation(dict:dict)
                    }
                    
                    if let appointments = (json as AnyObject).object(forKey: "appointments") as? NSArray {
                        self.arrAppointments = (appointments.mutableCopy() as? NSMutableArray)!
                    }
                    
                    self.collectionAppoinment.reloadData()
                    self.SachinFrameFirst()
                    
                    if let promotions = (json as AnyObject).object(forKey: "promotions") as? NSArray {
                        
                        for i in 0..<promotions.count {
                            let dict = promotions[i] as! NSDictionary
                            let ob = promotionShops()
                            ob.created_at = string(dict, "created_at")
                            ob.deleted_at = string(dict, "deleted_at")
                            ob.descrip = string(dict, "description")
                            ob.id = string(dict, "id")
                            ob.image = string(dict, "image")
                            ob.link = string(dict, "link")
                            ob.priority = string(dict, "priority")
                            ob.shop_id = string(dict, "shop_id")
                            ob.status = string(dict, "status")
                            ob.title = string(dict, "title")
                            ob.updated_at = string(dict, "updated_at")
                            self.arrPromotionlist.add(ob)
                        }
                        self.collectionPromotion.reloadData()
                    }
                    
                    if let discounts = (json as AnyObject).object(forKey: "discounts") as? NSArray {
                        
                        for i in 0..<discounts.count {
                            let dict = discounts[i] as! NSDictionary
                            self.arrDiscountList.add(string(dict, "image"))
                        }
                        
                        self.imgArr = self.arrDiscountList
                        self.makeImgForSwipe()
                    }
                    
                    self.SachinFrameSecond()
                    self.CustomNavigationBtn()
                    
                } else {
                    Http.alert("", string(json! , "message"))
                    let expire_token = string(json! , "expire_token")
                    let message = string(json! , "message")
                    
                    if expire_token == "1" {
                        let alert = UIAlertController (title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        
                        self.present(alert, animated: true, completion: nil)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            
                            
                            let viewControllers = self.navigationController!.viewControllers as [UIViewController]
                            
                            for aViewController:UIViewController in viewControllers {
                                if aViewController.isKind(of: SignInVC.self) {
                                    _ = self.navigationController?.popToViewController(aViewController, animated: false)
                                    kAppDelegate.arrGLVehicleList = NSMutableArray()
                                    kAppDelegate.dictUserInfo = NSMutableDictionary()
                                }
                            }
                            
                            /*self.navigationController?.viewControllers = []
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                           
                            self.navigationController?.pushViewController(vc, animated: false)*/
                            
                            /*var boolPoped = false
                            
                            for vc in (self.navigationController?.viewControllers)! {
                                
                                if vc is SignInVC {
                                    boolPoped = true
                                    self.navigationController?.popToViewController(vc, animated: false)
                                }
                            }
                            
                            if !boolPoped {
                                
                                self.navigationController?.viewControllers = []
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                                kAppDelegate.arrGLVehicleList = NSMutableArray()
                                kAppDelegate.dictUserInfo = NSMutableDictionary()
                                self.navigationController?.pushViewController(vc, animated: false)
                            }*/
                        }))
                        
                    } else {
                        Http.alert("", string(json! , "message"))
                        self.CustomNavigationBtn()
                    }
                    
                }
            }
        }
    }
    
    //------------------------------ Framing End ------------------------------//
    
    //api_reviewList
    func ws_reviewList() {
        
        let params = NSMutableDictionary()
        params["shop_id"] = shop_id
        params["page"] = "1"
        
        Http.instance().json(api_reviewList, params, "POST", ai: true, popup: false, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrFeedbackList = NSMutableArray()
                    if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                        for i in 0..<result.count {
                            let dict = result[i] as! NSDictionary
                            let ob = feedback()
                            ob.avg_rating = string(dict, "avg_rating")
                            ob.created_at = string(dict, "created_at")
                            ob.feedback = string(dict, "feedback")
                            ob.first_name = string(dict, "first_name")
                            ob.id = string(dict, "id")
                            ob.last_name = string(dict, "last_name")
                            self.arrFeedbackList.add(ob)
                        }
                        
                        self.tblView.reloadData()
                        self.tblView.frame.size.height = 65
                        self.tblView.isScrollEnabled = false
                        
                        let arrCount = self.arrFeedbackList.count
                        
                        if arrCount == 0 {
                            self.lblFeddbackTitle.text! = "Feedback" + " (" + String(0) + " Reviews)"
                        } else {
                            self.lblFeddbackTitle.text! = "Feedback" + " (" + String(arrCount) + " Reviews)"
                        }
                        
                        if arrCount == 1 {
                            self.tblView.isHidden = false
                            self.showAllReviewView.isHidden = true
                          //  self.btnEstimate.frame.origin.y = self.tblView.frame.origin.y + 10 + self.tblView.frame.size.height
                            //    self.btnAppointment.frame.origin.y =  self.btnEstimate.frame.origin.y
                            
                             self.viewEstimateButton.frame.origin.y = self.tblView.frame.origin.y + 10 + self.tblView.frame.size.height
                            
                        } else {
                            self.tblView.isHidden = false
                            self.showAllReviewView.isHidden = false
                            self.showAllReviewView.frame.origin.y = self.tblView.frame.origin.y - 1 + self.tblView.frame.size.height
                          //  self.btnEstimate.frame.origin.y = self.showAllReviewView.frame.origin.y + 10 + self.showAllReviewView.frame.size.height
                              //  self.btnAppointment.frame.origin.y =  self.btnEstimate.frame.origin.y
                             self.viewEstimateButton.frame.origin.y = self.showAllReviewView.frame.origin.y + 10 + self.showAllReviewView.frame.size.height
                        }
                        
                        self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                        self.scrollView.isHidden = false
                        
                        self.scrollView.contentSize = CGSize(width: 0, height: self.viewEstimateButton.frame.origin.y + self.viewEstimateButton.frame.size.height + 10)
                        
                    }
                } else {
                    
                    let expire_token = string(json! , "expire_token")
                    
                    if expire_token == "1" {
                        let alert = UIAlertController (title: "", message: expire_token , preferredStyle: UIAlertControllerStyle.alert)
                        
                        self.present(alert, animated: true, completion: nil)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            
                            /*var boolPoped = false
                            
                            for vc in (self.navigationController?.viewControllers)! {
                                
                                if vc is SignInVC {
                                    boolPoped = true
                                    self.navigationController?.popToViewController(vc, animated: false)
                                }
                            }
                            
                            if !boolPoped {
                                
                                self.navigationController?.viewControllers = []
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                                kAppDelegate.arrGLVehicleList = NSMutableArray()
                                kAppDelegate.dictUserInfo = NSMutableDictionary()
                                self.navigationController?.pushViewController(vc, animated: false)
                            }*/
                        }))
                        
                    } else {
                        self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                        self.scrollView.isHidden = false
                        Http.alert("", string(json! , "message"))
                        
                        self.tblView.isHidden = true
                        self.showAllReviewView.isHidden = true
                        
                        self.lblFeddbackTitle.text! = "Feedback" + " (" + String(0) + " Reviews)"
                        
                        self.feedbackView.frame.origin.y = self.lblFeddbackTitle.frame.origin.y + self.lblFeddbackTitle.frame.size.height
                        
                    //    self.btnEstimate.frame.origin.y = self.feedbackView.frame.origin.y + 10 + self.feedbackView.frame.size.height
                      //      self.btnAppointment.frame.origin.y =  self.btnEstimate.frame.origin.y
                        self.viewEstimateButton.frame.origin.y = self.feedbackView.frame.origin.y + 10 + self.feedbackView.frame.size.height
                        
                       self.scrollView.contentSize = CGSize(width: 0, height: self.viewEstimateButton.frame.origin.y + self.viewEstimateButton.frame.size.height + 10)
                      //  self.scrollView.contentSize = CGSize(width: 0, height: self.btnEstimate.frame.origin.y + self.btnEstimate.frame.size.height + 10)
                    }
                }
            }
        }
    }
    
    //MARK:- MyEstimate List --------------------
    func ws_MyEstimate() {
        let defaults = UserDefaults.standard
        let myString = defaults.value(forKey: "savedId")
        let vehicle_id = myString
        
        let params = NSMutableDictionary()
        params["page"] = "1"
        params["vehicle_id"] = vehicle_id
        params["shop_id"] = shop_id
        
        Http.instance().json(api_estimateList, params, "POST", ai: true, popup: false, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyEstimateVC") as! MyEstimateVC
                    vc.shop_id = self.shop_id
                    kAppDelegate.shopId = self.shop_id
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "EstimateRequestVC") as! EstimateRequestVC
                    vc.shop_id = self.shop_id
                    kAppDelegate.shopId = self.shop_id
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    //MARK:- MAP VIEW SHOP DETAILS
    ////////////////////////////////////======***************
    /////==========================================...Neeleshwari
    
    let locationmanager = CLLocationManager()
    var myPolyLinePath: MKPolyline!
    
    func selectedShopLocation(dict:NSDictionary) {
        //let distance = string(dict, "distance")
        let shopLat = string(dict, "lat")
        let shopLong = string(dict, "long")
        let address = string(dict, "address").capFirstLetter()
        
        let float_lat = (shopLat as NSString).floatValue
        let float_long = (shopLong as NSString).floatValue
        
        let shopLocationCoords = CLLocationCoordinate2D(latitude: CLLocationDegrees(float_lat), longitude: CLLocationDegrees(float_long))
        
        map.delegate = self
        self.locationmanager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationmanager.delegate = self
            locationmanager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationmanager.startUpdatingLocation()
            
            let pick = kAppDelegate.curLocation()
            let currentLocation = CLLocationCoordinate2DMake((pick?.coordinate.latitude)!, (pick?.coordinate.longitude)!)
            //   let span = MKCoordinateSpanMake(0.5, 0.5)
            let span = MKCoordinateSpanMake(0.005, 0.005)
            //  let span = MKCoordinateSpanMake(1.5, 1.5)
            let region = MKCoordinateRegionMake(currentLocation, span)
            map.setRegion(region, animated: true)
            self.map.showAnnotations(self.map.annotations, animated: true)
            
            ////currentlocation
            let coordinate₀ = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
            let coordinate₁ = CLLocation(latitude: shopLocationCoords.latitude, longitude: shopLocationCoords.longitude)
            
            let distanceInMeters = coordinate₀.distance(from: coordinate₁) // result is in meters
            
            let strDistanceInMeters =  "(\(String(format: "%.2f", Double(distanceInMeters))))"
            
            print("strDistanceInMeters ----\(strDistanceInMeters)")
            /////////----
            
            let userLocationPoint = CLLocationCoordinate2DMake((pick?.coordinate.latitude)!, (pick?.coordinate.longitude)!)
            
            //===========
            let pointUser = ShopDetailsAnnotation(coordinate: shopLocationCoords)
            pointUser.shopName = "Current Location"
            pointUser.distance = "zero"
            pointUser.address = "N/A"
            pointUser.imageName = "Clocation.png"
            self.map.addAnnotation(pointUser)
            
            //===
            let pointShop = ShopDetailsAnnotation(coordinate: CLLocationCoordinate2D(latitude: (pick?.coordinate.latitude)! , longitude: (pick?.coordinate.longitude)!))
            pointShop.shopName = "\(address)"//string(dictResponse,
            pointShop.distance = "\(strDistanceInMeters) m"
            pointShop.address = "address" + address + "Indoreee"//string(dictResponse, "address")
            pointShop.imageName = "map_pin_.png"
            self.map.addAnnotation(pointShop)
            //===
            getPolylineRoute(myLat: userLocationPoint.latitude, myLng: userLocationPoint.longitude, shopLat: shopLocationCoords.latitude, shopLng: shopLocationCoords.longitude)
        }
    }
    
    //MARK:- MAP VIEW getPolylineRoute +++++++++++++++===========================
    func getPolylineRoute(myLat: Double, myLng: Double, shopLat: Double, shopLng: Double)  {
        
        let config = URLSessionConfiguration.default // Session Configuration
        let session = URLSession(configuration: config) // Load configuration into Session
        let origin = "\(myLat),\(myLng)"
        let destination = "\(shopLat),\(shopLng)"
        //  let directionURL = String(format: "https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=\(googleApiKey)", origin, destination)
        let directionURL = String(format: "https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=\(kAppDelegate.googleApiKey)", origin, destination)
        print("directionURL >>>>\(directionURL)")
        
        let url = URL(string: directionURL)!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]
                    {
                        //Implement your logic
                        //print("json>>>>>",json)
                        let routes = json["routes"] as! [NSDictionary]
                        if routes.count > 0 {
                            for route in routes {
                                if route["overview_polyline"] != nil {
                                    let overviewPolyline = route["overview_polyline"] as! NSDictionary
                                    if overviewPolyline["points"] != nil {
                                        let points = overviewPolyline["points"] as! String
                                        
                                        // self.mapPolyline = line
                                        self.myPolyLinePath = MyRoute().polyline(withEncodedString: points)
                                        DispatchQueue.main.async {
                                            let overlays = self.map.overlays
                                            self.map.removeOverlays(overlays)
                                            self.map.add(self.myPolyLinePath)
                                            //self.zoomToRegion()
                                        }
                                    }
                                }
                                //let  arr = route["legs"] as! NSArray
                                //print("arr>>>",arr)
                            }
                        }
                    }
                } catch {
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
    
    //MARK:- MAP VIEW delegates
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation
        {
            return nil
        }
        var annotationView = self.map.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if annotationView == nil{
            annotationView = ShopDetailsAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = false
        }else{
            annotationView?.annotation = annotation
        }
        let cpa = annotation as! ShopDetailsAnnotation
        
       /* let imgN =  imageWithImage(image: UIImage(named:cpa.imageName)!, scaledToSize: CGSize(width: 30.0, height: 30.0))*/
        
        annotationView?.image = UIImage(named:cpa.imageName) // imgN //UIImage(named: "map_pin2x.png")
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay is MKPolyline {
            let polyLineRender = MKPolylineRenderer(overlay: myPolyLinePath)
            polyLineRender.strokeColor = appColor.appPrimaryColor //UIColor.blue //appColor.appPrimaryColor //UIColor.blue //
            polyLineRender.lineWidth = 2.0
            
            return polyLineRender
        }
        
        return MKPolylineRenderer()
    }
    
    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView)
    {
        // 1
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        // 2
        let shopDetailsAnnotation = view.annotation as! ShopDetailsAnnotation
        let views = Bundle.main.loadNibNamed("CallOutView", owner: nil, options: nil)
        
        let calloutView = views?[0] as! ShopDetailsCalloutView
        
        calloutView.lblTitle.text = shopDetailsAnnotation.shopName
        calloutView.lblTitle.numberOfLines = 0
        calloutView.lblTitle.lineBreakMode = .byWordWrapping
        calloutView.lblTitle.sizeToFit()
        
        calloutView.lblDistance.frame.origin.y =  calloutView.lblTitle.frame.origin.y +  calloutView.lblTitle.frame.size.height + 2
        
        calloutView.lblDistance.text = shopDetailsAnnotation.distance
        
        if shopDetailsAnnotation.shopName == "Current location"{
            calloutView.lblDistance.frame.size.height = 0
              calloutView.lblTitle.textAlignment = .center
        } else {
              calloutView.lblTitle.textAlignment = .left
             calloutView.lblDistance.frame.size.height = 20
        }
        calloutView.lblTitle.frame.size.width = calloutView.lblDistance.frame.size.width
        calloutView.viewBG.frame.size.height =  calloutView.lblDistance.frame.origin.y +   calloutView.lblDistance.frame.size.height + 10//
        
        /*calloutView.viewBG.backgroundColor = UIColor.red
        calloutView.lblTitle.backgroundColor = UIColor.brown
        calloutView.lblDistance.backgroundColor = UIColor.cyan*/
        
        calloutView.lblShadow.frame.size.height =  calloutView.viewBG.frame.size.height + 4
        
        calloutView.imgDrop.frame.origin.y =   calloutView.viewBG.frame.origin.y  + calloutView.viewBG.frame.size.height - 1
        
        calloutView.frame.size.height =  calloutView.viewBG.frame.size.height +  calloutView.imgDrop.frame.size.height + 20
        
        calloutView.clipsToBounds = false
        calloutView.backgroundColor = UIColor.init(white: 1, alpha: 0)
        
        calloutView.viewBG.backgroundColor = UIColor.white
        calloutView.viewBG.layer.borderColor = UIColor.lightGray.cgColor
        calloutView.viewBG.layer.borderWidth = 0.5
        calloutView.viewBG.layer.masksToBounds = true
        calloutView.viewBG.layer.cornerRadius = 8
        calloutView.viewBG.clipsToBounds = true
        calloutView.lblShadow.layer.cornerRadius = 8
        calloutView.lblShadow.dropShadow(scale: true)
        
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
        view.addSubview(calloutView)
        map.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        
        if view.isKind(of: ShopDetailsAnnotationView.self)
        {
            for subview in view.subviews
            {
                subview.removeFromSuperview()
            }
        }
    }
    
    /////==========================================...Neeleshwari
    
    func displayAndDelegates(){
        
        tblViewNotifications.delegate = self
        tblViewNotifications.dataSource = self
        
         let imgAppointment = imageWithImage(image: UIImage(named: "calender-blue.png")!, scaledToSize: CGSize(width: 22.0, height: 20.0))
        btnAppointment.setImage(imgAppointment, for: .normal)
        
    }

    //Notification work ===========================Neeleshwari
    
    //MARK:- FUNCTION FOR POPVIEW
    func addPopupShowNotifications(viewMain: UIView, viewPopUP: UIView)  {
        viewMain.frame = self.view.frame
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.window?.addSubview(viewMain)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = (self as UIGestureRecognizerDelegate)
        viewMain.addGestureRecognizer(tap)
        
        viewPopUP.leftAnchor.constraint(equalTo: viewPopUP.leftAnchor, constant: 15).isActive = true
        viewPopUP.rightAnchor.constraint(equalTo: viewPopUP.rightAnchor, constant: -15).isActive = true
        
        viewMain.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        viewMain.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
     //   self.subView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
      //  self.subView.heightAnchor.constraint(equalToConstant: 568).isActive = true
        
        self.test(viewTest: viewPopUP)
    }
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewMain: UIView, viewPopUP: UIView) {
        let orignalT: CGAffineTransform = viewPopUP.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewMain.removeFromSuperview()
            viewPopUP.transform = orignalT
        })
        arrNotificationList = NSMutableArray()
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        //viewPopup.removeFromSuperview()
        removeSubViewWithAnimation(viewMain: ViewNotifications, viewPopUP: viewPopUPNotification)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if ViewNotifications.bounds.contains(touch.location(in: viewPopUPNotification)) {
            return false
        }
 
        return true
    }
    
    //MARK: Button Actions
    
    @IBAction func btnNotificationOK(_ sender: Any) {
        removeSubViewWithAnimation(viewMain: ViewNotifications, viewPopUP: viewPopUPNotification)
    }
    
    ////////////////////////////////////======***************
}//SACHIN :]

class RepairShopFeedbackCell : UITableViewCell {
    @IBOutlet var lblMyName: UILabel!
    @IBOutlet var lblRatingNo: UILabel!
    @IBOutlet var imgFevrate: UIImageView!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblMyFeedback: UILabel!
}

class RepairShopPromotionCell : UICollectionViewCell {
    @IBOutlet var shopImg: UIImageView!
}

class collectionAppoinmentCell : UICollectionViewCell {
    
    @IBOutlet var lblMyAppoinment: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblColor: Label!
    @IBOutlet var viewAppoinment: UIView!
}

/////==========================================...Neeleshwari

//MARK:- ShopDetailsCalloutView

class ShopDetailsCalloutView: UIView {
    
    @IBOutlet weak var lblShadow: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imgDrop: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    
}

//MARK:- ShopDetailsAnnotation

class ShopDetailsAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var shopName: String!
    var distance: String!
    var address: String!
    var imageName: String!
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}

//MARK:- ShopDetailsAnnotationView ShopDetails
class ShopDetailsAnnotationView: MKAnnotationView
{
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        if (hitView != nil)
        {
            self.superview?.bringSubview(toFront: self)
        }
        return hitView
    }
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = self.bounds
        var isInside: Bool = rect.contains(point)
        if(!isInside)
        {
            for view in self.subviews
            {
                isInside = view.frame.contains(point)
                if isInside
                {
                    break
                }
            }
        }
        return isInside
    }
}

extension UIView {
    
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 5
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

//MARK:- ShopDetailsAnnotationView ShopDetails end. ************
//MARK:-MenuVendorNoticationCell
class MenuVendorNoticationCell : UITableViewCell {

    @IBOutlet var lblNotification: UILabel!

}


