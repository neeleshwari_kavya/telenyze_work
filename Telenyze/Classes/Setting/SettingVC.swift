//
//  SettingVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 08/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class SettingVC: UIViewController {
    
    @IBOutlet var imgUpdateSwich: UIImageView!
    @IBOutlet var imgLocationSwich: UIImageView!
    
    //MARK:- VARIABLES
    var strSwich = ""
    var strSwich2 = ""
    let imgR_Off = UIImage(named: "swich_off.png")
    let imgR_On = UIImage(named: "swich_on.png")
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.menuNavigationButton() 
        setNav(className: "Settings")
        
        let strUpdate = string(kAppDelegate.dictUserInfo, "update")
        let strLocation = string(kAppDelegate.dictUserInfo, "location")
    
        if strUpdate == "on"{
            strSwich = "on"
            imgUpdateSwich.image = imgR_On
        } else {
            strSwich = "off"
            imgUpdateSwich.image = imgR_Off
        }
        
        if strLocation == "on" {
            imgLocationSwich.image = imgR_On
        } else {
            imgLocationSwich.image = imgR_Off
        }
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "MenuButton"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        self.title  = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func actionSwichBtnAll(_ sender: Any) {
        let button = sender as! UIButton
        let indexbtn = button.tag
        
        if indexbtn == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationSettingVC") as! NotificationSettingVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else if indexbtn == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AppConfigurationVC") as! AppConfigurationVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else if indexbtn == 3 {
            if strSwich == "on" {
                ws_Setting(para: "update", value: "off")
            } else {
                ws_Setting(para: "update", value: "on")
            }
        } else if indexbtn == 4 {
            if strSwich2 == "on" {
                ws_Setting(para: "location", value: "off")
            } else {
                ws_Setting(para: "location", value: "on")
            }
        } else if indexbtn == 5 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func ws_Setting(para :String , value : String) {
        
        let params = NSMutableDictionary()
        params[para] = value
     
        Http.instance().json(api_settings, params, "POST", ai: true, popup: true, prnt: true,tokenClass.getToken()) { (json, params, strJson)  in
           
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                     let result = (json as AnyObject).object(forKey: "result") as? NSDictionary
                    if self.strSwich == "on" {
                        self.strSwich = "off"
                        self.imgUpdateSwich.image = self.imgR_Off
                    } else {
                        self.strSwich = "on"
                        self.imgUpdateSwich.image = self.imgR_On
                    }
                    
                    if self.strSwich2 == "on" {
                        self.strSwich2 = "off"
                        self.imgLocationSwich.image = self.imgR_Off
                    } else {
                        self.strSwich2 = "on"
                        self.imgLocationSwich.image = self.imgR_On
                    }
                 
                    kAppDelegate.dictUserInfo.removeObject(forKey: "update")
                    kAppDelegate.dictUserInfo.removeObject(forKey: "location")
                    
                    kAppDelegate.dictUserInfo["update"] = string(result!, "update")
                    kAppDelegate.dictUserInfo["location"] = string(result!, "location")
                    
                    self.viewWillAppear(true)
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
}//Sachin :]
