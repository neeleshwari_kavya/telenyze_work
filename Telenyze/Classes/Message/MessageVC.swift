//
//  MessageVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 14/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class MessageVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tblView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.setNav(className: "Messages")
        self.menuNavigationButton()
        self.CustomSearchMessageBtn()
    }
    
    func CustomSearchMessageBtn() {
        let buttonSearch = UIButton.init(type: .custom)
        let imgN = imageWithImage(image: UIImage(named: "search.png")!, scaledToSize: CGSize(width: 15.0, height: 15.0))
        buttonSearch.setImage(imgN, for: UIControlState.normal)
        buttonSearch.addTarget(self, action:#selector(btnSearchMessageAction), for:.touchUpInside)
        buttonSearch.frame = CGRect.init(x: 0, y: 0, width: 15, height: 15)
        let barButton = UIBarButtonItem.init(customView: buttonSearch)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func btnSearchMessageAction() {
        print("button press search message btn")
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "MenuButton"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- TABLEVIEW_DELEGATE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        return cell
    }
    
}//Sachin :]

class MessageCell : UITableViewCell {
    
    @IBOutlet var lblMsgTitle: UILabel!
    @IBOutlet var lblMsgDes: UILabel!
    @IBOutlet var lblNotification: UILabel!
    @IBOutlet var lblTime: UILabel!
}

