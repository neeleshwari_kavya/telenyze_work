//
//  RepairShopVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 12/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import HarishFrameworkSwift4

class RepairShopVC: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, FloatRatingViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var ShopMap: MKMapView!
    @IBOutlet var lblShopCount: UILabel!
    @IBOutlet var viewMap: UIView!
    @IBOutlet var lblNoRecordFound: UILabel!
    
    //MARK:- VARIABLES
    var barrating:Float = 0.0
    var arrRepairShopList = NSMutableArray()
    let locationmanager = CLLocationManager()
    var intShopCount = Int()
    var intShopCountList = Int()
    var pageNum = Int()
    var boolWsPage = true
   
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.BackNavigationButton()
        self.refreshRightNavigationButton()
        //self.automaticallyAdjustsScrollViewInsets = false
        self.displayAndDelegates()
        
        if kAppDelegate.menuPush == "menubtn" {
            setNav(className: "My Shops")
        } else {
            setNav(className: "Repair Shops")
        }
    }
    
    func displayAndDelegates() {
        ShopMap.delegate = self
        self.lblNoRecordFound.isHidden = true
        self.locationmanager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationmanager.delegate = self
            locationmanager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationmanager.startUpdatingLocation()
            
            pageNum = 1
            self.ws_ShopList()
            
            let pick = kAppDelegate.curLocation()
            
            if pick != nil {
                if pick?.coordinate.latitude != 0.0 && pick?.coordinate.longitude != 0.0 {
                    let currentLocation = CLLocationCoordinate2DMake((pick?.coordinate.latitude)!, (pick?.coordinate.longitude)!)
                    let span = MKCoordinateSpanMake(0.008, 0.008)
                    let region = MKCoordinateRegionMake(currentLocation, span)
                    ShopMap.setRegion(region, animated: true)
                    
                    let annotationCurrentLocation = ShopLocationAnnotation(title: "Current location", coordinate: CLLocationCoordinate2D(latitude: currentLocation.latitude, longitude:currentLocation.longitude))
                    
                    annotationCurrentLocation.imageName = "Clocation.png"
                    self.ShopMap.addAnnotation(annotationCurrentLocation)
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        locationmanager.stopUpdatingLocation()
    }
    
    //MARK:- MAP VIEW delegates==========================================...Neeleshwari
    ////////////////////////////////////======***************
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation
        {
            return nil
        }
        let identifier = "shopAnnotation"
        var annotationView = self.ShopMap.dequeueReusableAnnotationView(withIdentifier: identifier)
        if annotationView == nil{
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
        }else{
            annotationView?.annotation = annotation
        }
        
        let myAnnotation = annotation as! ShopLocationAnnotation
        let mapPinImage =  UIImage(named:myAnnotation.imageName)
        var abc = "neeleshwari"
        // let imgN =  imageWithImage(image: mapPinImage!, scaledToSize: CGSize(width: 30.0, height: 30.0))
        // mapPinImage = imgN
        if myAnnotation.imageName == "map_pin1.png" {
            intShopCount += 1
            abc = "\(intShopCount)"
        }else{
            abc = ""
        }
        
        annotationView?.image = mapPinImage
        
        let myFrame = annotationView?.frame.size
        let myX = ((myFrame?.width)! - 2)/2
        let newMyX = myX + 2 - (getTextWidth(str: abc)/2)
        
        let newImage = textToImage(drawText: abc as NSString, inImage: mapPinImage!, atPoint: CGPoint(x: newMyX, y: 10))
        annotationView!.image = newImage
        
        return annotationView
    }
    
    /////==========================================...Neeleshwari
    ////////////////////////////////////======***************
    
    func getTextWidth(str: String) -> CGFloat {
        let label = UILabel()
        label.text = str
        label.sizeToFit()
        return label.frame.size.width
    }
    
    func textToImage(drawText text: NSString, inImage image: UIImage, atPoint point: CGPoint) -> UIImage {
        let textColor = UIColor.white
        let textFont = UIFont.boldSystemFont(ofSize: 14.0)//UIFont(name: "Helvetica Bold", size: 12)!
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor,
            ] as [String : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let rect = CGRect(origin: point, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func BackNavigationButton() {
        
        if kAppDelegate.menuPush == "menubtn" {
            let button1 = UIBarButtonItem(image: UIImage(named: "MenuButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
            self.navigationItem.leftBarButtonItem = button1
            
        } else {
            let button1 = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
            self.navigationItem.leftBarButtonItem = button1
        }
    }
    
    func actionBackButton() {
        if kAppDelegate.menuPush == "menubtn" {
            self.sideMenuViewController.presentLeftMenuViewController()
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:Navigation Buttons
    func refreshRightNavigationButton() {
        let button1 = UIBarButtonItem(image: UIImage(named: "refresh.png"), style: .plain, target: self, action: #selector(actionRefreshButton)) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem = button1
    }
    
    func actionRefreshButton()  {
        intShopCount = 0
        intShopCountList = 0
        arrRepairShopList = NSMutableArray()
        self.ws_ShopList()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- TABLEVIEW_DELEGATE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRepairShopList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepairShopCell", for: indexPath) as! RepairShopCell
        
        let dict = arrRepairShopList.object(at: indexPath.row) as! NSDictionary

        if let arrNotifications = dict.object(forKey: "shop_notification") as? NSArray {
            arrNotificationList = (arrNotifications.mutableCopy() as! NSArray) as! NSMutableArray
        }
        
        if arrNotificationList.count != 0{
            cell.viewBg.backgroundColor = appColor.redColor
        }else{
            cell.viewBg.backgroundColor = appColor.whiteColor
        }

        cell.lblName.text! = string(dict, "name").capFirstLetter()
        cell.lblAddress.text! = string(dict, "address").capFirstLetter()
        
        cell.lblStatus.text! = string(dict, "calculated_status")
        cell.lblStatus.adjustsFontSizeToFitWidth = true
        
        if string(dict, "calculated_status") == "Open now" {
            cell.lblStatus.textColor = appColor.greenColor
            
        } else if string(dict, "calculated_status") == "Active" {
            cell.lblStatus.textColor = appColor.greenColor
            
        } else {
            cell.lblStatus.textColor = appColor.redColor
        }
        
        let myDistance = string(dict, "distance")
        if myDistance.count > 3 {
            let km = Float(myDistance)! * 1000.0
            cell.lblMin.text = String(km) + "km"
        } else {
            cell.lblMin.text = string(dict, "distance") + "m"
        }
        
        cell.lblMin.adjustsFontSizeToFitWidth = true
        
        cell.lblRatingReview.text = string(dict, "rating") + "(\(string(dict, "review") + " Reviews"))"
        
        let ratingFloat = number(dict, "rating").floatValue
        cell.RatingView.delegate = self
        cell.RatingView.contentMode = UIViewContentMode.scaleAspectFit
        cell.RatingView.maxRating = 5
        cell.RatingView.minRating = 0
        cell.RatingView.rating = ratingFloat
        cell.RatingView.editable = false
        cell.RatingView.halfRatings = true
        cell.imgFav.image = UIImage(named: "imgHeart.png")
        
        let fav_status = string(dict, "fav_status")
        cell.imgFav.center =  cell.lblShopNo.center
        
        if fav_status == "1"{
            cell.lblShopNo.text = ""
            cell.lblShopNo.backgroundColor = appColor.appPrimaryColor
            cell.imgFav.isHidden = false
        }else{
            cell.lblShopNo.text = string(dict, "shop_number")
            cell.lblShopNo.backgroundColor = appColor.profileBGColor
            cell.imgFav.isHidden = true
        }

        if indexPath.row == (arrRepairShopList.count - 1) {
            if !boolWsPage {
                boolWsPage = true
                if self.arrRepairShopList.count % 50 == 0 {
                    pageNum += 1
                    self.ws_ShopList()
                }
            }
        }
        
        return cell
    }
    
    //MARK:- NOtification work========================Neeleshwari =================================
    var arrNotificationList = NSMutableArray()
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if let dict = arrRepairShopList.object(at: indexPath.row) as? NSDictionary {
            
            print("shop name ->>>\(string(dict, "name"))")
            print("dict ->>>\(dict)")
            print("arrNotificationList---->>>>>>\(arrNotificationList)")

            if let arrNotifications = dict.object(forKey: "shop_notification") as? NSArray {
                arrNotificationList = (arrNotifications.mutableCopy() as! NSArray) as! NSMutableArray
            }
            print("arrNotificationList---->>>>>>\(arrNotificationList)")
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RepairShopDetailVC") as! RepairShopDetailVC
            kAppDelegate.dictShopNeeleshwari = dict
            vc.shopName = string(dict, "name").capFirstLetter()
            vc.shop_id = string(dict, "id")
            vc.lat = string(dict, "lat")
            vc.long = string(dict, "long")
            vc.arrNotificationList = arrNotificationList
            self.navigationController?.pushViewController(vc, animated: true)
        }
 
    }
    // NOtification work========================Neeleshwari=================================
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        print("Sachin")
    }
    
    //MARK:- ws_ShopList =================================
    
    func ws_ShopList() {
        intShopCountList = 0
        
        let params = NSMutableDictionary()
        params["page"] = pageNum
        // params["fav"] = ""
        
        if kAppDelegate.menuPush == "menubtn" {
            params["fav"] = "1"
        } else {
            params["fav"] = ""
        }
      // print("params>>>\(params)")
        
        let pick = kAppDelegate.curLocation()
        
        if pick != nil {
            if pick?.coordinate.latitude != 0.0 && pick?.coordinate.longitude != 0.0 {
                params["lat"] = (pick?.coordinate.latitude)!
                params["long"] = (pick?.coordinate.longitude)!
            }
        }
        
        Http.instance().json(api_shopList, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            print("json>>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // self.arrRepairShopList = NSMutableArray()
                    if let arr = json?.object(forKey: "result") as? NSArray {
                        
                        if self.boolWsPage{
                            self.arrRepairShopList.addObjects(from: arr as [AnyObject])
                            self.boolWsPage = false
                        }else{
                            self.arrRepairShopList = (arr.mutableCopy() as? NSMutableArray)!
                        }
                        
                        //self.tblView.reloadData()
                        print("self.arrRepairShopList>>>\(self.arrRepairShopList)")
                        self.lblShopCount.text = "Repair Shops " + "(\(self.arrRepairShopList.count))"
                        
                        for index in 0..<self.arrRepairShopList.count {
                            let dict = ((self.arrRepairShopList.object(at: index) as! NSDictionary).mutableCopy() as? NSMutableDictionary)!
                            
                            let Sachinlat = number(dict, "lat").doubleValue
                            let Sachinlong = number(dict, "long").doubleValue
                            
                            let fav_status = string(dict, "fav_status")
                            
                            let annotation = ShopLocationAnnotation(title: "", coordinate: CLLocationCoordinate2D(latitude: Sachinlat, longitude:Sachinlong))
                            
                            if fav_status == "1"{
                                annotation.imageName = "fav_location_icon.png"
                                dict.setObject("", forKey: "shop_number" as NSCopying)
                            }else{
                                annotation.imageName = "map_pin1.png"
                                self.intShopCountList += 1
                                dict.setObject("\(self.intShopCountList)", forKey: "shop_number" as NSCopying)
                            }
                            self.arrRepairShopList.replaceObject(at: index, with: dict)
                            self.ShopMap.addAnnotation(annotation)
                            self.ShopMap.mapType = MKMapType.standard
                        }
                           // self.tblView.reloadData()
                            self.performMyTask()
                        
                        if self.arrRepairShopList.count == 0{
                            self.lblNoRecordFound.isHidden = false
                        }else{
                            self.lblNoRecordFound.isHidden = true
                        }
                        
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                    if self.arrRepairShopList.count == 0{
                        self.lblNoRecordFound.isHidden = false
                    }else{
                        self.lblNoRecordFound.isHidden = true
                    }
                }
            }
        }
    }
    
    /*
     if let notifications = (json as AnyObject).object(forKey: "shop_notification") as? NSArray {
     print("notifications>>>\(notifications)")
     self.arrNotifications = (notifications.mutableCopy() as? NSMutableArray)!
     print("self.arrNotifications>>>\(self.arrNotifications)")
     self.tblViewNotifications.reloadData()
     }
 */
    //MARK:- Other Fuctions
    func performMyTask() {

        for i in 0..<arrRepairShopList.count {
            
            if let dict = arrRepairShopList.object(at: i) as? NSDictionary {
                
                let newDictRahul = dict.mutableCopy() as! NSMutableDictionary
                
                print("name -\(string(dict, "name"))-")
                
                var boolHoliday = false
                
                if let arrShopHoliday = dict.object(forKey: "shop_holiday") as? NSArray {
                    
                    for r in 0..<arrShopHoliday.count {
                        
                        if let dictHoliday = arrShopHoliday.object(at: r) as? NSDictionary {
                            
                            let fromDate = string(dictHoliday, "from_date").getDate("yyyy-MM-dd HH:mm:ss")
                            let toDate = string(dictHoliday, "to_date").getDate("yyyy-MM-dd HH:mm:ss")
                            
                            let currentDate = Date().getLocalDate()
                            
                            if fromDate <= currentDate && toDate >= currentDate { //its holiday
                                boolHoliday = true
                                break
                            }
                        }
                    }
                }
                
                if boolHoliday {
                    newDictRahul.setValue("Close now", forKey: "calculated_status")
                    print("Close now")
                    
                } else { //not a holiday.
                    
                    if let arrShopWeeklyTime = dict.object(forKey: "shop_weekly_timing") as? NSArray {
                        
                        if arrShopWeeklyTime.count > 0 {
                            
                            for j in 0..<arrShopWeeklyTime.count {
                                
                                if let dictWeek = arrShopWeeklyTime.object(at: j) as? NSDictionary {
                                    
                                    let day = number(dictWeek, "day").intValue
                                    
                                    if day == getDayOfWeek() - 1 {
                                        
                                        let openStatus = string(dictWeek, "status")
                                        
                                        if openStatus == "open" {
                                            
                                            let openTime =  getExactTime(date: string(dictWeek, "open").getDate("HH:mm:ss"))
                                            let closeTime =  getExactTime(date: string(dictWeek, "close").getDate("HH:mm:ss"))
                                            
                                            let currentTime = Date().getLocalDate()
                                            
                                            print("openTime -\(openTime)-")
                                            print("openTime -\(closeTime)-")
                                            print("currentTime -\(currentTime)-")
                                            
                                            if openTime <= currentTime && closeTime >= currentTime { // will be open.
                                                
                                                let obj = currentTime.getDifference(fromDate: closeTime)
                                                
                                                if obj.hour == 0 { //less than one hour remain to close.
                                                    
                                                    newDictRahul.setValue("close in \(obj.minute) minutes", forKey: "calculated_status")
                                                    
                                                    print("close in \(obj.minute) minutes-")
                                                } else {
                                                    newDictRahul.setValue("Open now", forKey: "calculated_status")
                                                    print("open now")
                                                }
                                            } else { //closed
                                                newDictRahul.setValue("Closed till \(string(dictWeek, "close").converDate("HH:mm:ss", "hh:mm a"))", forKey: "calculated_status")
                                                print("Closed till \(string(dictWeek, "close").converDate("HH:mm:ss", "hh:mm a"))")
                                                //closed.
                                            }
                                        } else { //closed
                                            newDictRahul.setValue("Close Now", forKey: "calculated_status")
                                            
                                            print("Close Now")
                                        }
                                    }
                                }
                            }
                        } else {
                            newDictRahul.setValue("Active", forKey: "calculated_status")
                            print("Active")
                        }
                    }
                }
                self.arrRepairShopList.replaceObject(at: i, with: newDictRahul)
            }
        }

        tblView.reloadData()
    }
    
        // NOtification work========================Neeleshwari=================================
   /*
    var intNotificationsCount = Int()
    
    func showNotifications() {
        
        for i in 0..<arrRepairShopList.count {
            
            if let dict = arrRepairShopList.object(at: i) as? NSDictionary {
                 print("dict>>>\(dict)")
                
                if let arrShopNotifications = dict.object(forKey: "shop_notification") as? NSArray {
                     print("intNotificationsCount>>>\(intNotificationsCount)")
                    
                    for _ in 0..<arrShopNotifications.count {
                      intNotificationsCount += intNotificationsCount
                        print("intNotificationsCount>>>\(intNotificationsCount)")
                    }
                }
            }
        }

    }
    *///Notificationwork=================Neeleshwari==================
    
    
    func getDayOfWeek() -> Int { //1 Sunday, 2 Monday, ----- 7 Saturday
        let calendar = NSCalendar.current
        let weekDay = calendar.component(.weekday, from: Date())
        return weekDay
    }
    
    func timeForCurrentDate(hour: Int, minute: Int, second: Int) -> Date {
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "GMT+0:00")!
        let now = Date().getLocalDate()
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now)
        
        // Change the time to 9:30:00 in your locale
        components.hour = hour
        components.minute = minute
        components.second = second
        
        let date = calendar.date(from: components)!
        
        return date
    }
    
    func getHourMinuteSeconds(fromDate: Date) -> (hour: Int, minute: Int, seconds: Int) {
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "GMT+0:00")!
        
        let components = calendar.dateComponents([.hour, .minute, .second], from: fromDate)
        
        return (components.hour!, components.minute!, components.second!)
    }
    
    
    func getExactTime(date: Date) -> Date {
        
        let obj = getHourMinuteSeconds(fromDate: date)
        
        return timeForCurrentDate(hour: obj.hour, minute: obj.minute, second: obj.seconds)
    }


    
}//class end.
//Open Time function//
//*******************************************************

extension String {
    func getDate(_ formate:String) -> Date {
        if self.count == 0 {
            return Date()
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        return dateFormatter.date(from: self)!
    }
}


extension Date {
    
    func getDifference(fromDate: Date) -> (hour: Int, minute: Int) {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: fromDate, to: self);
        
        let seconds = "\(difference.second ?? 0)s"
        let minutes = "\(difference.minute ?? 0)m" + " " + seconds
        let hours = "\(difference.hour ?? 0)h" + " " + minutes
        // let days = "\(difference.day ?? 0)d" + " " + hours
        
        return (difference.hour!, difference.minute!)
        
        //
        //        if let day = difference.day, day          > 0 { return days }
        //        if let hour = difference.hour, hour       > 0 { return hours }
        //        if let minute = difference.minute, minute > 0 { return minutes }
        //        if let second = difference.second, second > 0 { return seconds }
        //        return ""
    }
}


//*******************************************************

class RepairShopCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var RatingView: FloatRatingView!
    @IBOutlet var lblMin: UILabel!
    @IBOutlet var lblRatingReview: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var viewBg: UIView!
    @IBOutlet var lblShopNo: Label!
    @IBOutlet var imgFav: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class CustomMapAnnotation : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var id: String?
    var title: String?
    var subtitle: String?
    var dict: NSDictionary?
    var strImageName: String?
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String, dict:NSDictionary, id: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        self.dict = dict
        self.id = id
    }
    
    init(coordinate: CLLocationCoordinate2D, title: String, strImg: String?) {
        self.coordinate = coordinate
        self.title = title
        self.strImageName = strImg
    }
}

//mark:- function for UK time convert to local time change...
extension Date {
    
    func getLocalDate() -> Date {
        
        let timeZoneLocal = NSTimeZone.local as TimeZone!
        let newDate = Date(timeInterval: TimeInterval((timeZoneLocal?.secondsFromGMT(for: self))!), since: self)
        return newDate
    }
}

/////==========================================...Neeleshwari
////////////////////////////////////======***************
class ShopLocationAnnotation: NSObject, MKAnnotation {
    
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var imageName: String!
    
    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
}
/////==========================================...Neeleshwari
////////////////////////////////////======***************

/*
 let text = "This is the text....."
 let textShare = [ text ]
 let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
 activityViewController.popoverPresentationController?.sourceView = self.view
 self.present(activityViewController, animated: true, completion: nil)
 }
 */
