//
//  LeftMenuView.swift
//  ResideMenuDemo
//
//  Created by Rahul Khatri on 24/07/17.
//  Copyright © 2017 KavyaSoftech. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class MenuVendor: UIViewController, UITableViewDelegate, UITableViewDataSource ,UIGestureRecognizerDelegate {
    
    @IBOutlet var lblUserId: UILabel!
    @IBOutlet var lblProfileInfo: UILabel!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var viewButton: UIView!
    @IBOutlet var btnLogout: Button!
    @IBOutlet var viewParking: UIView!
    @IBOutlet var viewTop: UIView!
    @IBOutlet var imgArrow: UIImageView!
    @IBOutlet var viewBgTop: UIView!
    //logout
    
    //MARK:- VARIABLES
    var menuViewWidth: CGFloat!
    var arrMenu = NSMutableArray()
    var selectindex:Int = 0
    let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuItems()
        let screenWidth = UIScreen.main.bounds.width
        menuViewWidth = (screenWidth / 2) + 72
        viewBgTop.frame.size.width = menuViewWidth + 10
        viewTop.frame.size.width = viewBgTop.frame.size.width
        viewParking.frame.origin.x = viewTop.frame.size.width - viewParking.frame.size.width - 8
        imgArrow.frame.origin.x = viewTop.frame.size.width - imgArrow.frame.size.width - 18
        viewButton.frame.size.width = menuViewWidth
        btnLogout.frame.size.width = viewButton.frame.size.width - 10
        btnLogout.frame.origin.x = 8
        lblUserId.frame.size.width = viewTop.frame.size.width - 100
        lblProfileInfo.frame.size.width = viewTop.frame.size.width - 100
    }
    
    //MARK:- Fuctions
    
    func menuItems() {
        arrMenu = NSMutableArray(array: [["title": "Dashboard"],["title": "My Appointments"],["title": "Messages"],["title": "My Cars"],["title": "My Shop"],["title": "Settings"],["title": "Help"]])
    }
    
    //MARK:- Button Actions
    //Mark:-SIGN OUT Action====================================================================================
    
    @IBAction func actionSignOut(_ sender: Any) {

        let attributedString = NSAttributedString(string: "Confirmation", attributes: [
            NSFontAttributeName : UIFont.systemFont(ofSize: 15), //your font here
            NSForegroundColorAttributeName : appColor.blueColor//UIColor.blue
            ])
        let alert = UIAlertController(title: "", message: "Are you sure, you Want to logout?\n",  preferredStyle: .alert)
        
        alert.setValue(attributedString, forKey: "attributedTitle")
        
     //   let alert = UIAlertController (title: "Confirmation", message: "Are you sure, you Want to logout?\n\n" , preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            
            var boolPoped = false
            
            for vc in (self.navigationController?.viewControllers)! {
                
                if vc is SignInVC {
                    boolPoped = true
                    self.navigationController?.popToViewController(vc, animated: false)
                }
            }
            
            if !boolPoped {
                
                self.navigationController?.viewControllers = []
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                self.kAppDelegate.arrGLVehicleList = NSMutableArray()
                self.kAppDelegate.dictUserInfo = NSMutableDictionary()
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
 
     //SIGN OUT Action====================================================================================
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func actionHelpMe(_ sender: Any) {
        let params = NSMutableDictionary()
        let pick = kAppDelegate.curLocation()
        
        if pick != nil {
            if pick?.coordinate.latitude != 0.0 && pick?.coordinate.longitude != 0.0 {
                params["lat"] = (pick?.coordinate.latitude)!
                params["long"] = (pick?.coordinate.longitude)!
            }
        }
        
        print("params-\(params)-")
        
        Http.instance().json(api_SOS_Help, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                    self.sideMenuViewController.hideViewController()
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                    self.sideMenuViewController.hideViewController()
                }
            }
        }
    }
    
    @IBAction func actionParkingLocation(_ sender: Any) {
        let alert = UIAlertController (title: "Confirmation", message: "Have you parked your vehicle in this location?" , preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            self.ws_parkingLocation()
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .default, handler: { action in
            self.sideMenuViewController.hideViewController()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func actionProfileInfo(_ sender: Any) {
        self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "ProfileNav"), animated: true)
        self.sideMenuViewController.hideViewController()
    }
    
    //MARK:- ws_parkingLocation
    func ws_parkingLocation() {
        
        let defaults = UserDefaults.standard
        let vehicleId = defaults.value(forKey: "savedId")
        
        let params = NSMutableDictionary()
        let pick = kAppDelegate.curLocation()
        
        if pick != nil {
            if pick?.coordinate.latitude != 0.0 && pick?.coordinate.longitude != 0.0 {
                params["lat"] = (pick?.coordinate.latitude)!
                params["long"] = (pick?.coordinate.longitude)!
            }
        }
        
        params["vehicle_id"]  = vehicleId
        print("params-\(params)-")
        
        Http.instance().json(api_parkingLocation, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                    self.sideMenuViewController.hideViewController()
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
    }
    
    
    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCellVendor") as! MenuCellVendor
        cell.lblNotiMenu.frame.origin.x = menuViewWidth - cell.lblNotiMenu.frame.size.width - 8
        cell.imgBackArrow.frame.origin.x = menuViewWidth - cell.imgBackArrow.frame.size.width - 8
        
        let dict = arrMenu.object(at: indexPath.row) as! NSDictionary
        let title = string(dict, "title")
        cell.lblMenu.text = string(dict, "title")
        
        if title == "Dashboard" {
            cell.imgMenu.image = UIImage(named: "dashboard.png")
            
        } else if title == "My Appointments" {
            cell.imgMenu.image = UIImage(named: "appointments.png")
            
        } else if title == "Messages" {
            cell.imgMenu.image = UIImage(named: "message.png")
            
        } else if title == "My Cars" {
            cell.imgMenu.image = UIImage(named: "mycars.png")
            
        } else if title == "My Shop" {
            cell.imgMenu.image = UIImage(named: "shop.png")
            
        } else if title == "Settings" {
            cell.imgMenu.image = UIImage(named: "settings.png")
            
        } else if title == "Help" {
            cell.imgMenu.image = UIImage(named: "support.png")
        }
        
        if title == "Settings" {
            cell.imgBackArrow.isHidden = false
            
        } else {
            cell.imgBackArrow.isHidden = true
        }
        
        if title == "Messages" {
            cell.lblNotiMenu.isHidden = false
            
            /*
             if kAppDelegate.strNotificationCount == "0" {
             cell1.lblNotification.isHidden = true
             } else {
             cell1.lblNotification.isHidden = false //neeleshwari have check it ...1
             }
             */
        } else {
            cell.lblNotiMenu.isHidden = true
        }
        
        if indexPath.row == selectindex {
            cell.backgroundColor = appColor.appPrimaryColor
            cell.lblMenu.textColor = UIColor.white
            
            if title == "Dashboard" {
                cell.imgMenu.image = UIImage(named: "dashboard_selected.png")
                
            } else if title == "My Appointments" {
                cell.imgMenu.image = UIImage(named: "appointments_selected.png")
                
            } else if title == "Messages" {
                cell.imgMenu.image = UIImage(named: "message_selected.png")
                
            } else if title == "My Cars" {
                cell.imgMenu.image = UIImage(named: "mycars_selected.png")
                
            } else if title == "My Shop" {
                cell.imgMenu.image = UIImage(named: "shop_selected.png")
                
            } else if title == "Settings" {
                cell.imgMenu.image = UIImage(named: "settings_selected.png")
                
            } else if title == "Help" {
                cell.imgMenu.image = UIImage(named: "support_selected.png")
            }
            
        } else {
            cell.lblMenu.textColor = UIColor.black
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblView{
            let dict = arrMenu.object(at: indexPath.row) as! NSDictionary
            let title = string(dict, "title")
            
            arrMenu = NSMutableArray(array: [["title": "Dashboard"],["title": "My Appointments"],["title": "Messages"],["title": "My Cars"],["title": "My Shop"],["title": "Settings"],["title": "Help"]])
            
            if title == "Dashboard" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "DashboardNav"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "My Appointments" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "MyAppointmentsNav"), animated: true)
                
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Messages" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                //Http.alert("", "Under Working")
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "MessageNav"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "My Cars" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "MyCarsNav"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "My Shop" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "RepairShopNav"), animated: true)
                kAppDelegate.menuPush = "menubtn"
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Settings" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "SettingNav"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Help" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "HelpNav"), animated: true)
                self.sideMenuViewController.hideViewController()
            }
        }else{
            
        }
    }
    
    //MARK:-Notification work ===========================Neeleshwari****************************************
    
    let windowScreen = UIApplication.shared.keyWindow!
    
    override func viewWillAppear(_ animated: Bool) {
        
        let userName = string(kAppDelegate.dictUserInfo, "username")
        lblUserId.text! = userName
        /////====notification ============neeleshwari
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "TelenyzeGetNewNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(methodRemoteNotification), name: NSNotification.Name(rawValue: "TelenyzeGetNewNotification"), object: nil)
        
        //  self.wsTestNotification()
    }
    
    func methodRemoteNotification(notification:Notification){
        // What message comes here ?
        
        print("remoteMessage.appData : ", notification.userInfo)
        
        var userInfo = notification.userInfo! as! [String:AnyObject]
        print("Neeleshwari received NOTIFICATION check actual Data ------------- \(userInfo)")
       
//         kAppDelegate.dicc = (userInfo as? NSDictionary)!
//
//         dd = kAppDelegate.dictCatchNotifications
//          print(dd)
//
        if let dictNotifications : NSDictionary = (userInfo as? NSDictionary)!{
            print("dictNotifications ----------->>>\(dictNotifications)")
      
          kAppDelegate.dictCatchNotifications = (dictNotifications.mutableCopy() as! NSDictionary) as! NSMutableDictionary
        }
        
        print("dictCatchNotifications ----------->>>\(kAppDelegate.dictCatchNotifications)")
        
        if string(userInfo as NSDictionary, "type") == "QUOTED_ON_ESTIMATE"{
            print("dictCatchNotifications ----------->>>QUOTED_ON_ESTIMATE")
        }else  if string(userInfo as NSDictionary, "type") == "ACCEPT_APPOINTMENT"{
            print("dictCatchNotifications ----------->>>ACCEPT_APPOINTMENT")
        }
        
    }
    
    //MARK:- ws Test notificaiton
    
    func wsTestNotification(){
        
        let params = NSMutableDictionary()
        
        params["type"] = "QUOTED_ON_ESTIMATE"
        params["userId"] = "4"
        params["notification_id"] = "f229XWOcI1g:APA91bEWHkOIzCAf3NpT3Zut42jWHguV67mQv-Ut8ZtrbC7sFHz1IVkMyht7cQ68-4znYlOxrIEG7FbUB2EjWJTf5QOwtR39aBP5h1nmjaO_tnFu-MwlVBvWPeuuAg2hXE0-KuV6Q5lp"
        params["user_type"] = "Customer"
        
        params["vehicle_id"] = "2"
        params["estimate_id"] = "1"
        
        Http.instance().json(api_test_notification, params, "POST", ai: true, popup: true, prnt: true) { (json, params, strJson)  in
            
            print("json>>>\(json)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // Http.alert("", string(json! , "message"))
                    
                    
                    /*
                     let viewControllers = self.navigationController!.viewControllers as [UIViewController]
                     for aViewController:UIViewController in viewControllers {
                     if aViewController.isKind(of: SignInVC.self) {
                     _ = self.navigationController?.popToViewController(aViewController, animated: true)
                     }
                     }
                     */
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    //MARK:-Notification work ===========================Neeleshwari****************************************
} //class ends here....
















