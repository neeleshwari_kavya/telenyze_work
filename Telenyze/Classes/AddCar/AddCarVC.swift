//
//  AddCarVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 02/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class AddCarVC: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var tfVinNumber: SkyFloatingLabelTextField!
    @IBOutlet var collection: UICollectionView!
    @IBOutlet var tfOBD: SkyFloatingLabelTextField!
    @IBOutlet var tfSerialNo: SkyFloatingLabelTextField!
    @IBOutlet var imgCar: UIImageView!
    
    @IBOutlet var lblBrandName: UILabel!
    @IBOutlet var lblModelName: UILabel!
    @IBOutlet var lblYearName: UILabel!
    
    //MARK:- POPUP_VIEW
    @IBOutlet var lblPopupTitle: UILabel!
    @IBOutlet var viewPopup: UIView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var tfSearch: UITextField!
    @IBOutlet var lblNoRecord: UILabel!
    @IBOutlet var btnClearString: UIButton!
    
    //MARK:- VARIABLES
    var strLocal = ""
    var selectBrand = ""
    var selectindex:Int = -1
    var lblGeneric: UILabel!
    var brandId = String()
    var modelId = String()
    var colorId = String()
    var picker:UIImagePickerController? = UIImagePickerController()
    
    //MARK:- ARRAY_LIST
    var arrColorList = NSMutableArray()
    var arrBrandList = NSMutableArray()
    var arrModelList = NSMutableArray()
    var arrYearList = NSMutableArray()
    var arrTable = NSMutableArray()
    var arrYear = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfOBD.delegate = self
        tfSearch.delegate = self
        tfSerialNo.delegate = self
        tfVinNumber.delegate = self
        lblNoRecord.isHidden = true
        btnClearString.isHidden = true
        self.registerForKeyboardNotifications()
        self.ws_colorList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuViewController.panGestureEnabled = false
        tfVinNumber.placeHolderColor = UIColor.white
        tfOBD.placeHolderColor = UIColor.white
        tfSerialNo.placeHolderColor = UIColor.white
        self.navigationItem.hidesBackButton = true
        scroll.contentInset = UIEdgeInsets.zero
        
        let date = Date()
        let calendar = Calendar.current
        var year = calendar.component(.year , from: date)
        
        year = year + 1
        for _ in 0 ..< 20 {
            let dict = NSMutableDictionary()
            year = year - 1
            dict.setValue(year, forKey: "title")
            arrYear.add(dict)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
        sideMenuViewController.panGestureEnabled = true
    }
    
    @IBAction func actionCamera(_ sender: Any) {
        self.view.endEditing(true)
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func actionChooseBrand(_ sender: Any) {
        self.view.endEditing(true)
        strLocal = "brand"
        lblNoRecord.isHidden = true
        lblPopupTitle.text = "Search Brand"
        lblGeneric = lblBrandName
        self.ws_BrandList()
        addDoneButtonOnKeyboard()
    }
    
    @IBAction func actionChooseModel(_ sender: Any) {
        self.view.endEditing(true)
        strLocal = "model"
        lblNoRecord.isHidden = true
        lblPopupTitle.text = "Search Model"
        lblGeneric = lblModelName
        
        if selectBrand == "yes" {
            self.ws_ModelList()
        } else {
            Http.alert("", "Please choose brand")
        }

        addDoneButtonOnKeyboard()
    }
    
    @IBAction func actionYear(_ sender: Any) {
        self.view.endEditing(true)
        tfSearch.keyboardType = UIKeyboardType.numberPad
        strLocal = "year"
        lblNoRecord.isHidden = true
        lblPopupTitle.text = "Search Year"
        lblGeneric = lblYearName
        self.static_YearList()
        addDoneButtonOnKeyboard()
    }
    
    @IBAction func actionSave(_ sender: Any) {
        self.view.endEditing(true)
        
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            self.ws_AddCar()
        }
    }
 
    func checkValidation() -> String? {
        
        if tfVinNumber.text?.count == 0 {
            return AlertMSG.blankVinNo
            
        } else if lblBrandName.text == "Choose brand" {
            return AlertMSG.blankBrand
            
        } else if lblModelName.text == "Choose model" {
            return AlertMSG.blankModel
            
        } else if selectindex == -1 {
            return AlertMSG.blankThemeColor
            
        } else if lblYearName.text == "Year" {
            return AlertMSG.blankYear
            
        } else if tfOBD.text?.count == 0 {
            return AlertMSG.blankObd
            
        } else if tfSerialNo.text?.count == 0 {
            return AlertMSG.blankIdAndSerialNo
        }
        
        return nil
    }
    
    //MARK:- CAMERA AND GALLARY FUNCTION
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //MARK : CAMERAVIEW DELEGATES
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let img:UIImage? = info[UIImagePickerControllerEditedImage] as? UIImage
        
        if (img != nil) {
            imgCar.image = img?.resize(300.0)
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- COLLECTIONVIEW_DELEGATES
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrColorList.count
    }
    
    let checkimg = UIImage(named: "check.png")
    let uncheckimg = UIImage(named: "")
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddCarCell", for: indexPath) as! AddCarCell
        
        let dict = arrColorList.object(at: indexPath.row) as! colorList
        let strCode = dict.code
        let colorCode = strCode.replacingOccurrences(of: "#", with: "")
        cell.imgBg.backgroundColor = UIColor(hex: colorCode)
        
        if indexPath.row == selectindex {
            cell.imgColor.image = checkimg
            cell.imgBg.layer.borderWidth = 1
            cell.imgBg.layer.borderColor = UIColor.white.cgColor
        } else {
            cell.imgColor.image = uncheckimg
            cell.imgBg.layer.borderWidth = 0
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dict = arrColorList.object(at: indexPath.row) as! colorList
        selectindex = indexPath.row
        self.colorId = dict.id
        collection.reloadData()
    }
    
    func ws_colorList() {
        
        Http.instance().json(api_colorlist, nil, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    
                    if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                        
                        for i in 0..<result.count {
                            let dict = result[i] as! NSDictionary
                            let ob = colorList()
                            ob.code = string(dict, "code")
                            ob.id = string(dict, "id")
                            ob.title = string(dict, "title")
                            self.arrColorList.add(ob)
                        }
                        self.collection.reloadData()
                    }
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
    }
    
    //------------------------------- POPUP-VIEW ---------------------------------------//
    //------------------------------- POPUP-VIEW ---------------------------------------//
    //------------------------------- POPUP-VIEW ---------------------------------------//
    //------------------------------- POPUP-VIEW ---------------------------------------//
    //------------------------------- POPUP-VIEW ---------------------------------------//
    //------------------------------- POPUP-VIEW ---------------------------------------//
    //------------------------------- POPUP-VIEW ---------------------------------------//
    //------------------------------- POPUP-VIEW ---------------------------------------//
    //------------------------------- POPUP-VIEW ---------------------------------------//
    //------------------------------- POPUP-VIEW ---------------------------------------//
    
    @IBAction func actionStringClear(_ sender: Any) {
        tfSearch.text = ""
        btnClearString.isHidden = true
        
        /*if tfSearch.text == "" {
            lblNoRecord.isHidden = true
        } else {
            lblNoRecord.isHidden = false
        }*/
        
        if strLocal == "brand" {
            self.arrTable = self.arrBrandList
            
        } else if strLocal == "model" {
            self.arrTable = self.arrModelList
            
        } else if strLocal == "year" {
            self.arrTable = self.arrYear
        }
        
        tblView.reloadData()
    }
    
    @IBAction func actionPopupClose(_ sender: Any) {
        self.view.endEditing(true)
        removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: subView)
        scroll.contentInset = UIEdgeInsets.zero
    }
    
    func addDoneButtonOnKeyboard() {
        tfSearch.text = ""
        
        if strLocal == "year" {
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            
            doneToolbar.barStyle = UIBarStyle.blackTranslucent
            doneToolbar.barTintColor = appColor.appPrimaryColor//kAppDelegate.AppColor
            doneToolbar.tintColor = UIColor.white
            
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            
            let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.done))
            
            var items:[UIBarButtonItem] = []
            items.append(flexSpace)
            items.append(done)
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            self.tfSearch.inputAccessoryView = doneToolbar
            //self.tfVinNumber.inputAccessoryView = doneToolbar
            
        } else {
            
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.blackTranslucent
            //doneToolbar.barTintColor = UIColor.t
            doneToolbar.tintColor = UIColor.white
            doneToolbar.isHidden = true
            
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            
            //let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.done))
            
            var items:[UIBarButtonItem] = []
            items.append(flexSpace)
            //items.append(done)
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            self.tfSearch.inputAccessoryView = doneToolbar
            tfSearch.keyboardType = UIKeyboardType.default
        }
    }
  
    func done() {
        tfSearch.resignFirstResponder()
        tfVinNumber.resignFirstResponder()
        scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scroll.contentInset = UIEdgeInsets.zero
    }
    
    //MARK:- FUNCTION FOR POPVIEW
    func popupShow() {
        viewPopup.frame = self.view.frame
        viewPopup.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.window?.addSubview(viewPopup)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = (self as UIGestureRecognizerDelegate)
        self.viewPopup.addGestureRecognizer(tap)
        
        self.subView.leftAnchor.constraint(equalTo: viewPopup.leftAnchor, constant: 15).isActive = true
        self.subView.rightAnchor.constraint(equalTo: viewPopup.rightAnchor, constant: -15).isActive = true
        
        self.subView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        self.subView.heightAnchor.constraint(equalToConstant: 568).isActive = true
        self.test(viewTest: subView)
    }
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewPopup: UIView, viewWebShow: UIView) {
        let orignalT: CGAffineTransform = viewWebShow.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewWebShow.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewPopup.removeFromSuperview()
            viewWebShow.transform = orignalT
        })
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: subView)
        scroll.contentInset = UIEdgeInsets.zero
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if subView.bounds.contains(touch.location(in: subView)) {
            return false
        }
        
        return true
    }
    
    //MARK:- TABLEVIEW DELETE ----------------------------------------
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblView {
            return arrTable.count
            
            if lblGeneric == nil {
                return 0
            }
            
            if lblGeneric == lblBrandName {
                return arrBrandList.count
                
            } else if lblGeneric == lblModelName {
                return arrModelList.count
                
            } else if lblGeneric == lblYearName {
                return arrYearList.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BrandModelYearCell", for: indexPath) as! BrandModelYearCell
      
        if lblGeneric == lblBrandName {
            let dict = arrTable.object(at: indexPath.row) as! brandList
            cell.lblName.text = dict.title
            return cell
            
        } else if lblGeneric == lblModelName {
            let dict = arrTable.object(at: indexPath.row) as! modelList
            cell.lblName.text = dict.title
            return cell
            
        } else if lblGeneric == lblYearName {
            let dict = arrTable.object(at: indexPath.row) as! yearList
            cell.lblName.text = dict.title
            return cell
        }
        
        if lblGeneric != nil {
            
            if lblGeneric == lblBrandName {
                let dict = arrBrandList.object(at: indexPath.row) as! brandList
                cell.lblName.text = dict.title
                return cell
                
            } else if lblGeneric == lblModelName {
                let dict = arrModelList.object(at: indexPath.row) as! modelList
                cell.lblName.text = dict.title
                return cell
                
            } else if lblGeneric == lblYearName {
                let dict = arrYearList.object(at: indexPath.row) as! yearList
                cell.lblName.text = dict.title
                return cell
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblView {
            if lblGeneric == lblBrandName {
                let dict = arrTable.object(at: indexPath.row) as! brandList
                let str = dict.title
                
                if str != "Choose brand" {
                    brandId = dict.id
                    let defaults = UserDefaults.standard
                    defaults.removeObject(forKey: "savedVehicle")
                    defaults.removeObject(forKey: "savedId")
                    defaults.set(dict.title, forKey: "savedVehicle")
                    defaults.set(dict.id, forKey: "savedId")
            
                    lblBrandName.text = dict.title.capFirstLetter()
                    lblModelName.text = "Choose model"
                    
                    if lblBrandName.text == "Choose brand" {
                        selectBrand = ""
                    } else {
                        selectBrand = "yes"
                    }
                    
                    removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: subView)
                }
                
            } else if lblGeneric == lblModelName {
                
                let dict = arrTable.object(at: indexPath.row) as! modelList
                let str = dict.title
                
                if str != "Choose model" {
                    lblModelName.text = dict.title.capFirstLetter()
                    self.modelId = dict.id
                    removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: subView)
                }
                
            } else if lblGeneric == lblYearName {
                let dict = arrTable.object(at: indexPath.row) as! yearList
                let str = dict.title
                
                if str != "Choose year" {
                    lblYearName.text = dict.title
                    removeSubViewWithAnimation(viewPopup: viewPopup, viewWebShow: subView)
                }
            }
        }
    }
    
    /////////__________***********************_________
    //MARK:- KEYBOARD NOTIFICATION METHODS
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scroll.contentInset = UIEdgeInsets.zero
        } else {
            scroll.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        
        scroll.scrollIndicatorInsets = scroll.contentInset
    }
    /////////__________***********************_________
    
    //MARK:- TEXTFIELD_DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if tfVinNumber == textField {
            strLocal = "year"
            addDoneButtonOnKeyboard()
        } else {
            strLocal = ""
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfSearch {
            tfSearch.resignFirstResponder()
    
        } else if textField == tfVinNumber {
            tfVinNumber.resignFirstResponder()
            scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            scroll.contentInset = UIEdgeInsets.zero
            
        } else if textField == tfOBD {
            tfSerialNo.becomeFirstResponder()
            
        } else if textField == tfSerialNo {
            tfSerialNo.resignFirstResponder()
            scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        
        return true
    }

    //MARK:- ARRAY DATA SEARCH-*-*-*-*-*-*-*-*-*-*
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newLength: Int = textField.text!.count + string.count - range.length
        
        if tfSearch.text == "0" {
            btnClearString.isHidden = true
        } else {
            btnClearString.isHidden = false
        }
        
        if textField == tfVinNumber {
            let characterSet = CharacterSet.init(charactersIn: kAppDelegate.ACCEPTABLE_CHARACTERSWITHNO).inverted
            newLength = 17
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            let filter = string.components(separatedBy: characterSet).joined(separator:"")
            return ((string == filter) && newString.length <= newLength)
            
        } else if textField == tfOBD {
            let characterSet = CharacterSet.init(charactersIn: kAppDelegate.ACCEPTABLE_CHARACTERSWITHNO).inverted
            newLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            let filter = string.components(separatedBy: characterSet).joined(separator:"")
            return ((string == filter) && newString.length <= newLength)
            
        } else if textField == tfSerialNo {
            let characterSet = CharacterSet.init(charactersIn: kAppDelegate.ACCEPTABLE_CHARACTERSWITHNO).inverted
            newLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            let filter = string.components(separatedBy: characterSet).joined(separator:"")
            return ((string == filter) && newString.length <= newLength)
        }
        
        if textField == tfSearch {
            if string == "\n" {
                textField.resignFirstResponder()
                return false
            } else {
                if newLength == 0 {
                    if lblGeneric == lblBrandName {
                        arrTable = arrBrandList
                    } else if lblGeneric == lblModelName {
                        arrTable = arrModelList
                    } else if lblGeneric == lblYearName {
                        arrTable = arrYearList
                    }
                    tblView.reloadData()
                } else {
                    search(strSearch: textField.text! + string)
                }
            }
        }
        
        if textField == tfSearch {
            return (newLength > 50) ? false : true
        }
        
        if textField == tfOBD {
            return (newLength > 50) ? false : true
        } else if textField == tfSerialNo {
            return (newLength > 50) ? false : true
        } else if textField == tfVinNumber {
            return (newLength > 17) ? false : true
        }
        
        return true
    }
    
    func search(strSearch:String) {
        let arr =  NSMutableArray()
        if lblGeneric == lblBrandName {
            
            for i in 0 ..< arrBrandList.count {
                let dict = arrBrandList.object(at: i) as! brandList
                print(strSearch)
                if dict.title.lowercased().subString(strSearch.lowercased()) {
                    arr.add(dict)
                }
            }
            
            arrTable = arr
            
        } else if lblGeneric == lblModelName {
            for i in 0 ..< arrModelList.count {
                let dict = arrModelList.object(at: i) as! modelList
                print(strSearch)
                if dict.title.subString(strSearch) {
                    arr.add(dict)
                }
            }
            
            arrTable = arr
            
        } else if lblGeneric == lblYearName {
            for i in 0 ..< arrYearList.count {
                let dict = arrYearList.object(at: i) as! yearList
                print(strSearch)
                if dict.title.subString(strSearch) {
                    arr.add(dict)
                }
            }
            arrTable = arr
        }

        tblView.reloadData()
    }
    
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*- WS_Call *-*-*-*-*-*-*-*-*-*-*-*-//
    
    func ws_BrandList() {
        
        Http.instance().json(api_brandlist, nil, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    
                    self.arrBrandList = NSMutableArray()
                    self.arrModelList = NSMutableArray()
                    self.arrYearList = NSMutableArray()
                    
                    if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                        
                        let ob = brandList()
                        ob.id = "-1"
                        ob.title = "Choose brand"
                        self.arrBrandList.add(ob)
                        
                        for i in 0..<result.count {
                            let dict = result[i] as! NSDictionary
                            let ob = brandList()
                            ob.code = string(dict, "code")
                            ob.id = string(dict, "id")
                            ob.title = string(dict, "title")
                            self.arrBrandList.add(ob)
                        }
                        
                        self.arrTable = self.arrBrandList
                        self.tblView.reloadData()
                        self.popupShow()
                    }
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
    }
    
    func ws_ModelList() {
        
        Http.instance().json(api_Modellist + brandId, nil, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    
                    self.arrBrandList = NSMutableArray()
                    self.arrModelList = NSMutableArray()
                    self.arrYearList = NSMutableArray()
                    
                    if let result = (json as AnyObject).object(forKey: "result") as? NSArray {
                        
                        let ob = modelList()
                        ob.id = "-1"
                        ob.title = "Choose model"
                        self.arrModelList.add(ob)
                        
                        for i in 0..<result.count {
                            let dict = result[i] as! NSDictionary
                            let ob = modelList()
                            ob.id = string(dict, "id")
                            ob.title = string(dict, "title")
                            ob.code = string(dict, "code")
                            self.arrModelList.add(ob)
                        }
                        
                        self.arrTable = self.arrModelList
                        self.tblView.reloadData()
                        self.popupShow()
                    }
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
    }
    
    func static_YearList() {
        
        self.arrBrandList = NSMutableArray()
        self.arrModelList = NSMutableArray()
        self.arrYearList = NSMutableArray()
        
        let ob = yearList()
        ob.title = "Choose year"
        self.arrYearList.add(ob)
        
        for i in 0..<arrYear.count {
            let dict = arrYear[i] as! NSDictionary
            let ob = yearList()
            ob.title = string(dict, "title")
            self.arrYearList.add(ob)
        }
        
        self.arrTable = self.arrYearList
        self.tblView.reloadData()
        self.popupShow()
    }
   
    func ws_AddCar() {
        
        let imageData:NSData = UIImagePNGRepresentation(imgCar.image!)! as NSData
        let imageStr = "data:image/jpeg;base64," + imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        let params = NSMutableDictionary()
        params["make_id"] = brandId
        params["model_id"] = modelId
        params["color_id"] = colorId
        params["vin_number"] = tfVinNumber.text!
        params["id_serial_no"] = tfSerialNo.text!
        params["obd_device_make"] = tfOBD.text!
        params["year"] = lblYearName.text!
        params["lat"] = ""
        params["long"] = ""
        params["vehicle_image"] = imageStr
        params["vehicle_image_name"] = lblBrandName.text!
        
         Http.instance().json(api_addVehicle, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in

            if (json != nil) {
                if number(json! as! NSDictionary, "success").boolValue {
                    kAppDelegate.arrGLVehicleList = NSMutableArray()
                    //Http.alert("", string(json! as! NSDictionary, "message"))
                    
//                    let user = User()
//                    user.no_of_vehicle = string(json! as! NSDictionary, "no_of_vehicle")
//                    User.save(user)
                    
                    kAppDelegate.dictUserInfo.removeObject(forKey: "no_of_vehicle")
                    kAppDelegate.dictUserInfo["no_of_vehicle"] = string(json! as! NSDictionary, "no_of_vehicle")
      
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "RootVendor") as! RootVendor
                    self.navigationController?.pushViewController(vc, animated: true)
              
                } else {
                    Http.alert("", string(json! as! NSDictionary, "message"))
                }
            }
        }
    }
}//Sachin :]

class AddCarCell : UICollectionViewCell {
    
    @IBOutlet var imgBg: UIImageView!
    @IBOutlet var imgColor: UIImageView!
}

class BrandModelYearCell : UITableViewCell {
    @IBOutlet var lblName: UILabel!
}


/*func BackButton()  {
 let button1 = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(actionMenuButton)) //
 self.navigationItem.leftBarButtonItem = button1
 }
 
 //MARK:Navigation Buttons
 func refreshRightNavigationButton()  {
 let button1 = UIBarButtonItem(image: UIImage(named: "check.png"), style: .plain, target: self, action: #selector(actionDeleteButton)) // action:#selector(Class.MethodName) for swift 3
 self.navigationItem.rightBarButtonItem = button1
 }*/

/*func actionDeleteButton()  {
 print("navigation right buttion clicked")
 }
 
 func actionMenuButton()  {
 _ = self.navigationController?.popViewController(animated: true)
 }
 
 override func viewWillDisappear(_ animated: Bool) {
 self.title  = ""
 }
 
 override func didReceiveMemoryWarning() {
 super.didReceiveMemoryWarning()
 }*/

