//
//  RatingViewVC.swift
//  Telenyze
//
//  Created by Himanshu on 15/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class RatingViewVC: UIViewController, FloatRatingViewDelegate, UITextFieldDelegate {
    
    @IBOutlet var tfdescription: UITextField!
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var viewRatingAppointment: FloatRatingView!
    @IBOutlet var viewRatingCommunication: FloatRatingView!
    @IBOutlet var viewRatingQuality: FloatRatingView!
    @IBOutlet var viewRatingShopFacilities: FloatRatingView!
    @IBOutlet var viewRatingShopLocation: FloatRatingView!
    @IBOutlet var viewRatingOnlineServies: FloatRatingView!
    @IBOutlet var viewRatingPrice: FloatRatingView!
    @IBOutlet var viewRatingRecommend: FloatRatingView!
    @IBOutlet var viewRatingAverage: FloatRatingView!
    
    var strcome = "yes"
    var rating1:Float = 5.0
    var rating2:Float = 5.0
    var rating3:Float = 5.0
    var rating4:Float = 5.0
    var rating5:Float = 5.0
    var rating6:Float = 5.0
    var rating7:Float = 5.0
    var rating8:Float = 5.0
    var totalRating:Float = 0.0
    var averageRating:Float = 0.0
    
    var shop_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerForKeyboardNotifications()
        self.setBarrating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setBarrating()
        tfdescription.setLeftPaddingPoints(5)
        setNav(className: "Please rate your visit")
        self.BackNavigationButton()
    }
    
    func BackNavigationButton() {
        let button1 = UIBarButtonItem(image: UIImage(named: "CrossButton"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionBackButton() {
        _ = self.navigationController?.popViewController(animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
        self.deregisterFromKeyboardNotifications()
    }
  
    func setBarrating() {
        self.viewRatingAppointment.delegate = self
        self.viewRatingCommunication.delegate = self
        self.viewRatingQuality.delegate = self
        self.viewRatingShopFacilities.delegate = self
        self.viewRatingShopLocation.delegate = self
        self.viewRatingOnlineServies.delegate = self
        self.viewRatingPrice.delegate = self
        self.viewRatingRecommend.delegate = self
        self.viewRatingAverage.delegate = self
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newLength: Int = textField.text!.count + string.count - range.length
        
        if textField == tfdescription {
            let characterSet = CharacterSet.init(charactersIn: kAppDelegate.ACCEPTABLE_CHARACTERSWITHNO).inverted
            newLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            let filter = string.components(separatedBy: characterSet).joined(separator:"")
            return ((string == filter) && newString.length <= newLength)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfdescription {
            tfdescription.resignFirstResponder()
            scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        
        return true
    }
    
    // MARK:- FloatRatingViewDelegate
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating:Float) {
        
        if ratingView == viewRatingAppointment{
            rating1 = Float(self.viewRatingAppointment.rating)
            
        } else if ratingView == viewRatingCommunication{
            rating2 = Float(self.viewRatingCommunication.rating)
            
        } else if ratingView == viewRatingQuality{
            rating3 = Float(self.viewRatingQuality.rating)
            
        }else if ratingView == viewRatingShopFacilities{
            rating4 = Float(self.viewRatingShopFacilities.rating)
            
        }else if ratingView == viewRatingShopLocation{
            rating5 = Float(self.viewRatingShopLocation.rating)
            
        }else if ratingView == viewRatingOnlineServies{
            rating6 = Float(self.viewRatingOnlineServies.rating)
            
        }else if ratingView == viewRatingPrice{
            rating7 = Float(self.viewRatingPrice.rating)
            
        }else if ratingView == viewRatingRecommend{
            rating8 = Float(self.viewRatingRecommend.rating)
        }
        
        totalRating = rating1 + rating2 + rating3 + rating4 + rating5 + rating6 + rating7 + rating8
        
       averageRating =   totalRating * 100 / 800
        
        viewRatingAverage.rating = averageRating
        strcome = "no"
        
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
    }
    
    //MARK:- KEYBOARD NOTIFICATION METHODS
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scroll.contentInset = UIEdgeInsets.zero
        } else {
            scroll.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        scroll.scrollIndicatorInsets = scroll.contentInset
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        if tfdescription.text! == "" {
            Http.alert("", "Please enter description.")
        } else {
            self.ws_Addreview()
        }
    }
    
    func ws_Addreview() {
        
        let params = NSMutableDictionary()
        
        if strcome == "yes"{
          params["avg_rating"] = "5.0"
        } else {
          params["avg_rating"] = averageRating
        }
        
        params["shop_id"] = shop_id
        params["easy_of_appointment"] = rating1
        params["communications"] = rating2
        params["qwality_of_work"] = rating3
        params["shop_facilities"] = rating4
        params["shop_location"] = rating5
        params["online_services"] = rating6
        params["price"] = rating7
        params["will_u_recomed"] = rating8
        
        params["feedback"] = tfdescription.text!
        
        Http.instance().json(api_addReview, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    
                    _ = self.navigationController?.popViewController(animated: false)
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
}//Sachin :]

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
