//
//  ProfileView.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 2 on 06/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import Foundation
import HarishFrameworkSwift4

class ProfileViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var scrlView: UIScrollView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var tfFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var tfLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var tfPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var tfUserID: SkyFloatingLabelTextField!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var tfEmergencyEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var tfEmergencyPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSaveUpdates: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegatesAndDisplayView()
        self.addDoneButtonOnKeyboard()
        self.addDoneButtonOnKeyboardTwo()
        self.setUserProfileData()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hideKeyboard()
        self.menuNavigationButton()
        setNav(className: "My Profile")
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
        self.title  = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "MenuButton"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
    
    //MARK:- Button Actions
    @IBAction func btnChangePassword(_ sender: Any) {
        self.hideKeyboard()
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordView") as! ChangePasswordView
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSaveUpdates(_ sender: Any) {
        self.hideKeyboard()
        
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            wsEditProfile()
        }
    }
    
    //MARK: FUNCTIONS.
    func delegatesAndDisplayView(){
        tfFirstName.delegate = self
        tfLastName.delegate = self
        tfEmail.delegate = self
        tfPhoneNumber.delegate = self
        tfUserID.delegate = self
        tfEmergencyPhoneNumber.delegate = self
        tfEmergencyEmail.delegate = self
    }
    
    func setUserProfileData()  {
        
        tfFirstName.text! = string(kAppDelegate.dictUserInfo, "first_name")
        tfLastName.text! = string(kAppDelegate.dictUserInfo, "last_name")
        tfEmail.text! = string(kAppDelegate.dictUserInfo, "email")
        tfPhoneNumber.text! = string(kAppDelegate.dictUserInfo, "contact_no")
        tfUserID.text! = string(kAppDelegate.dictUserInfo, "username")
        tfEmergencyPhoneNumber.text! = string(kAppDelegate.dictUserInfo, "emergency_no")
        tfEmergencyEmail.text! = string(kAppDelegate.dictUserInfo, "emergency_email")
    }
    
    func checkValidation() -> String? {
        
        if tfFirstName.text?.count == 0 || tfLastName.text?.count == 0 || tfEmail.text?.count == 0 || tfUserID.text?.count == 0 || tfPhoneNumber.text?.count == 0 {
            return AlertMSG.blankRequiredFields
            
        } else {
            
            if (tfEmail.text?.count)! < 6 || !Validate.email(tfEmail.text!){
                return AlertMSG.invalidEmailFormat
                
            } else if (tfPhoneNumber.text?.count)! < 10 {
                return AlertMSG.invalidMobileNumber
                
            } else if (tfEmergencyEmail.text?.count)! != 0 && ((tfEmergencyEmail.text?.count)! < 6 || !Validate.email(tfEmergencyEmail.text!)) {
                return AlertMSG.invalidEmailFormat
                
            } else if (tfEmergencyPhoneNumber.text?.count)! != 0 && ((tfEmergencyPhoneNumber.text?.count)! < 10 ) {
                return AlertMSG.invalidMobileNumber
            }
        }
        
        return nil
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
        scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scrlView.contentInset = UIEdgeInsets.zero
    }
    
    //MARK:- TEXTFIELD_DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if tfFirstName == textField {
            tfLastName.becomeFirstResponder()
        } else if tfLastName == textField {
            tfEmail.becomeFirstResponder()
        } else if tfEmail == textField {
            tfPhoneNumber.becomeFirstResponder()
        } else if tfPhoneNumber == textField {
            tfUserID.becomeFirstResponder()
        } else if tfUserID == textField {
            tfEmergencyEmail.becomeFirstResponder()
        } else if tfEmergencyEmail == textField {
            tfEmergencyPhoneNumber.becomeFirstResponder()
        } else if tfEmergencyPhoneNumber == textField {
            tfEmergencyPhoneNumber.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var length = (textField.text?.count)! + string.count - range.length
        
        if textField == tfFirstName {
            let characterSet = CharacterSet.init(charactersIn: kAppDelegate.ACCEPTABLE_CHARACTERS).inverted
            length = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            let filter = string.components(separatedBy: characterSet).joined(separator:"")
            return ((string == filter) && newString.length <= length)
            
        } else if textField == tfLastName {
            let characterSet = CharacterSet.init(charactersIn: kAppDelegate.ACCEPTABLE_CHARACTERS).inverted
            length = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            let filter = string.components(separatedBy: characterSet).joined(separator:"")
            return ((string == filter) && newString.length <= length)
        }
        
        if textField == tfFirstName {
            return (length > 50) ? false : true
        } else if textField == tfLastName {
            return (length > 50) ? false : true
        } else if textField == tfEmail {
            return (length > 50) ? false : true
        } else if textField == tfPhoneNumber {
            return (length > 10) ? false : true
        } else if textField == tfUserID {
            return (length > 20) ? false : true
        } else if textField == tfEmergencyEmail {
            return (length > 50) ? false : true
        } else if textField == tfEmergencyPhoneNumber {
            return (length > 10) ? false : true
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrlView.contentSize = CGSize(width: view.frame.size.width, height: btnSaveUpdates.frame.origin.y + btnSaveUpdates.frame.size.height + 10)
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = appColor.appPrimaryColor
        doneToolbar.tintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let next: UIBarButtonItem = UIBarButtonItem(title: "NEXT", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.nextOfDoneTool))
        let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.hideKeyboard))
        
        var items:[UIBarButtonItem] = []
        items.append(next)
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.tfPhoneNumber.inputAccessoryView = doneToolbar
    }
    
    func addDoneButtonOnKeyboardTwo() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = appColor.appPrimaryColor
        doneToolbar.tintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.hideKeyboard))
        
        var items:[UIBarButtonItem] = []
        //items.append(next)
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.tfEmergencyPhoneNumber.inputAccessoryView = doneToolbar
    }
    
    func done() {
        self.view.endEditing(true)
    }
    
    func nextOfDoneTool () {
        tfUserID.becomeFirstResponder()
    }
    
    //__________**********__________*************_________************__________
    //MARK:- KEYBOARD NOTIFICATION METHODS
    func registerForKeyboardNotifications() {
    let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrlView.contentInset = UIEdgeInsets.zero
        } else {
            scrlView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        
        scrlView.scrollIndicatorInsets = scrlView.contentInset
    }
    /////////__________***********************_________
    
    //MARK:- Edit Profile
    func wsEditProfile() {
        let password = "123456"   //dictUser.password
        let params = NSMutableDictionary()
        params["first_name"] = tfFirstName.text
        params["last_name"] = tfLastName.text
        params["password"] = password
        params["username"] = tfUserID.text
        params["contact_no"] = tfPhoneNumber.text
        params["email"] = tfEmail.text
        params["emergency_email"] = tfEmergencyEmail.text
        params["emergency_no"] = tfEmergencyPhoneNumber.text
        
        Http.instance().json(api_edit_profile, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    Http.alert("", string(json! , "message"))
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
}//_____***********||||************_____
