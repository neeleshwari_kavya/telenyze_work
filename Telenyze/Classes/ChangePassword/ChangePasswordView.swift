//
//  ChangePasswordView.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 2 on 07/02/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import Foundation
import HarishFrameworkSwift4

class ChangePasswordView: UIViewController , UIGestureRecognizerDelegate, UITextFieldDelegate{
    
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tfCurrentPassword: SkyFloatingLabelTextField!
    @IBOutlet var tfNewPassword: SkyFloatingLabelTextField!
    @IBOutlet var tfConfirmPassword: SkyFloatingLabelTextField!
    @IBOutlet var btnChangePassword: UIButton!
    
    //variables
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegatesAndDisplayView()
        self.hideKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setBackButton()
        setNav(className: "Change Password")
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Button Actions
    
    @IBAction func btnChangePassword(_ sender: Any) {
        self.hideKeyboard()
        
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            wsChangePassword()
        }
    }
    
    //MARK: FUNCTIONS.
    func delegatesAndDisplayView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.delegate = self
        self.subView.addGestureRecognizer(tap)
        
        tfCurrentPassword.delegate = self
        tfNewPassword.delegate = self
        tfConfirmPassword.delegate = self
    }
    
    func checkValidation() -> String? {
        
        if tfCurrentPassword.text?.count == 0  {
            
            return AlertMSG.blankCurrentPassword
        }else if tfNewPassword.text?.count == 0  {
            
            return AlertMSG.blankNewPassword
        }else if (tfNewPassword.text?.count)! < 6  {
            
            return AlertMSG.invalidPassword
        }else if tfConfirmPassword.text?.count == 0  {
            
            return AlertMSG.blankConfirmPassword
        }else if tfNewPassword.text !=  tfConfirmPassword.text  {
            
            return AlertMSG.misMatchCurrentNewPwd
        }
        return nil
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
        scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scrlView.contentInset = UIEdgeInsets.zero
    }
    
    //MARK:- TEXTFIELD_DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if tfCurrentPassword == textField {
            tfNewPassword.becomeFirstResponder()
        } else if tfNewPassword == textField {
            tfConfirmPassword.becomeFirstResponder()
        } else if tfConfirmPassword == textField {
            tfConfirmPassword.resignFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        
        if textField == tfCurrentPassword {
            return (length > 20) ? false : true
        } else if textField == tfNewPassword {
            return (length > 20) ? false : true
        } else if textField == tfConfirmPassword {
            return (length > 20) ? false : true
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    /////////__________***********************_________
    //MARK:- KEYBOARD NOTIFICATION METHODS
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrlView.contentInset = UIEdgeInsets.zero
        } else {
            scrlView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        
        scrlView.scrollIndicatorInsets = scrlView.contentInset
    }
    /////////__________***********************_________
    
    //MARK:- Change Password
    func wsChangePassword() {
        
        let params = NSMutableDictionary()
        params["old_password"] = tfCurrentPassword.text
        params["new_password"] = tfNewPassword.text
        
        Http.instance().json(api_change_password, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    Http.alert("", string(json! , "message"))
                    
                    print("Password Changed.")
                    self.tfConfirmPassword.text = ""
                    self.tfNewPassword.text = ""
                    self.tfConfirmPassword.text = ""
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    
}//_____***********||||************_____



