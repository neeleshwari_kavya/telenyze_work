// changes done at 5.48..... 20 march 2018
//  SignInVC.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 31/01/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4
import UserNotifications

class SignInVC: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate, UNUserNotificationCenterDelegate  {
    
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet var tfPassword: SkyFloatingLabelTextField!
    @IBOutlet var btnForgot: UIButton!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var btnSignIn: UIButton!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerForKeyboardNotifications()
        self.delegatesAndDisplayView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tfEmail.placeHolderColor = UIColor.white
        tfPassword.placeHolderColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = true
        kAppDelegate.arrGLVehicleList = NSMutableArray()
        self.hideKeyboard()
        
        scroll.keyboardDismissMode = .onDrag
        tfEmail.text = "neel"//"pa"
        tfPassword.text = "123456"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Actions.
    @IBAction func actionLogIn(_ sender: Any) {
        
        self.hideKeyboard()
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            self.ws_SignIn()
        }
 
       // self.hello()
    }
    func hello(){
        
        //creating the notification content
        let content = UNMutableNotificationContent()
        
        //adding title, subtitle, body and badge
        content.title = "Hey this is Simplified iOS"
        content.subtitle = "iOS Development is fun"
        content.body = "We are learning about iOS Local Notification"
        content.badge = 1
        
        //getting the notification trigger
        //it will be called after 5 seconds
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        //getting the notification request
        
        let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: content, trigger: trigger)
        
        //adding the notification to notification center
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    @IBAction func actionForgot(_ sender: Any) {
       /*
        self.view.endEditing(true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPassVC") as! ForgotPassVC
        self.navigationController?.pushViewController(vc, animated: true)
        */
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RepairStatusVC") as! RepairStatusVC
       self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func actionSignUp(_ sender: Any) {
        self.view.endEditing(true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: FUNCTIONS.
    func delegatesAndDisplayView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.delegate = self
        self.subView.addGestureRecognizer(tap)
        
        tfEmail.delegate = self
        tfPassword.delegate = self
    }
    
    func checkValidation() -> String? {
        let string1 = tfEmail.text!
        
        if tfEmail.text?.count == 0 || tfPassword.text?.count == 0  {
            return AlertMSG.blankEmailOrPassword
            
        } else if (string1 as NSString).contains("@") && !Validate.email(tfEmail.text!) {
            return AlertMSG.invalidEmailFormat
            
        } else if (tfPassword.text?.count)! < 6 {
            return AlertMSG.passwordLength
        }
        
        return nil
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
        scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scroll.contentInset = UIEdgeInsets.zero
    }

    //MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if tfEmail == textField {
            tfPassword.becomeFirstResponder()
        } else if textField == tfPassword {
            tfPassword.resignFirstResponder()
            scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        if textField == tfEmail {
            return (length > 40) ? false : true
        }
        else if textField == tfPassword {
            return (length > 20) ? false : true
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scroll.contentSize = CGSize(width: view.frame.size.width, height: btnSignUp.frame.origin.y + btnSignUp.frame.size.height + 10)
    }
    
    //MARK:- KEYBOARD NOTIFICATION METHODS
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scroll.contentInset = UIEdgeInsets.zero
        } else {
            scroll.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        scroll.scrollIndicatorInsets = scroll.contentInset
    }
    
    //MARK: WS_SignIn
    func ws_SignIn() {
        
        let params = NSMutableDictionary()
        let string1 = tfEmail.text!
        
        if (string1 as NSString).contains("@") {
            params["email"] = string1
        } else {
            params["username"] = string1
        }
        
        params["password"] = tfPassword.text!
        params["device_id"] = kAppDelegate.DeviceID
        params["device_type"] = kAppDelegate.device_type//"iPhone"
        params["notification_id"] = kAppDelegate.strNotificationToken//"123"
        params["app_version"] = "1.0"
        
        print("params>>>\(params)")
        
        Http.instance().json(api_signIn, params, "POST", ai: true, popup: true, prnt: true) { (json, params, strJson)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    
                    if let result = (json as AnyObject).object(forKey: "result") as? NSDictionary {
                   
                        kAppDelegate.dictUserInfo = result.mutableCopy() as! NSMutableDictionary
                        self.tfEmail.text = ""
                        self.tfPassword.text = ""
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RootVendor") as! RootVendor
                        kAppDelegate.arrGLVehicleList = NSMutableArray()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
   
    override var prefersStatusBarHidden: Bool {
        return true
    }
 
}//Sachin :]
