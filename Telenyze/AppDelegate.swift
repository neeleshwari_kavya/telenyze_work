//
//  AppDelegate.swift
//  Telenyze
//
//  Created by Kavya Mac Mini 3 on 31/01/18.
//  Copyright © 2018 Kavya Mac Mini 3. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4
import CoreLocation
import AlamofireImage
import Fabric
import Crashlytics
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
let KEY_UserInfo = "KEY_UserInfo"


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate  {
    
    var window: UIWindow?
    var server:BaseUrl?
    var menuPush = ""
    var shopId = ""
    var dictShopNeeleshwari = NSDictionary()
    var dictCatchNotifications = NSMutableDictionary()
    // let AppColor = UIColor(hex: "29669b")
    var arrGLVehicleList = NSMutableArray()
    let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
    let ACCEPTABLE_CHARACTERSWITHNO = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789. "
    
    let googleApiKey = "AIzaSyArUQyLyWtbNGR4nJJiZcX-_YUjNNXyNYc"//new key generated on 21 March ....neeleshwari
    let DeviceID = UIDevice.current.identifierForVendor!.uuidString
    let device_type = "iPhone"
    var dictUserInfo = NSMutableDictionary()
    
    var strNotificationToken = ""
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //MARK:- NAVIGATION CONTROLLER TITLE IN WHITE COLOR CODE
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        FirebaseApp.configure()
        
        //header()
        //notification
        
        let center  = UNUserNotificationCenter.current()
        center.delegate = self
        Messaging.messaging().remoteMessageDelegate = self
        // set the type as sound or badge
        center.requestAuthorization(options: [.sound,.alert,.badge]) { (granted, error) in
            // Enable or disable features based on authorization
        }
        application.registerForRemoteNotifications()
      
        // var badgeCount = 0
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.tokenRefreshNotification),name: NSNotification.Name.InstanceIDTokenRefresh,object: nil)
        
        if InstanceID.instanceID().token() != nil {
            let refreshedToken = InstanceID.instanceID().token()!
            print("InstanceID token: \(refreshedToken)")
            strNotificationToken = refreshedToken
            print("strNotificationToken: \(strNotificationToken)")
        }
        //basic functions
        getBaseurl()
        initLocationManager()
        //for crash/bug/fatalError()  report
        Fabric.with([Crashlytics.self])
        
        return true
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {//addred new
        application.registerForRemoteNotifications()
    }
    
    //FOR FCM
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        InstanceID.instanceID().setAPNSToken(deviceToken as Data, type: .sandbox)
        if InstanceID.instanceID().token() != nil {
            let refreshedToken = InstanceID.instanceID().token()!
            print("InstanceID token: \(refreshedToken)")
            strNotificationToken = refreshedToken
            print("strNotificationToken: \(strNotificationToken)")
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Couldn't register: \(error)")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("Notif register: \(userInfo)")
        //        NSNotificationCenter.defaultCenter().postNotificationName("ReceiveRemoteNotification", object: nil, userInfo: userInfo)
        let appReceiptTime  = NSData()
        print("time notification arrival\(appReceiptTime)")
        
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TelenyzeGetNewNotification"), object: nil, userInfo:userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // [START refresh_token]
    func tokenRefreshNotification(notification: NSNotification) {
        if InstanceID.instanceID().token() != nil {
            let refreshedToken = InstanceID.instanceID().token()!
            print("InstanceID token: \(refreshedToken)")
            strNotificationToken = refreshedToken
            print("apnsToken: \(strNotificationToken)")
        } else {
            connectToFcm()
        }
    }
    
    func connectToFcm() {
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("Disconnected from FCM.")
        Messaging.messaging().disconnect()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func userInfo () -> NSDictionary? {
        let user = UserDefaults.standard
        return user.value(forKey: KEY_UserInfo) as! NSDictionary?
    }
    
    func logOut() {
        let user = UserDefaults.standard
        user.removeObject(forKey: KEY_UserInfo)
        user.synchronize()
    }
    
    func saveUserInfo (_ json:NSDictionary?) {
        if let dt = json?["result"] as? NSDictionary {
            let user = UserInfo()
            //user.active_date_time =  string(dt, "active_date_time")
            //user.address =  string(dt, "address")
            user.save()
        }
    }
    
    //MARK: GET_BASE_URL
    func getBaseurl() {
        
        if kAppDelegate.server == nil {
            
            Http.instance().json(base_url, nil, "GET", ai: false, popup: true, prnt: true) { (json, params, strJson)  in
                
                if json != nil {
                    let json = json as? NSDictionary
                    
                    if number(json! , "success").boolValue {
                        let dict = json?["result"] as! NSDictionary
                        let ob = BaseUrl()
                        kAppDelegate.server = ob
                        ob.admin_email = string(dict, "admin_email")
                        ob.alternate_contact = string(dict, "alternate_contact")
                        ob.android_app_version = string(dict, "android_app_version")
                        ob.android_bottom_banner = string(dict, "android_bottom_banner")
                        ob.android_update_req = string(dict, "android_update_req")
                        ob.base_url = string(dict, "base_url")
                        ob.created_at = string(dict, "created_at")
                        ob.deleted_at = string(dict, "deleted_at")
                        ob.fault_mil_off_image = string(dict, "fault_mil_off_image")
                        ob.fault_mil_on_image = string(dict, "fault_mil_on_image")
                        ob.id = string(dict, "id")
                        ob.no_fault_image = string(dict, "no_fault_image")
                        ob.site_address = string(dict, "site_address")
                        ob.site_contact = string(dict, "site_contact")
                        ob.site_email = string(dict, "site_email")
                        ob.site_logo = string(dict, "site_logo")
                        ob.site_name = string(dict, "site_name")
                        ob.smtp_mode = string(dict, "smtp_mode")
                        ob.smtp_password = string(dict, "smtp_password")
                        ob.smtp_port = string(dict, "smtp_port")
                        ob.smtp_server = string(dict, "smtp_server")
                        ob.smtp_username = string(dict, "smtp_username")
                        ob.tax = string(dict, "tax")
                        ob.update_app_message = string(dict, "update_app_message")
                        ob.updated_at = string(dict, "updated_at")
                        ob.warning_image = string(dict, "warning_image")
                        ob.webServiceDirectory = string(dict, "webServiceDirectory")
                        kAppDelegate.server = ob
                        
                    } else {
                        print("BaseUrl Not Call")
                    }
                }
            }
        }
    }
    
    //hp
    var locManager:CLLocationManager? = CLLocationManager()
    
    func curLocation () -> CLLocation? {
        locManager?.startUpdatingLocation()
        
        let userLocation = locManager?.location
        
        if userLocation != nil {
            print("location is \(String(describing: userLocation))")
        } else {
            print("location is nil")
        }
        
        return userLocation
    }
    
    func initLocationManager() {
        
        locManager?.delegate = self
        locManager?.desiredAccuracy = kCLLocationAccuracyBest
        locManager?.startMonitoringSignificantLocationChanges()
        locManager?.requestWhenInUseAuthorization()
        //locManager?.allowsBackgroundLocationUpdates = true
        
        // Check authorizationStatus
        let authorizationStatus = CLLocationManager.authorizationStatus()
        // List out all responses
        
        switch authorizationStatus {
        case .authorizedAlways:
            print("authorized")
        case .authorizedWhenInUse:
            print("authorized when in use")
        case .denied:
            print("denied")
        case .notDetermined:
            print("not determined")
        case .restricted:
            print("restricted")
        }
        
        // Get the location
        locManager?.startUpdatingLocation()
        
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) {
            // Extract the location from CLLocationManager
            let userLocation = locManager?.location
            
            // Check to see if it is nil
            if userLocation != nil {
                print("location is \(String(describing: userLocation))")
            } else {
                print("location is nil")
            }
        } else {
            print("not authorized")
        }
    }
    
    //MARK:- Notification fuctions............Neeleshwari

    //didReceiveRemoteNotification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print(userInfo)
         print("Sourabh check actually Data ------------- \(userInfo)")
    }
    
    
}//Sachin :]

   //MARK:- Notification fuctions extension............Neeleshwari =====
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate,MessagingDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TelenyzeGetNewNotification"), object: nil, userInfo:notification.request.content.userInfo)
           print("dictCatchNotifications ----------->>>\(dictCatchNotifications)")
        completionHandler([.badge, .sound])
    }
    
    @nonobjc func application(received remoteMessage: MessagingRemoteMessage){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceiveRemoteNotification"), object: nil, userInfo: remoteMessage.appData)
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) { //when you coming background to forground
        completionHandler()
        // Print message ID.
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TelenyzeGetNewNotification"), object: nil, userInfo:response.notification.request.content.userInfo)
    }
    
    
    // Fcm Message protocol Functions :
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print("New FCM Token received : /(fcmToken)")
        
    }
    @available(iOS 10.0, *)//Sourabh it's very important method ......
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage)
    {
        // What message comes here ?
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TelenyzeGetNewNotification"), object: nil, userInfo: remoteMessage.appData)
       print("dictCatchNotifications ----------->>>\(dictCatchNotifications)")
    }
    
    func applicationReceivedRemoteMessage(remoteMessage: MessagingRemoteMessage){
        print("remoteMessage ========================================= %@", remoteMessage)
    }
   
    
//    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
//
//        //Called when a notification is delivered to a foreground app.
//
//        let userInfo = notification.request.content.userInfo as? NSDictionary
//        print("\(userInfo)")
//
//    }
//
//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//
//        // Called to let your app know which action was selected by the user for a given notification.
//        let userInfo = response.notification.request.content.userInfo as? NSDictionary
//        print("\(userInfo)")
//    }

    
}
//MARK:- Notification fuctions extension............Neeleshwari =====

