  //
  //  GenricViewController.swift
  //  VegiDelivery
  //
  //  Created by Avinash somani on 13/10/16.
  //  Copyright © 2016 Kavyasoftech. All rights reserved.
  //
  
  import UIKit
  
  class BaseUrl: NSObject {
    
    //MARK:- RESULT
    var admin_email = ""
    var alternate_contact = ""
    var android_app_version = ""
    var android_bottom_banner = ""
    var android_update_req = ""
    var base_url = ""
    var created_at = ""
    var deleted_at = ""
    var fault_mil_off_image = ""
    var fault_mil_on_image = ""
    var id = ""
    var no_fault_image = ""
    var site_address = ""
    var site_contact = ""
    var site_email = ""
    var site_logo = ""
    var site_name = ""
    var smtp_mode = ""
    var smtp_password = ""
    var smtp_port = ""
    var smtp_server = ""
    var smtp_username = ""
    var tax = ""
    var update_app_message = ""
    var updated_at = ""
    var warning_image = ""
    var webServiceDirectory = ""
  }
  
  let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
  
  let rs_sign = "\u{20B9}"
  let defaultUser = "defaultUser"
  
  let base_url = "http://ec2-54-193-77-74.us-west-1.compute.amazonaws.com/telenyze/api/auth/baseUrl"  //server
  
  //ec2-54-193-77-74.us-west-1.compute.amazonaws.com/telenyze/api ******* //example link
  
  let baseUrlCall = (kAppDelegate.server?.webServiceDirectory)!
  let api_terms_aboutUs_privacyPolicy = "\((kAppDelegate.server?.webServiceDirectory)!)/content/pages"
  
  let api_signIn = "\(baseUrlCall)/auth/login"
  let api_UserSignUp = "\(baseUrlCall)/auth/registration"
  let api_forgot_password = "\(baseUrlCall)/auth/forgot_password"
  let api_reset_password = "\(baseUrlCall)/auth/reset_password"
  
  let api_change_password = "\(baseUrlCall)/auth/change_password"
  let api_edit_profile = "\(baseUrlCall)/auth/edit_profile"
  let api_help_support = "\(baseUrlCall)/auth/support"
  let api_settings = "\(baseUrlCall)/auth/settings"
  
  let api_vehiclelisting = "\(baseUrlCall)/vehicle/list"
  let api_brandlist = "\(baseUrlCall)/vehicle/brand_list"
  let api_Modellist = "\(baseUrlCall)/vehicle/model_list/id/"
  let api_colorlist = "\(baseUrlCall)/vehicle/color_list"
  let api_addVehicle = "\(baseUrlCall)/vehicle/add_vehicle"
  let api_VehicleDetail = "\(baseUrlCall)/vehicle/detail"
  let api_EditVehicle = "\(baseUrlCall)/vehicle/edit_vehicle"
  let api_deleteVehicle = "\(baseUrlCall)/vehicle/delete_vehicle"
  let api_shopList = "\(baseUrlCall)/shop/list"
  let api_shopListDetail = "\(baseUrlCall)/shop/detail"
  
  let api_appointment_list = "\(baseUrlCall)/auth/appointment_list"
  let api_estimate_request = "\(baseUrlCall)/shop/add_estimate"
  let api_get_estimate = "\(baseUrlCall)/shop/estimate"
  let api_add_appointment = "\(baseUrlCall)/shop/add_appointment"
  let api_reviewList = "\(baseUrlCall)/shop/rating_list"
  let api_SOS_Help = "\(baseUrlCall)/auth/sos"
  let api_parkingLocation = "\(baseUrlCall)/vehicle/add_parking"
  let api_estimateList = "\(baseUrlCall)/shop/estimate_list"
  let api_addRemoveFav = "\(baseUrlCall)/shop/add_remove_fav"
  let api_addReview = "\(baseUrlCall)/shop/add_review"
  
  let api_acceptDenyEstimate = "\(baseUrlCall)/shop/accept_deny_estimate"
  
  //Neeleshwari//api after 14March
  let api_health_status = "\(baseUrlCall)/vehicle/health_status"
  let api_test_notification = "\(baseUrlCall)/auth/test_notification"
  let api_vehicle_connect_status = "\(baseUrlCall)/auth/test_notification"
  let api_acceptDenyAppointment = "\(baseUrlCall)/shop/accept_deny_appointment"
  let api_notificationCount = "\(baseUrlCall)auth/message_count"
  
//Sourabh
  let api_CarHeader_status = "\(baseUrlCall)usrdata/kbtelenyze/"
  let api_notificationList = "\(baseUrlCall)auth/notification_list"
  
  
  
  
  
  
  //**************************** API_END *******************************
  
  class AlertMSG: NSObject {
    
    //ENGLISH
    static let blankEmailAndUserID = "Oops! Looks like you forgot to enter your complete login credentials. Please try again"
    static let passwordLength = "Opps! Your password does not meet the standard. Please enter password between 6-20 characters."
    
    //Add - Car Validation
    static let blankVinNo = "Please enter VIN number"
    static let blankBrand = "Please choose brand"
    static let blankModel = "Please choose model"
    static let blankThemeColor = "Please select theme color"
    static let blankYear = "Please choose year"
    static let blankObd = "Please enter OBD II Device make"
    static let blankIdAndSerialNo = "Please enter Id/Serial number"
    
    //Sign Up - Basic detail
    static let blankRequiredFields = "Sorry, but we will need all the fields to be filled to be able to create your account. Please fill the same. Appreciate it!"
    static let invalidEmailFormat = "The email id format looks incorrect. Please enter a correct one!"
    static let invalidMobileNumber = "The mobile number format looks incorrect / incomplete. Please enter a correct one!"
    
    static let invalidPassword = "Opps! Your password does not meet the standard. Please enter password between 6-20 characters."
    static let pwdMissMatch = "Password and confirm password do not match."
    static let existingEmail = "Looks like the entered email id already exists. Please try a different one to register or login instead"
    
    static let signUpSuccessfulMsg = "We have sent you an OTP on the registered email id & mobile number. Please enter it here to verify your account!"
    
    //Sign Up - Basic detail
    static let blankOTPField = "Oops! Looks like you forgot to enter OTP sent on your registered mobile number. Mobile verification is neccesary to access your account."
    
    static let invalidOTPField = "Please confirm the password."
    
    static let mobileNumberChanged = "Done! New OTP has been sent to your changed mobile number."
    
    static let SuccessfulVerification = "Awesome! Welcome onboard"
    
    //Login
    static let blankEmailOrPassword = "Oops! Looks like you forgot to enter your complete login credentials. Please try again"
    static let invalidLoginDetail = "Sorry! But the credentials entered are incorrect. Please try again"
    static let  validLoginSuccessful = "Login successful."
    
    //Forgot Password
    static let blankEmailForgotPwd = "Oops! Looks like you forgot to enter your registered email/number. Please try again."
    
    static let nonRegisteredEmailOrMobile = "Sorry, but the email / mobile number entered is not registered with us. Please try again with a registered email / mobile number!"
    
    static let forgotSuccessfulSubmit = "We have sent you a temporary password on the registered email id & mobile number. Please login with it to access your account again!"
    
    //Change Password
    static let blankCurrentPassword = "Please enter your current password."
    static let  wrongCurrentPassword = "You have entered wrong current password."
    static let blankConfirmPassword = "Please confirm the password."
    
    static let misMatchCurrentNewPwd = "New and confirm password do not match."
    
    static let blankNewPassword = "Please enter your new password."
    //Mismatched Password
    static let  profileUpdate = "Done! Thanks for the update!"
    
    //Cart
    static let  addToCart = "Great! <item name> has been added to cart"
    static let UpdateQTYCart = "Done! Quantity has been updated"
    static let  deleteItemFromCart = "No worries! <item name> has been removed from your cart"
    static let paymentOrOrderSuccessful = "Awesome! We have got your order & are on it. Thank you!"
    
    static let orderNotPlaced = "Sorry, but your order could not be placed. Request you to please try again. If still no luck then get in touch with us & we will surely help you!"
    
    //Help
    static let blankSubject = "Please enter subject."
    static let blankMessage = "Please enter message."
    
    //add estimate
    static let blankIssueDetails = "Please enter issue details."
    static let blankIssueDescription = "Please enter description."
    
    //add appointment
    static let blankDate = "Please select date."
    static let blankTime = "Please select time."
  }
  
  class appColor :NSObject {
    
    static let appPrimaryColor = UIColor(hex:"29669b")
    static let colorPrimaryDark = UIColor(hex:"29669b")
    static let colorAccent = UIColor(hex:"29669b")
    
    static let whiteColorOpeque = UIColor(hex:"FFFFFF").withAlphaComponent(0.70)
    static let whiteColor = UIColor(hex:"FFFFFF")
    static let darkWhiteColor = UIColor(hex:"a0ffffff")
    static let blackColor = UIColor(hex:"000000")
    static let transparentColor = UIColor(hex:"00000000")
    static let baseColor = UIColor(hex:"29669b")
    
    static let blueColor = UIColor(hex:"1fa8ff")
    static let orangeColor = UIColor(hex:"e73a13")
    static let darkGrayColor = UIColor(hex:"7e7e7e")
    static let appGrayLightColor = UIColor(hex:"1fa8ff")
    static let appGrayImageColor = UIColor(hex:"EDEDED")
    static let profileBGColor = UIColor(hex:"2A3640")
    static let textColorBaseColor = UIColor(hex:"20A9FF")
    
    static let dashboardBGColor = UIColor(hex:"2A363F")
    static let dashboardImageBGColor = UIColor(hex:"6CC72F")
    static let starColor = UIColor(hex:"FFBA01")
    static let declineBGColor = UIColor(hex:"EF3D37")
    static let acceptBGColor = UIColor(hex:"31B22A")
    static let redColor = UIColor(hex:"EF3D37")
    static let greenColor = UIColor(hex:"3CB62B")
    static let yellowColor = UIColor(hex:"FFBA01")
    
    //<!--color for calendar-->
    static let todayColor = UIColor(hex:"FFFFFF")
    static let greyedOutColor = UIColor(hex:"c7c7c7")
    static let summerColor = UIColor(hex:"FFFFFF")
    static let fallColor = UIColor(hex:"FFFFFF")
    static let winterColor = UIColor(hex:"FFFFFF")
    static let springColor = UIColor(hex:"FFFFFF")
    static let appHolidayColor = UIColor(hex:"000000")
    
  }
  
  
  class Base: NSObject {
    
    
  }
  class Validate: NSObject {
    
    public class func email(_ testStr:String) -> Bool {
        var count = 0
        
        for r in testStr.characters {
            
            if r == "@" {
                count += 1
            }
        }
        
        if count > 1 {
            return false
        }
        
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}", options: .caseInsensitive)
            return regex.firstMatch(in: testStr, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, testStr.characters.count)) != nil
        } catch {
            print("hgkjhgjkgh")
            return false
        }
    }
    
  }
  func dayshours (_ days:String, _ hours:String) -> String {
    var day:Int = 0
    
    if days.count > 0 {
        day = Int(days)!
    }
    
    var hour:Int = 0
    
    if hours.count > 0 {
        hour = Int(hours)!
    }
    
    if day > 1 {
        if hour > 1 {
            return "\(day) days \(hour) hours"
        } else {
            return "\(day) days \(hour) hour"
        }
    } else {
        if hour > 1 {
            return "\(day) day \(hour) hours"
        } else {
            return "\(day) day \(hour) hour"
        }
    }
  }
  
  //MARK:- other fuctions
  
  func stringToDate(strDateTime:String) -> String {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let dateFromString: Date? = dateFormatter.date(from: strDateTime)
    
    let formatter = DateFormatter()
    formatter.dateFormat = "d MMM yyyy,HH:mm"
    let stringDateNew: String = formatter.string(from: dateFromString!)
    
    return stringDateNew
  }
  
  /*func alertError (_ json:NSDictionary?) {
   let message = json?["message"] as! String!
   
   if message != "failed" {
   Http.alert("", json?["message"] as! String!)
   } else {
   if let result = json?["result"] as? NSDictionary {
   var msg = ""
   
   let arr = result.allKeys
   
   for i in 0..<arr.count {
   let key = arr[i] as? String
   
   if let keyArr = result[key!] as? NSArray {
   for j in 0..<keyArr.count {
   msg = keyArr[j] as! String
   Http.alert("", msg)
   }
   }
   }
   }
   }
   }*/
  
  /*func checkServer() -> String {
   if kAppDelegate.server == nil {
   return ""
   } else {
   return (kAppDelegate.server?.baseUrl)!
   }
   }*/
  
  //convert date in specific format
  
  extension Float {
    func getStringWith2Decimals() -> String {
        return String(format: "%.2f", self)
    }
  }
  extension CGFloat {
    func getStringWith2Decimals() -> String {
        return String(format: "%.2f", self)
    }
  }
  
  extension UITableView {
    
    func displayBackgroundText(text: String, fontStyle: String, fontSize: CGFloat) {
        let lblRahul = UILabel();
        
        if let headerView = self.tableHeaderView {
            lblRahul.frame = CGRect(x: 0, y: headerView.bounds.size.height, width: self.bounds.size.width, height: self.bounds.size.height - headerView.bounds.size.height)
        } else {
            lblRahul.frame = CGRect(x: 10, y: 0, width: self.bounds.size.width - 20, height: self.bounds.size.height);
        }
        lblRahul.text = text;
        lblRahul.textColor = UIColor.black;
        lblRahul.numberOfLines = 0;
        lblRahul.textAlignment = .center;
        lblRahul.font = UIFont(name: fontStyle, size:fontSize);
        
        let backgroundViewRahul = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height));
        backgroundViewRahul.addSubview(lblRahul);
        self.backgroundView = backgroundViewRahul;
    }
    
    func displayBackgroundText(text: String, fontStyle: String) {
        let lblRahul = UILabel();
        
        if let headerView = self.tableHeaderView {
            lblRahul.frame = CGRect(x: 0, y: headerView.bounds.size.height, width: self.bounds.size.width, height: self.bounds.size.height - headerView.bounds.size.height)
        } else {
            lblRahul.frame = CGRect(x: 10, y: 0, width: self.bounds.size.width - 20, height: self.bounds.size.height);
        }
        lblRahul.text = text;
        lblRahul.textColor = UIColor.black;
        lblRahul.numberOfLines = 0;
        lblRahul.textAlignment = .center;
        lblRahul.font = UIFont(name: fontStyle, size:18);
        
        let backgroundViewRahul = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height));
        backgroundViewRahul.addSubview(lblRahul);
        self.backgroundView = backgroundViewRahul;
    }
    
    func displayBackgroundText(text: String) {
        let lblRahul = UILabel();
        
        if let headerView = self.tableHeaderView {
            lblRahul.frame = CGRect(x: 0, y: headerView.bounds.size.height, width: self.bounds.size.width, height: self.bounds.size.height - headerView.bounds.size.height)
        } else {
            lblRahul.frame = CGRect(x: 10, y: 0, width: self.bounds.size.width - 20, height: self.bounds.size.height);
        }
        lblRahul.text = text;
        lblRahul.textColor = UIColor.gray;
        lblRahul.numberOfLines = 0;
        lblRahul.textAlignment = .center;
        lblRahul.font = UIFont(name: "Montserrat-Regular", size:18);
        //lblRahul.sizeToFit();
        
        let backgroundViewRahul = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height));
        backgroundViewRahul.addSubview(lblRahul);
        self.backgroundView = backgroundViewRahul;
    }
  }
  
  /*extension UIButton {
   
   func makeDisable() {
   self.isUserInteractionEnabled = false
   self.backgroundColor = UIColor.lightGray
   }
   
   func makeEnable() {
   self.isUserInteractionEnabled = true
   self.backgroundColor = PredefinedConstants.appColor()
   }
   }/*
   
   */*/
  
  /*static let blankTime1 = "Please select timeslot"
   static let blankAddress1 = "Please select delivery address"
   static let blankCountry = "Please select country"
   static let blankProblem = "Please select problem"
   static let blankState = "Please select state"
   static let blankCity = "Please select city"
   static let blankZipCode = "Please enter Zip/Postal code"
   static let blankStreet = "Please enter street name"
   static let blankUserName = "Please enter your email."
   static let invalidUserName = "Username should be between 2-200 characters"
   static let blankPassword = "Please enter your password."
   static let blankFirstName = "Please enter your full name."
   static let blankName = "Enter your name."
   static let blankLastName = "Please enter last name."
   static let blankEmail = "Please enter your email."
   static let blankNumber = "Please enter your mobile number."
   static let blankNo = "Enter your mobile no."
   static let blankUserID = "Enter your user id."
   static let numberLength = "Mobile number should be between 8 to 10 digits."
   static let blankCurrentPassword = "Please enter your current password."
   static let blankNewPassword = "Please enter new password."
   static let pwdMissMatch = "New and confirm password do not match."
   static let pwdMissMatchRe = "New password and re-enter password does not match."
   static let blackConfirmPassword = "Please confirm the password."
   static let invalidEmail = "Please enter valid email."
   static let blankStartDate = "Please select start date."
   static let blankEndDate = "Please select end date."
   static let blankCategory = "Please select category."
   static let blankBooking = "Please select booking."
   static let blankNoOfPeople = "Please enter number of people."
   static let blankBankName = "Please enter bank."
   static let blankCompanyName = "Please enter comapany name."
   static let blankAddress = "Please enter your address."
   static let blankContactNumber = "Please enter contact number."
   static let blankZip = "Please enter zip code."
   static let invalidContactNumber = "Please enter valid contact number"
   static let blackOTP = "Please enter OTP number"
   static let blackOtherProblem = "Please enter problem name"
   static let blackDecProblem = "Please describe problem"
   static let blackProblemName = "Please enter problem name"
   static let blankTime = "Please select time."
   static let blankDestination = "Please select destination."
   static let blankIdImage = "Please select photo id image"
   
   static let blankSelect = "Please select..........."
   static let passwordLength = "Password should be 6-20 characters long."
   static let blanknewpassword = "Please enter new password."
   static let blankConfirmPass = "Please enter confirm password."
   static let blankOffer = "Please select offer."
   static let blankStart = "Please select start."
   static let blankPort = "Please select port."
   static let blankPortOther = "Please select other port."
   static let blankTerms = "Please accept Terms & Conditions."
   static let blankNameEmail = "Please enter your email."
   
   static let blnkCleanRate = "Please give cleanliness rating."
   static let blnkServiceRate = "Please give service rating."
   static let blnkComfRate = "Please give comfort rating."
   static let blnkCndtionRate = "Please give condition rating."
   static let blnkNeighRate = "Please give neighbourhood rating."
   static let blnkFeedback = "Please enter your review."
   static let noData = "No data to show."
   
   static let blankMainActivity = "Please select main activity."
   static let blankSubActivity = "Please select sub activity."
   static let blankCancelationType = "Please select cancellation type."
   static let blankActivity = "Please select activity."
   static let blankShareType = "Please select sharing type."
   static let blankSubSharingType = "Please select sub-sharing type."
   static let blankTitle = "Please enter title."
   static let blankLicence = "Please enter licence number."
   static let blankLicenceImage = "Please select licence image."
   static let blankAvailableSeats = "Please enter available seats."
   
   static let blankFishType = "Please enter fish type."
   static let blankBoatSize = "Please enter boat size."
   static let blankBoatSpeed = "Please enter boat speed."
   static let blankBoatCapacity = "Please enter boat capacity."
   static let blankBoatDescription = "Please enter boat description."
   static let blankBoatManufacturer = "Please select boat manufacturer."
   static let blankBoatModel = "Please enter boat model."
   
   static let blankSize = "Please enter size."
   static let blankSpeed = "Please enter speed."
   static let blankCapacity = "Please enter capacity."
   static let blankDescription = "Please enter description."
   static let blankManufacturer = "Please select manufacturer."
   static let blankModel = "Please enter model."
   static let blankType = "Please enter type."
   
   static let blankEquipmentSize = "Please enter equipment size."
   static let blankEquipmentSpeed = "Please enter equipment speed."
   static let blankEquipmentCapacity = "Please enter equipment capacity."
   static let blankEquipmentDescription = "Please enter equipment description."
   static let blankEquipmentManufacturer = "Please select equipment manufacturer."
   static let blankEquipmentModel = "Please enter equipment model."
   static let blankEquipmentType = "Please enter equipment type."*/

