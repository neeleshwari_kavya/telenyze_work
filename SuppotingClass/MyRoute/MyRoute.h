//
//  MyRoute.h
//  ParkPALS
//
//  Created by mac on 06/12/17.
//  Copyright © 2017 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MyRoute : NSObject
-(MKPolyline *)polylineWithEncodedString:(NSString *)encodedString;
@end
