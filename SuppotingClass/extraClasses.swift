//
//  Classes.swift
//  Cloud Market
//
//  Created by Avinash somani on 21/01/17.
//  Copyright © 2017 Kavyasoftech. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class tokenClass: NSObject {
    class func getToken()-> NSMutableDictionary {
        // if let user:NSDictionary = user() {
        if let token:String = string(kAppDelegate.dictUserInfo, "auth_token") {
            // return NSMutableDictionary(dictionary: ["Authorization": "Bearer " + token])
            return NSMutableDictionary(dictionary: ["Authorization":  token])
        }
        // }
        return NSMutableDictionary(dictionary: ["Authorization": "token"])
    }
}

class vehicleList: NSObject {
    var color = ""
    var color_code = ""
    var color_id = ""
    var created_at = ""
    var deleted_at = ""
    var id = ""
    var id_serial_no = ""
    var lat = ""
    var long = ""
    var make = ""
    var make_code = ""
    var model = ""
    var model_code = ""
    var model_id = ""
    var obd_device_make = ""
    var status = ""
    var updated_at = ""
    var users_id = ""
    var vehicle_image = ""
    var vin_number = ""
    var year = ""
}

class brandList: NSObject {
    var code = ""
    var id = ""
    var title = ""
}

class modelList: NSObject {
    var code = ""
    var id = ""
    var title = ""
}

class yearList: NSObject {
    var code = ""
    var id = ""
    var title = ""
}

class colorList: NSObject {
    var code = ""
    var id = ""
    var title = ""
}

class estimateList: NSObject {
    
    var created_at = ""
    var deleted_at = ""
    var descrip = ""
    var estimate_price = ""
    var high_priority = ""
    var id = ""
    var is_drivable = ""
    var is_tow_needed = ""
    var need_ride = ""
    var shop_id = ""
    var shop_name = ""
    var status = ""
    var updated_at = ""
    var users_id = ""
    var vehicle_id = ""
}

class repairShops: NSObject {
    
    var address = ""
    var admin_type = ""
    var contact_number = ""
    var created_at = ""
    var deleted_at = ""
    var distance = ""
    var email = ""
    var fav_status = ""
    var filename = ""
    var forgot_string = ""
    var forgot_string_expiry = ""
    var id = ""
    var image_path = ""
    var lat = ""
    var long = ""
    var name = ""
    var parent_id = ""
    var password = ""
    var rating = ""
    var review = ""
}

class promotionShops: NSObject {
    
    var created_at = ""
    var deleted_at = ""
    var descrip = ""
    var id = ""
    var image = ""
    var link = ""
    var priority = ""
    var shop_id = ""
    var status = ""
    var title = ""
    var updated_at = ""
}

class discount: NSObject {
    var image = ""
}

class feedback: NSObject {
    var avg_rating = ""
    var created_at = ""
    var feedback = ""
    var first_name = ""
    var id = ""
    var last_name = ""
}

class EstimateList : NSObject {
    
    var updated_at = ""
    var name = ""
    var status = ""
    var descrip = ""
    var estimate_price = ""
}

open class ViewC: View {
    @IBInspectable open var isBorderC: Bool = false
    
    @IBInspectable open var borderC: Int = 0
    
    @IBInspectable open var radiousC: Int = 0
    
    @IBInspectable open var borderColorC: UIColor? = nil
    
    @IBInspectable open var isShadowC: Bool = false
    
    @IBInspectable open var shadow_ColorC: UIColor? = UIColor.darkGray
    
    @IBInspectable open var ls_OpacityC:CGFloat = 0.5
    @IBInspectable open var ls_RadiusC:Int = 0
    
    @IBInspectable open var lsOff_WidthC:CGFloat = 2.0
    @IBInspectable open var lsOff_HeightC:CGFloat = 2.0
    
    @IBInspectable open var isStrokeColorC: Bool = false
    
    override open func layoutSubviews() {
        super.isBorder = self.isBorderC
        super.border = borderC
        super.radious = radiousC
        super.borderColor = borderColorC
        super.isShadow = isShadowC
        super.shadow_Color = shadow_ColorC
        super.ls_Opacity = ls_OpacityC
        super.ls_Radius = ls_RadiusC
        super.lsOff_Width = lsOff_WidthC
        super.lsOff_Height = lsOff_HeightC
        super.isStrokeColor = isStrokeColorC
        
        super.layoutSubviews()
    }
}

open class LabelC: Label {
    @IBInspectable open var isBorderC: Bool = false
    
    @IBInspectable open var borderC: Int = 0
    
    @IBInspectable open var radiousC: Int = 0
    
    @IBInspectable open var borderColorC: UIColor? = nil
    
    @IBInspectable open var isShadowC: Bool = false
    
    @IBInspectable open var shadow_ColorC: UIColor? = UIColor.darkGray
    
    @IBInspectable open var ls_OpacityC:CGFloat = 0.5
    @IBInspectable open var ls_RadiusC:Int = 0
    
    @IBInspectable open var lsOff_WidthC:CGFloat = 2.0
    @IBInspectable open var lsOff_HeightC:CGFloat = 2.0
    
    @IBInspectable open var isStrokeColorC: Bool = false
    
    override open func layoutSubviews() {
        super.isBorder = self.isBorderC
        super.border = borderC
        super.radious = radiousC
        super.borderColor = borderColorC
        super.isShadow = isShadowC
        super.shadow_Color = shadow_ColorC
        super.ls_Opacity = ls_OpacityC
        super.ls_Radius = ls_RadiusC
        super.lsOff_Width = lsOff_WidthC
        super.lsOff_Height = lsOff_HeightC
        super.isStrokeColor = isStrokeColorC
        
        super.layoutSubviews()
    }
}

open class TextViewC: TextView {
    @IBInspectable open var isBorderC: Bool = false
    
    @IBInspectable open var borderC: Int = 0
    
    @IBInspectable open var radiousC: Int = 0
    
    @IBInspectable open var borderColorC: UIColor? = nil
    
    @IBInspectable open var isShadowC: Bool = false
    
    @IBInspectable open var shadow_ColorC: UIColor? = UIColor.darkGray
    
    @IBInspectable open var ls_OpacityC:CGFloat = 0.5
    @IBInspectable open var ls_RadiusC:Int = 0
    
    @IBInspectable open var lsOff_WidthC:CGFloat = 2.0
    @IBInspectable open var lsOff_HeightC:CGFloat = 2.0
    
    @IBInspectable open var isStrokeColorC: Bool = false
    
    override open func layoutSubviews() {
        super.isBorder = self.isBorderC
        super.border = borderC
        super.radious = radiousC
        super.borderColor = borderColorC
        super.isShadow = isShadowC
        super.shadow_Color = shadow_ColorC
        super.ls_Opacity = ls_OpacityC
        super.ls_Radius = ls_RadiusC
        super.lsOff_Width = lsOff_WidthC
        super.lsOff_Height = lsOff_HeightC
        super.isStrokeColor = isStrokeColorC
        
        super.layoutSubviews()
    }
}

open class TextFieldC: TextField {
    @IBInspectable open var isBorderC: Bool = false
    
    @IBInspectable open var borderC: Int = 0
    
    @IBInspectable open var radiousC: Int = 0
    
    @IBInspectable open var borderColorC: UIColor? = nil
    
    @IBInspectable open var isShadowC: Bool = false
    
    @IBInspectable open var shadow_ColorC: UIColor? = UIColor.darkGray
    
    @IBInspectable open var ls_OpacityC:CGFloat = 0.5
    @IBInspectable open var ls_RadiusC:Int = 0
    
    @IBInspectable open var lsOff_WidthC:CGFloat = 2.0
    @IBInspectable open var lsOff_HeightC:CGFloat = 2.0
    
    @IBInspectable open var isStrokeColorC: Bool = false
    
    override open func layoutSubviews() {
        super.isBorder = self.isBorderC
        super.border = borderC
        super.radious = radiousC
        super.borderColor = borderColorC
        super.isShadow = isShadowC
        super.shadow_Color = shadow_ColorC
        super.ls_Opacity = ls_OpacityC
        super.ls_Radius = ls_RadiusC
        super.lsOff_Width = lsOff_WidthC
        super.lsOff_Height = lsOff_HeightC
        super.isStrokeColor = isStrokeColorC
        
        super.layoutSubviews()
    }
}

open class ButtonC: Button {
    @IBInspectable open var isBorderC: Bool = false
    
    @IBInspectable open var borderC: Int = 0
    
    @IBInspectable open var radiousC: Int = 0
    
    @IBInspectable open var borderColorC: UIColor? = nil
    
    @IBInspectable open var isShadowC: Bool = false
    
    @IBInspectable open var shadow_ColorC: UIColor? = UIColor.darkGray
    
    @IBInspectable open var ls_OpacityC:CGFloat = 0.5
    @IBInspectable open var ls_RadiusC:Int = 0
    
    @IBInspectable open var lsOff_WidthC:CGFloat = 2.0
    @IBInspectable open var lsOff_HeightC:CGFloat = 2.0
    
    @IBInspectable open var isStrokeColorC: Bool = false
    
    override open func layoutSubviews() {
        super.isBorder = self.isBorderC
        super.border = borderC
        super.radious = radiousC
        super.borderColor = borderColorC
        super.isShadow = isShadowC
        super.shadow_Color = shadow_ColorC
        super.ls_Opacity = ls_OpacityC
        super.ls_Radius = ls_RadiusC
        super.lsOff_Width = lsOff_WidthC
        super.lsOff_Height = lsOff_HeightC
        super.isStrokeColor = isStrokeColorC
        
        super.layoutSubviews()
    }
}
