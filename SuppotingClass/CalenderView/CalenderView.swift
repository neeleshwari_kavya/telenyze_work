//
//  CalView.swift
//  Calender_Mayal
//
//  Created by Avinash somani on 11/04/17.
//  Copyright © 2017 Kavyasoftech. All rights reserved.
//

import UIKit
import HarishFrameworkSwift4

class CalenderButton : UIButton {
    var view:CalenderView!
    var date:Date!
}

protocol CalenderViewDelegate {
    func currentMonthYear (_ monthYear:String, _ view:CalenderView)
    func calenderDateClicked (_ btn:CalenderButton)
    // func holidayList (_ arrHolidaysNeeleshwari:NSMutableArray)
}
//

class CalenderView: UIView {
    
    var arrHolidays = NSMutableArray()
    //  var dictMYShopDetails = NSDictionary()
    
    //  var arrHolidays =  kAppDelegate.arrHolidaysList
    //= //dictShopDetails.mutableCopy() as! NSMutableDictionary
    
    override func draw(_ rect: CGRect) {
        monthForDate (convertDate (Date()))
    }
    
    func convertDate (_ date:Date) -> Date {
        let date = "\(date.getStringDate("dd-MM-yyyy")) 00:00:00  +0000"
        return date.getDate("dd-MM-yyyy HH:mm:ss z")
    }
    
    let calendar = Calendar(identifier: .gregorian)
    
    func getMonthName (_ month:Int) -> String {
        switch month {
        case 1:
            return "January"
        case 2:
            return "February"
        case 3:
            return "March"
        case 4:
            return "April"
        case 5:
            return "May"
        case 6:
            return "June"
        case 7:
            return "July"
        case 8:
            return "August"
        case 9:
            return "September"
        case 10:
            return "October"
        case 11:
            return "November"
        case 12:
            return "December"
        default:
            return ""
        }
    }
    
    var delegate:CalenderViewDelegate!
    
    func monthForDate (_ startDate:Date) {
        //let weeekDay = calendar.component(.weekday, from: startDate)
        var day = calendar.component(.day, from: startDate)
        let month = calendar.component(.month, from: startDate)
        let year = calendar.component(.year, from: startDate)
        
        if delegate != nil {
            delegate.currentMonthYear("\(getMonthName(month)) \(year)", self)
        }
        
        var decrease = -1
        
        day -= 1
        
        let maMonthDays = NSMutableArray()
        
        while day >= 1 {
            
            let date1 = calendar.date(byAdding: .day, value: decrease, to: startDate)
            
            maMonthDays.insert(convertDate(date1!), at: 0)
            
            if day == 1 {
                var weeekDayPrevious = calendar.component(.weekday, from: date1!)
                
                while weeekDayPrevious > 1 {
                    decrease -= 1
                    
                    let date2 = calendar.date(byAdding: .day, value: decrease, to: startDate)
                    maMonthDays.insert(convertDate(date2!), at: 0)
                    
                    weeekDayPrevious -= 1
                }
                break
            }
            
            decrease -= 1
            day -= 1
        }
        
        maMonthDays.add(startDate)
        
        day = calendar.component(.day, from: startDate)
        
        var increase = 1
        
        var dateNext = calendar.date(byAdding: .day, value: increase, to: startDate)
        var dayNext = calendar.component(.day, from: dateNext!)
        
        var lastDate:Date!
        
        while (dayNext > day) {
            dateNext = calendar.date(byAdding: .day, value: increase, to: startDate)
            lastDate = dateNext
            dayNext = calendar.component(.day, from: dateNext!)
            let monthNext = calendar.component(.month, from: dateNext!)
            
            if monthNext > month {
                maMonthDays.add(convertDate(dateNext!))
                
                var weeekDayNext = calendar.component(.weekday, from: dateNext!)
                
                while (7 > weeekDayNext) {
                    increase += 1
                    dateNext = calendar.date(byAdding: .day, value: increase, to: startDate)
                    lastDate = dateNext
                    maMonthDays.add(convertDate(dateNext!))
                    
                    weeekDayNext += 1
                }
            } else {
                maMonthDays.add(convertDate(dateNext!))
            }
            
            increase += 1
        }
        
        if lastDate != nil {
            increase = 1
            while maMonthDays.count < 42 {
                let date = calendar.date(byAdding: .day, value: increase, to: lastDate)
                maMonthDays.add(convertDate(date!))
                increase += 1
            }
        }
        
        maMonthDays.insert("SAT", at: 0)
        maMonthDays.insert("FRI", at: 0)
        maMonthDays.insert("THU", at: 0)
        maMonthDays.insert("WED", at: 0)
        maMonthDays.insert("TUE", at: 0)
        maMonthDays.insert("MON", at: 0)
        maMonthDays.insert("SUN", at: 0)
        
        createCalButtons (maMonthDays, startDate)
    }
    
    var sizeButtonMain:CGSize!
    
    func createCalButtons (_ maDates:NSMutableArray, _ startDate:Date) {
        
        //=============================================
        self.neelesshwariAppointmentDetails()
        //self.weeklyHolidays()
        self.neelesshwariWeeklyHolidays()
        //=============================================
        
        sizeButtonMain = CGSize(width: self.frame.size.width / 7.0, height: self.frame.size.height / 7.0)
        
        for vv in self.subviews {
            vv.removeFromSuperview()
        }
        
        var index = 0
        
        let month = calendar.component(.month, from: startDate)
        let year = calendar.component(.year, from: startDate)
        
        if month == 1 {
            previousMonth = "15-12-\(year-1) 00:00:00 +0000".getDate("dd-MM-yyyy HH:mm:ss z")
            nextMonth = "15-\(month+1)-\(year) 00:00:00 +0000".getDate("dd-MM-yyyy HH:mm:ss z")
        } else if month == 12 {
            previousMonth = "15-\(month-1)-\(year) 00:00:00 +0000".getDate("dd-MM-yyyy HH:mm:ss z")
            nextMonth = "15-01-\(year+1) 00:00:00 +0000".getDate("dd-MM-yyyy HH:mm:ss z")
        } else {
            previousMonth = "15-\(month-1)-\(year) 00:00:00 +0000".getDate("dd-MM-yyyy HH:mm:ss z")
            nextMonth = "15-\(month+1)-\(year) 00:00:00 +0000".getDate("dd-MM-yyyy HH:mm:ss z")
        }
        
        for i in 0..<8 {
            for j in 0..<8 {
                index = i * 7 + j
                
                if maDates.count > index {
                    let x:CGFloat = (CGFloat(j) * sizeButtonMain.width)
                    let y:CGFloat = (CGFloat(i) * sizeButtonMain.height)
                    
                    let btn = CalenderButton(frame: CGRect(x: x + 3, y: y, width: 30, height: 30))
                    btn.view = self
                    self.addSubview(btn)
                    
                    if i == 0 {
                        if j <= 6 {
                            btn.setTitleColor(appColor.whiteColor, for: .normal)
                            btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12.0)
                            let date:String = (maDates[index] as? String)!
                            btn.setTitle("\(date)", for: .normal)
                            //print("daysss>>>>>.\(date)")
                            let btn2 = CalenderButton(frame: CGRect(x: x, y: y + 30 , width: sizeButtonMain.width, height: 1))
                            self.addSubview(btn2)
                            btn2.backgroundColor = UIColor.white
                        }
                    } else {
                        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12.0)
                        
                        let date:Date = (maDates[index] as? Date)!
                        btn.date = date
                        
                        let monthNow = calendar.component(.month, from: date)
                        // print("monthNow>>>\(monthNow)")
                        
                        if month == monthNow {
                            btn.addTarget(self, action: #selector(calenderButtonClicked(_:)), for: .touchUpInside)
                            btn.setTitleColor(UIColor.white, for: .normal)
                            
                        } else {
                            btn.setTitleColor(appColor.whiteColorOpeque, for: .normal)
                        }
                        
                        let day = calendar.component(.day, from: date)
                        ////
                        let today = NSDate() as Date
                        
                        btn.layer.cornerRadius =   btn.frame.size.height/2
                        btn.layer.borderWidth = 0
                        btn.layer.borderColor = UIColor.clear.cgColor
                        
                        if (calendar.component(.day, from: date) == calendar.component(.day, from: today )) && (calendar.component(.month, from: date) == calendar.component(.month, from: today)) && (calendar.component(.year, from: date) == calendar.component(.year, from: today)){
                            
                            //  btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14.0)
                            btn.backgroundColor = appColor.blueColor
                            btn.layer.cornerRadius =   btn.frame.size.height/2
                            btn.layer.borderWidth = 1.0
                            btn.layer.borderColor = UIColor.white.cgColor
                        }
                        if (calendar.component(.day, from: date) == calendar.component(.day, from: today )) || (calendar.component(.day, from: date) < calendar.component(.day, from: today )){
                            btn.isUserInteractionEnabled = false
                            // print("previous date>>\(date)")
                        }
                        ///arrHolidays ////static holidays
                        //  arrHolidays.add(today.yesterday)
                        //  arrHolidays.add(today.dayBeforeYesterday)
                        // print("arrHolidays>>>\(arrHolidays)")
                        
                        var status = ""
                        var weekDayshop = ""
                        
                        let weekdayCalendar = Calendar.current.component(.weekday, from: date)
                        
                        for i in 0..<arrWeeklyTime.count {
                            let dict = arrWeeklyTime[i] as! NSDictionary
                            
                            weekDayshop  = string(dict, "day")
                            status = string(dict, "status")
                            
                            if let dateDay = Int(weekDayshop) {
                                let intDay =  dateDay + 1
                                
                                if weekdayCalendar == intDay {
                                    if status == "close"{
                                        arrHolidays.add(date)
                                    }
                                }
                            }
                        }
                        //print("arrHolidays>>>\(arrHolidays)")
                        if month == monthNow {
                            for n in 0..<arrHolidays.count{
                                let holiday = arrHolidays.object(at: n) as! Date
                                
                                if (calendar.component(.day, from: date) == calendar.component(.day, from: holiday )) && (calendar.component(.month, from: date) == calendar.component(.month, from: holiday)) && (calendar.component(.year, from: date) == calendar.component(.year, from: holiday)){
                                    
                                    btn.backgroundColor = appColor.appHolidayColor //appColor.redColor
                                    btn.layer.cornerRadius =   btn.frame.size.height/2
                                    btn.layer.borderWidth = 0
                                    btn.layer.borderColor = UIColor.green.cgColor
                                    btn.isUserInteractionEnabled = false
                                }
                            }
                        }
                        btn.setTitle("\(day)", for: .normal)
                        // print("day>>>>>>.\(day)")
                    }
                }
            }
        }
    }
    
    //MARK:-NeelesshwariAppointmentDetails +++++++++++++++++++++============//Neeleshwari
    
    func neelesshwariAppointmentDetails() {
        
        let dictMYShopDetails = kAppDelegate.dictShopNeeleshwari.mutableCopy() as! NSMutableDictionary
        
        //print("dictMYShopDetails>>>\(dictMYShopDetails)")
        
        if dictMYShopDetails.count != 0{
            
            if let arrShopHoliday = dictMYShopDetails.object(forKey: "shop_holiday") as? NSArray {
                
                for r in 0..<arrShopHoliday.count {
                    
                    if let dictHoliday = arrShopHoliday.object(at: r) as? NSDictionary {
                        
                        let fromDate = string(dictHoliday, "from_date").getDate("yyyy-MM-dd HH:mm:ss")
                        let toDate = string(dictHoliday, "to_date").getDate("yyyy-MM-dd HH:mm:ss")
                        
                        let strDaysCount = daysBetweenDates(startDate: fromDate, endDate: toDate)
                        
                        let intDaysCount = Int(strDaysCount)
                        
                        for i in 0...intDaysCount {
                            
                            if  let tomorrow = Calendar.current.date(byAdding: .day, value: i, to: fromDate){
                                arrHolidays.add(tomorrow)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([Calendar.Component.day], from: startDate, to: endDate)
        return components.day!
    }
    
    //+++++++++++++++++++++============//Neeleshwari
    
    var arrWeeklyTime = NSMutableArray()
    
    func neelesshwariWeeklyHolidays() {
        let dictMYShopDetails = kAppDelegate.dictShopNeeleshwari.mutableCopy() as! NSMutableDictionary
        
        if let shop_weekly_timing =  dictMYShopDetails.object(forKey: "shop_weekly_timing") as? NSArray {
            arrWeeklyTime = shop_weekly_timing.mutableCopy() as! NSMutableArray
        }
    }
    
    ////=======================================Neeleshwari
    
    var nextMonth:Date!
    var previousMonth:Date!
    
    func calenderButtonClicked (_ btn:CalenderButton) {
        
        delegate.calenderDateClicked(btn)
        changeClickedButtonBackground (btn)
    }
    
    func showPreviousMonth () {
        monthForDate (convertDate(previousMonth))
    }
    
    func showNextMonth () {
        monthForDate (convertDate(nextMonth))
    }
    
    func changeClickedButtonBackground (_ btn:CalenderButton) {
        
        for vv in self.subviews {
            // vv.backgroundColor = UIColor.clear
            if vv.backgroundColor == appColor.appHolidayColor  || vv.backgroundColor == UIColor.white{
            }else{
                vv.backgroundColor = UIColor.clear
            }
        }
        btn.backgroundColor = appColor.blueColor
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13.0)
    }
    
}/////+++++++++++++++++++++============//Neeleshwari

extension UIView {
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:self.frame.size.height - width, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat,viewWidth: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:self.frame.size.height - width, width: viewWidth, height:width)
        self.layer.addSublayer(border)
    }
    
}

extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayBeforeYesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -2, to: noon)!
    }
    
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    
}





