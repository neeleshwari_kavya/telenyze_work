//
//  UserInfo.swift
//  Zooma
//
//  Created by Avinash somani on 26/12/16.
//  Copyright © 2016 Kavyasoftech. All rights reserved.
//

import UIKit

open class UserInfo: NSObject, NSCoding {
    
    open func encode(with aCoder: NSCoder) {
    }
    
    public required init?(coder aDecoder: NSCoder) {
    }
    
    //Harish sir class
    
   /* var address = ""
    var app_version = ""
    var appoitment_request = ""
    var auth_token = ""
    var contact_no = ""
    var created_at = ""
    var deleted_at = ""
    var device_id = ""
    var device_type = ""
    var distance = ""
    var email = ""
    var email_status = ""
    var emergency_email = ""
    var emergency_no = ""
    var first_name = ""
    var forgot_string = ""
    var forgot_string_expiry = ""
    var from_back_end = ""
    var geo_fence_radius = ""
    var id = ""
    var ip = ""
    var last_name = ""
    var lat = ""
    var location = ""
    var long = ""
    var new_message = ""
    var no_of_vehicle = ""
    var notification_id = ""
    var notification_status = ""
    var password = ""
    var profile_picture = ""
    
    var send_me_location = ""
    var set_scan_period = ""
    var shop_promotional = ""
    var sound_status = ""
    var status = ""
    var temperature = ""
    var type = ""
    var update = ""
    var update_period = ""
    var updated_at = ""
    var username = ""
    
    var vehicle_service = ""
    var verification_status = ""
    var vibration_status = ""*/
   
    override public init() {
    }
    
    /*public init(_ address:String, _ app_version:String, _ appoitment_request:String, _ auth_token:String, _ contact_no:String, _ created_at:String, _ deleted_at:String, _ device_id:String, _ device_type:String, _ vardistance:String,_ email:String, _ email_status:String, _ emergency_email:String, _ emergency_no:String, _ first_name:String, _ forgot_string:String, _ forgot_string_expiry:String, _ from_back_end:String, _ geo_fence_radius:String, id: String,_ ip:String, _ last_name:String, _ lat:String, _ location:String, _ long:String, _ new_message:String, _ no_of_vehicle:String, _ notification_id:String, _ notification_status:String, password: String, profile_picture: String,_ send_me_location:String, _ set_scan_period:String, _ shop_promotional:String, _ sound_status:String, _ status:String, _ temperature:String, _ type:String, _ update:String, _ update_period:String, updated_at: String, username: String, vehicle_service: String, verification_status: String, vibration_status: String) {
        
        self.address = address
        self.app_version = app_version
        self.appoitment_request = appoitment_request
        self.auth_token = auth_token
        self.contact_no = contact_no
        self.created_at = created_at
        self.deleted_at = deleted_at
        self.device_id = device_id
        self.device_type = device_type
        self.email = email
        self.email_status = email_status
        self.emergency_email = emergency_email
        self.emergency_no = emergency_no
        self.first_name = first_name
        self.geo_fence_radius = geo_fence_radius
        self.id = id
        self.ip = ip
        self.last_name = last_name
        self.lat = lat
        self.location = location
        self.long = long
        self.new_message = new_message
        self.no_of_vehicle = no_of_vehicle
        self.notification_id = notification_id
        self.notification_status = notification_status
        self.password = password
        self.profile_picture = profile_picture
        self.send_me_location = send_me_location
        self.set_scan_period = set_scan_period
        self.shop_promotional = shop_promotional
        self.sound_status = sound_status
        self.status = status
        self.temperature = temperature
        self.type = type
        self.update = update
        self.update_period = update_period
        self.updated_at = updated_at
        self.username = username
        self.vehicle_service = vehicle_service
        self.verification_status = verification_status
        self.vibration_status = vibration_status
    }*/
    
    /*open func encode(with aCoder: NSCoder) {
        aCoder.encode(address, forKey: "address")
        aCoder.encode(app_version, forKey: "app_version")
        aCoder.encode(appoitment_request, forKey: "appoitment_request")
        aCoder.encode(contact_no, forKey: "contact_no")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(deleted_at, forKey: "deleted_at")
        aCoder.encode(device_id, forKey: "device_id")
        aCoder.encode(device_type, forKey: "device_type")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(email_status, forKey: "self.email_status")
        aCoder.encode(emergency_email, forKey: "self.emergency_email")
        aCoder.encode(emergency_no, forKey: "emergency_no")
        aCoder.encode(first_name, forKey: "first_name")
        aCoder.encode(geo_fence_radius, forKey: "geo_fence_radius")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(ip, forKey: "ip")
        aCoder.encode(last_name, forKey: "last_name")
        aCoder.encode(lat, forKey: "lat")
        aCoder.encode(location, forKey: "location")
        aCoder.encode(long, forKey: "long")
        aCoder.encode(new_message, forKey: "new_message")
        aCoder.encode(no_of_vehicle, forKey: "no_of_vehicle")
        aCoder.encode(notification_id, forKey: "notification_id")
        aCoder.encode(notification_status, forKey: "notification_status")
        aCoder.encode(password, forKey: "password")
        aCoder.encode(profile_picture, forKey: "profile_picture")
        aCoder.encode(send_me_location, forKey: "send_me_location")
        aCoder.encode(set_scan_period, forKey: "set_scan_period")
        aCoder.encode(shop_promotional, forKey: "shop_promotional")
        aCoder.encode(shop_promotional, forKey: "shop_promotional")
        aCoder.encode(sound_status, forKey: "sound_status")
        aCoder.encode(status, forKey: "status")
        aCoder.encode(temperature, forKey: "temperature")
        aCoder.encode(type, forKey: "type")
        aCoder.encode(update, forKey: "update")
        aCoder.encode(update_period, forKey: "update_period")
        aCoder.encode(updated_at, forKey: "updated_at")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(vehicle_service, forKey: "vehicle_service")
        aCoder.encode(verification_status, forKey: "verification_status")
        aCoder.encode(vibration_status, forKey: "vibration_status")
    }*/
    
    /*public required init?(coder aDecoder: NSCoder) {
        address = aDecoder.decodeObject(forKey: "address") as! String!
        app_version = aDecoder.decodeObject(forKey: "app_version") as! String!
        appoitment_request = aDecoder.decodeObject(forKey: "appoitment_request") as! String!
        auth_token = aDecoder.decodeObject(forKey: "auth_token") as! String!
        contact_no = aDecoder.decodeObject(forKey: "contact_no") as! String!
        created_at = aDecoder.decodeObject(forKey: "created_at") as! String!
        deleted_at = aDecoder.decodeObject(forKey: "deleted_at") as! String!
        device_id = aDecoder.decodeObject(forKey: "device_id") as! String!
        device_type = aDecoder.decodeObject(forKey: "device_type") as! String!
        distance = aDecoder.decodeObject(forKey: "distance") as! String!
        email = aDecoder.decodeObject(forKey: "email") as! String!
        email_status = aDecoder.decodeObject(forKey: "email_status") as! String!
        emergency_email = aDecoder.decodeObject(forKey: "emergency_email") as! String!
        emergency_no = aDecoder.decodeObject(forKey: "emergency_no") as! String!
        first_name = aDecoder.decodeObject(forKey: "first_name") as! String!
        forgot_string = aDecoder.decodeObject(forKey: "forgot_string") as! String!
        forgot_string_expiry = aDecoder.decodeObject(forKey: "forgot_string_expiry") as! String!
        from_back_end = aDecoder.decodeObject(forKey: "from_back_end") as! String!
        geo_fence_radius = aDecoder.decodeObject(forKey: "geo_fence_radius") as! String!
        id = aDecoder.decodeObject(forKey: "id") as! String!
        ip = aDecoder.decodeObject(forKey: "ip") as! String!
        last_name = aDecoder.decodeObject(forKey: "last_name") as! String!
        lat = aDecoder.decodeObject(forKey: "lat") as! String!
        location = aDecoder.decodeObject(forKey: "location") as! String!
        long = aDecoder.decodeObject(forKey: "long") as! String!
        new_message = aDecoder.decodeObject(forKey: "new_message") as! String!
        no_of_vehicle = aDecoder.decodeObject(forKey: "no_of_vehicle") as! String!
        notification_id = aDecoder.decodeObject(forKey: "notification_id") as! String!
        notification_status = aDecoder.decodeObject(forKey: "notification_status") as! String!
        password = aDecoder.decodeObject(forKey: "password") as! String!
        profile_picture = aDecoder.decodeObject(forKey: "profile_picture") as! String!
        send_me_location = aDecoder.decodeObject(forKey: "send_me_location") as! String!
        set_scan_period = aDecoder.decodeObject(forKey: "set_scan_period") as! String!
        shop_promotional = aDecoder.decodeObject(forKey: "shop_promotional") as! String!
        sound_status = aDecoder.decodeObject(forKey: "sound_status") as! String!
        status = aDecoder.decodeObject(forKey: "status") as! String!
        temperature = aDecoder.decodeObject(forKey: "temperature") as! String!
        type = aDecoder.decodeObject(forKey: "type") as! String!
        update = aDecoder.decodeObject(forKey: "update") as! String!
        update_period = aDecoder.decodeObject(forKey: "update_period") as! String!
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as! String!
        username = aDecoder.decodeObject(forKey: "username") as! String!
        vehicle_service = aDecoder.decodeObject(forKey: "vehicle_service") as! String!
        verification_status = aDecoder.decodeObject(forKey: "verification_status") as! String!
        vibration_status = aDecoder.decodeObject(forKey: "vibration_status") as! String!
    }*/
    
    class public func archivePeople(_ people:UserInfo) -> NSData {
        let archivedObject = NSKeyedArchiver.archivedData(withRootObject: people)
        return archivedObject as NSData
    }
    
    class public func retrievePeople(_ data:NSData) -> UserInfo {
        return (NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? UserInfo)!
    }
    
    class public func save (_ ob:UserInfo) {
        let defaults = UserDefaults.standard
        defaults.set(archivePeople(ob), forKey: "LoginInfo")
        defaults.synchronize()
    }
    
    public func save () {
        UserInfo.save(self)
    }
    
    public class func logout () {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "LoginInfo")
        defaults.synchronize()
    }

    open class func user1() -> Any? {
        let defaults = UserDefaults.standard
        
        if (defaults.object(forKey: "LoginInfo") != nil) {
            let data = defaults.object(forKey: "LoginInfo") as! NSData
            return retrievePeople(data)
        } else {
            return nil
        }
    }
    
    open func user() ->  Any? {
        return UserInfo.user1()
    }
}
